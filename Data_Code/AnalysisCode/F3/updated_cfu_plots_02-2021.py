#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 23 12:00:57 2021

@author: meyerct6
"""

#Import libraries
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
font = {'family' : 'arial',
        'weight':'normal',
        'size'   : 8}    #Specify font size
axes = {'linewidth': 2}
rc('font', **font)
rc('axes',**axes)
import os
from cycler import cycler

#Set directory
os.chdir('/home/meyerct6/Repos/GCAMP_paper/Data_Code/AnalysisCode/F3')

#Read in and organize the data
df_sens = pd.read_csv('GCmS_sensitive_CFUs.csv')
df_res = pd.read_csv('mntH_CFUs.csv')
df_sens = df_sens.melt('time (h)')
df_res = df_res.melt('time (h)')
df_sens['Sample'] = 'sensitive'
df_res['Sample'] = 'resistant'
df = pd.concat([df_sens,df_res])
df.columns = ['Time','Conc(ug/ml)','Colony','Sample']
df['Drug']='Kan'
df['Conc(ug/ml)'] =df['Conc(ug/ml)'].astype(float).round(0)

#Set the zeros to 1/10th the minimum number of colonies observed
df.loc[df['Colony']==0,'Colony'] = np.sort(np.unique(df['Colony']))[1]/10
df['Colony'] = df['Colony']*1000.  #Convert to colonies per mL
#Generate figure.  Inches is the size
fig = plt.figure(figsize=(1.75,2.75),facecolor='w')
ax1 = plt.subplot(2,1,1)
ax2 = plt.subplot(2,1,2)
# Hide the right and top spines
ax1.spines['right'].set_visible(False)
ax1.spines['top'].set_visible(False)
ax1.yaxis.set_ticks_position('left')
ax1.xaxis.set_ticks_position('bottom')
ax2.spines['right'].set_visible(False)
ax2.spines['top'].set_visible(False)
ax2.yaxis.set_ticks_position('left')
ax2.xaxis.set_ticks_position('bottom')

#Use Pandas to plot
#df = pd.read_csv('compiled_cfu_time_vs_titration.csv')
#df= df[df['Time']<7]  #Remove timepoints greater than 7 hourstreatment
df['Time'] = df['Time']*60
df['Time'] = df['Time'].astype('int')
unTime = np.sort(df['Time'].unique())
df['unTime'] = 0
for e,u in enumerate(unTime):
    df.loc[df['Time']==u,'unTime'] = e

df['unTime']
dfs = df[df['Sample']=='sensitive']
dfr = df[df['Sample']=='resistant']
#Set the colors to match the R heat map colors
plt.rc('axes', prop_cycle=(cycler('color', ["#FFFFB2", "#FECC5C" ,"#FD8D3C" ,"#F03B20" ,"#BD0026"])))
#Groupby time and concentration and plot line plot
dfsg = dfs[['unTime','Conc(ug/ml)','Colony']].groupby(by=['unTime','Conc(ug/ml)']).agg(['mean']).unstack().plot.line(ax=ax1,legend=False,logy=True,linewidth=2);
dfrg = dfr[['unTime','Conc(ug/ml)','Colony']].groupby(by=['unTime','Conc(ug/ml)']).agg(['mean']).unstack().plot.line(ax=ax2,legend=False,logy=True,linewidth=2);
#Add legend
ax2.legend(['0','3','10','30','100'],title='[Kan]',frameon=False)
#Specify axis limits
ax1.set_ylim(10,10**9)
ax2.set_ylim(10,10**9)
ax1.set_xlim(0,4)
ax2.set_xlim(0,4)
ax2.set_xticks(range(5))
ax2.set_xticklabels(unTime,rotation=45)
ax1.set_ylabel('CFU/mL')
ax2.set_ylabel('CFU/mL')
ax1.set_xlabel('')
ax2.set_xlabel('Time (min)')
ax1.set_title('Sensitive')
ax1.set_xticklabels([])
ax2.set_title('Resistant')
plt.tight_layout()
ax1.plot(ax1.get_xlim(),(200.,200.),'--k')
ax2.plot(ax2.get_xlim(),(200.,200.),'--k')

#Save figure
plt.savefig('Fig3_cfu.pdf',bbox_inches = 'tight',pad_inches = 0)

