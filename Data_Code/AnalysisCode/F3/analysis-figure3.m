cd /home/meyerct6/Repos/GCAMP_paper/Data_Code/AnalysisCode/F3

%%Step 1: Read in dataset of interest
%Clear everything and start a timer
clear; close all;tic

%Load in data
load('../../Datasets/02262021_AllSingleCellInfoData.mat')

%Write CSV files to generate heatmaps figure in R 
sdMatG = movstd((intMatG./movmedian(intMatG',45)')',30)';

%Compile data for heatmap figure
writematrix(sdMatG,'compiled_sdIntG.csv')
writematrix(intMatG,'compiled_meanIntG.csv') %For plotting trajectories
writematrix(intMatR,'compiled_meanIntR.csv')
writematrix(matArea,'compiled_cellArea.csv')
writematrix(drug_conc,'compiled_drugConc.csv')
writematrix(kan_r,'compiled_kanR.csv')

%% Make heatmap using R code...
%% Make cfu plot using python code ...

