%%%%%To get a good segmentation image%%%%
%Must be run after processing as it loads jitter correction...
%%Step 1:  Set up experiment to process
%Clear everything and start a timer
clear; close all;tic
%Set active directory
activeDir    = '/mnt/kralj_celery/E coli GC antibioitic/2020-12-17-kan_dilution_gcamp_cm';
saveDir      = [activeDir filesep '2021-01-15_Results'];
dataDir      = [saveDir filesep 'matFiles'];

cd(activeDir) %Move into the experimental directory
flist        = dir('*C8*.tif'); %Current file list
nfiles       = length(flist); %Number of files
framesToKeep = 0;       % Set to 0 to go to maximum # frames
subBack      = 1;       % Subtract background?
corrIllum    = 1;       % Correct illumination?
alignRG      = 1;       % Align the red and green channels

% Sort through the data and find the number of colors
clist = cell(nfiles,1);
for ff = 1:nfiles
    tmp1 = flist(ff).name; tmp2 = regexp(tmp1,'_','split');
    clist{ff} = tmp2{1,6};
end
nColor = length(unique(clist));

load([saveDir filesep 'illum.mat'])
f = 1;

T = readtable('Well_treatments.xlsx');  
%Get image information
fName = cell(nColor,1);
fName{1} = flist((nColor*f) - (nColor-1)).name;
for cc = 2:nColor
    fName{cc} = [fName{1}(1:end-6) clist{(nColor*(f-1)) + (cc)}];
end
tmp = regexp(fName{1},'_','split');
tmp = regexp(tmp{1},' - ','split');
tmp = tmp{2};
%Pad with zero if less than 10
if str2double(tmp(2:end)) < 10
    currWell = [tmp(1) '0' tmp(end)];
else
    currWell = tmp;
end
%Find the current row and associated information
currXLSrow = find(contains(T.Well,currWell));
currDrug   = T.Drug{currXLSrow};
currConc   = floor(T.Conc(currXLSrow));
currCell   = T.CellType{currXLSrow};
%Names of movies
dataName = [dataDir filesep currWell '_' currCell '_' ...
            currDrug '_' num2str(currConc) filesep];
        
disp([num2str(f) ' of ' num2str(nfiles/nColor) ': ' fName{1}]);
%Get stack info
tifInfo = imfinfo(fName{1});    nframes = length(tifInfo);
ysize   = tifInfo(1).Height;    xsize   = tifInfo(1).Width;
%Read in image stack
dat = zeros(ysize,xsize,nframes,nColor,'uint16');
for cc = 1:nColor
    tmp = bigread2(fName{cc});
    dat(:,:,:,cc) = tmp;
end
if framesToKeep~=0  %If subsetting by number of frames
    time(framesToKeep+1:end) = [];
    R = squeeze(dat(:,:,1:framesToKeep,2));
    G = squeeze(dat(:,:,1:framesToKeep,1));
else
    R = squeeze(dat(:,:,:,2));
    G = squeeze(dat(:,:,:,1));
end
[ysize,xsize,nframes] = size(G);

% Correct the illumination
disp("...illumination correction...")
if corrIllum==1
    for nn = 1:nframes
        G(:,:,nn) = uint16(mean(illumG(:))*double(G(:,:,nn))./double(illumG));
        R(:,:,nn) = uint16(mean(illumR(:))*double(R(:,:,nn))./double(illumR));
    end
end
% Align the red movies to the green
disp("...aligning...")
if alignRG==1
    Reg = imref2d(size(R(:,:,1)));
    for nn = 1:nframes
        G(:,:,nn) = imwarp(G(:,:,nn),tFormRG{1},'OutputView',Reg);
    end
end
    
%Jitter Correction
disp("...jitter correction...")
S = load([dataName 'fft_registration.mat']);
xShift_tot = S.S.xShift_tot; yShift_tot = S.S.yShift_tot;
for kk=1:4
    xShift = xShift_tot{kk}; yShift = yShift_tot{kk};
    if ~isempty(xShift)
        for nn=1:nframes
            G(:,:,nn) = imtranslate(G(:,:,nn),[1.0*xShift(nn) 1.0*yShift(nn)]);
            R(:,:,nn) = imtranslate(R(:,:,nn),[1.0*xShift(nn) 1.0*yShift(nn)]);
        end
        xNew = 1 + ceil(max(xShift)):xsize + ceil(min(xShift));
        yNew = 1 + ceil(max(yShift)):ysize + ceil(min(yShift));
        G = G(yNew,xNew,:);        R = R(yNew,xNew,:);
        xsize = length(xNew);      ysize = length(yNew);
    end
end
    
% Subtract background
disp('...back subtract')
if subBack
    for nn = 1:nframes
        R(:,:,nn) = R(:,:,nn) - imgaussfilt(imopen(R(:,:,nn),strel('disk',70)),[45 45]); %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        G(:,:,nn) = G(:,:,nn) - imgaussfilt(imopen(G(:,:,nn),strel('disk',70)),[45 45]); %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    end
    G = G - min(G(:)) + 50;
    R = R - min(R(:)) + 50;
end
 
disp('...segmenting')
%Set the pixel intensity considered signal in the images
R_brightThresh = 120;                                                       %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
G_brightThresh = 500;                                                       %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
%Measure how the number of pixels exceeding a brightness threshold
%change over time.
nPix = length(yNew)*length(xNew);
pctBrightG = sum(G>G_brightThresh,[1,2])/nPix;
pctBrightR = sum(R>R_brightThresh,[1,2])/nPix;

% Segment and extract traces using the Hessian
startFrame = min(61,nframes-20);                                            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%                       

LR         = Hess6(mean(R(:,:,startFrame:1:startFrame+10),3),1.5,2,1,120);  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ncells     = max(LR(:));

%     %Commented out to only segment based on RED
LG         = Hess6(mean(G(:,:,startFrame:1:startFrame+10),3),1.5,2,1,G_brightThresh);
L = bwlabel((LG+LR)>0);
ncells    = max(L(:));   
%figure();imshowpair(L,LR)


fig = figure();
L = bwperim(G(:,:,startFrame)>G_brightThresh);
tmp1 = zeros(ysize,xsize); %1.7*mat2gray(R(:,:,startFrame));
tmp2 = 1.7*mat2gray(G(:,:,startFrame));
tmp3 = zeros(ysize,xsize);
tmp1(L==1) = 1.7;
tmp2(L==1) = 1.7;
tmp3(L==1) = 1.7;
colorimg = cat(3,tmp1,tmp2,tmp3);
imshow(colorimg);
cd("/home/meyerct6/Repos/GCAMP_paper/Data_Code/AnalysisCode/F1")
imwrite(colorimg,'A_segmentation.png')



elliptic_axis_stretch = [1.,1.5];  %How much the ellipses should be stretched by (major,minor axis) %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%For the Labeled (L) segmentation extract the region props related to the ellipse
cellProps = regionprops(L,"Perimeter","Circularity",'Centroid',"MajorAxisLength","MinorAxisLength","Orientation","FilledArea");
ncells = length(cellProps);

%Generate colors for each label
colors = prism(ncells);
%Figure
k = cell(ncells,1);
%For each cell


%Feature matrix for Green and Red Channel
cellMeanIntG = zeros(ncells,nframes);
cellMinIntG  = zeros(ncells,nframes);
cellMaxIntG  = zeros(ncells,nframes);
cellStdIntG  = zeros(ncells,nframes);
cellMeanIntR = zeros(ncells,nframes);
cellMinIntR  = zeros(ncells,nframes);
cellMaxIntR  = zeros(ncells,nframes);
cellStdIntR  = zeros(ncells,nframes);

segArea = cell2mat({cellProps.FilledArea})';
segMajAxisLen = cell2mat({cellProps.MajorAxisLength})';
segMinAxisLen = cell2mat({cellProps.MinorAxisLength})';
segCircularity = cell2mat({cellProps.Circularity})';
segPerimeter = cell2mat({cellProps.Perimeter})';
fig = figure();
hold on
imshow(colorimg);
for i = 1:ncells
    %disp(['Calculating cell ' num2str(i) ' of ' num2str(ncells)])
    x = cellProps(i); %Get the cell information
    %Create an ROI ellipse
    if x.Orientation<0
        x.Orientation = 180+x.Orientation;
    end
    k{i} = images.roi.Ellipse(gca,...
                                'SemiAxes',[x.MajorAxisLength*elliptic_axis_stretch(1),x.MinorAxisLength*elliptic_axis_stretch(2)],...
                                'Center',x.Centroid,'RotationAngle',x.Orientation,...
                                'InteractionsAllowed','none','Color',colors(i,:),...
                                'Linewidth',1,'FaceAlpha',0.);
    %Convert ellipse to a mask
    msk                 = createMask(k{i});
    bb                  = regionprops(msk,'BoundingBox');
    xMin = ceil(bb.BoundingBox(1));
    xMax = xMin + bb.BoundingBox(3) - 1;
    yMin = ceil(bb.BoundingBox(2));
    yMax = yMin + bb.BoundingBox(4) - 1;

    tmpR                = R(yMin:yMax,xMin:xMax,:);
    tmpG                = G(yMin:yMax,xMin:xMax,:);
    msk                 = msk(yMin:yMax,xMin:xMax);
    %Find the number of pixels above brightThresh in the mask across time
    stk_msk             = double(double(tmpG).*double(msk)>G_brightThresh);
    stk_msk(stk_msk==0) = NaN; 
    %Calculate the features using matrix multiplication
    tmp = double(tmpG).*double(stk_msk);
    cellMeanIntG(i,:)   = squeeze(nanmean(tmp,[1,2]));
    cellMinIntG(i,:)    = squeeze(nanmin(tmp,[],[1,2]));
    cellMaxIntG(i,:)    = squeeze(nanmax(tmp,[],[1,2]));
    cellStdIntG(i,:)    = squeeze(nanstd(tmp,0,[1,2]));
    %Now for the red channel
    stk_msk             = double(double(tmpR).*double(msk)>R_brightThresh);
    cellArea(i,:)       = squeeze(sum(stk_msk,[1,2])); 
    stk_msk(stk_msk==0) = NaN; 
    tmp = double(tmpR).*double(stk_msk);
    cellMeanIntR(i,:)   = squeeze(nanmean(tmp,[1,2]));
    cellMinIntR(i,:)    = squeeze(nanmin(tmp,[],[1,2]));
    cellMaxIntR(i,:)    = squeeze(nanmax(tmp,[],[1,2]));
    cellStdIntR(i,:)    = squeeze(nanstd(tmp,0,[1,2]));
end


sdMatG = movstd((cellMeanIntG./movmedian(cellMeanIntG',45)')',30)';
normMeanIntG = cellMeanIntG./movmedian(cellMeanIntG',45)';

frame = getframe(fig);
imwrite(frame.cdata,'B_segmentation_ellipses.png')


i = 15;
fig = figure('Color','w','Position',[900 1200 300 260]);
ax1 = subplot(2,1,1);
k = i;
plot(normMeanIntG(k,:),'LineWidth',2,'Color',[0.65,0,.65]);
xticks([])
yticks([])
set(gca,'Linewidth',2,'FontSize',8,'FontName','Arial')
ax2 = subplot(2,1,2);
plot(sdMatG(k,:),'LineWidth',2,'Color',[0.65,0,.65]);
xticks([])
yticks([])
set(gca,'Linewidth',2,'FontSize',8,'FontName','Arial')
saveas(fig,'example_traces.pdf')

i = 45
fig = figure('Color','w','Position',[900 1200 300 260]);
ax1 = subplot(2,1,1);
k = i;
plot(normMeanIntG(k,:),'LineWidth',2,'Color','c');
xticks([])
yticks([])
set(gca,'Linewidth',2,'FontSize',8,'FontName','Arial')
ax2 = subplot(2,1,2);
plot(sdMatG(k,:),'LineWidth',2,'Color','c');
xticks([])
yticks([])
set(gca,'Linewidth',2,'FontSize',8,'FontName','Arial')
saveas(fig,'example_tracesv2.pdf')


