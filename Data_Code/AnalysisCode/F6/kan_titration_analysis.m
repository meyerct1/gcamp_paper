%Change into the correct directory
cd /home/meyerct6/Repos/GCAMP_paper/Data_Code/AnalysisCode/F6
addpath(genpath('/home/meyerct6/Repos/GCAMP_paper/Data_Code/Functions'))

%% Step 1: Read in data generated from the "extract_GMmS_TIF_2color_parallel.m" file
%Clear everything and start a timer
clear; close all; tic
%Load in data
load('../../Datasets/02262021_AllSingleCellInfoData.mat')


%% For different deltaT estimate the predictivity of the RandomForest learning algorithms for different concentrations
%Calculate feature matrices based on 1:deltaT frames
deltaT = 10:10:240;
kFoldCnt = 10;
indx = find(((kan_r==1)&(contains(drugNm,'kan'))) | (((kan_r==0)&(drug_conc==100)&(contains(drugNm,'kan'))) | ((kan_r==0)&(drug_conc==0))));
conc = unique(drug_conc(indx)); 
sensitiveCells = zeros(length(conc),length(deltaT),kFoldCnt);

numWorkers = 24;
p = gcp('nocreate'); % If no pool, do not create new one. ~14Gb/exp
if isempty(p)
    parpool(numWorkers);
end
%For each concentration...
for cc = 1:length(conc)
    parfor dT = 1:length(deltaT)
        indx = find(((kan_r==1)&(contains(drugNm,'kan'))) | (((kan_r==0)&(drug_conc==100)&(contains(drugNm,'kan'))) | ((kan_r==0)&(drug_conc==0))));
        rng(5); %Set random number generator for reproducibility
        disp(['Model: ' num2str(deltaT(dT)) ', Conc: ' num2str(conc(cc))])
        S = struct();
        S.meanIntG = intMatG(indx,1:deltaT(dT));
        S.maxIntG = maxIntMatG(indx,1:deltaT(dT));
        S.minIntG = minIntMatG(indx,1:deltaT(dT));
        S.stdIntG = minIntMatG(indx,1:deltaT(dT));
        
        %Calculate Feature Matrix:
        [master_data,mastLab] = calcFeatureMatrix(S,deltaT(dT));

        %Use label cells Label cells as either Resistant or Sensitive.  Include untreated
        %sensitive as resistant cells!
        tmp_kan_r = kan_r(indx);tmp_drug_conc=drug_conc(indx);tmp_drug_nm = drugNm(indx);
        master_label = ones(length(tmp_kan_r),1);
        %Sensitive cells are zero
        master_label((tmp_kan_r==0)&(tmp_drug_conc==100)&(contains(tmp_drug_nm,'kan'))) = 0;
        %Normalize data
        mu_n = mean(master_data);std_n = std(master_data);
        master_data = (master_data-mu_n)./std_n;

        keep_cols = find(sum(isnan(master_data))~=length(master_data));
        master_data = master_data(:,keep_cols);
        master_label = master_label(sum(isnan(master_data'))==0);
        master_data = master_data(sum(isnan(master_data'))==0,:);
        %Uncomment to examine how class labels separate
        [coeff,score,~,~,explained,mu] = pca(master_data);

        %PCA downsampling...
        %Find the index that retains 95% of the variance
        expl = 0;pca_dim = 0;
        for i=1:length(explained)
            expl = expl+explained(i);
            if expl>95
                pca_dim = i;
                break
            end
        end
        master_data = score(:,1:pca_dim);
        Mdl = {};
        for j = 1:kFoldCnt
            %Keep an equal amount of sensitive and resistant cells for training
            mn = min([sum(master_label==1),sum(master_label==0)]);
            idx_sens = find(master_label==0);idx_res = find(master_label==1);
            idx_sens = idx_sens(randperm(length(idx_sens)));
            idx_res = idx_res(randperm(length(idx_res)));
            idx_sens = idx_sens(1:mn);idx_res=idx_res(1:mn);
            idx = union(idx_res,idx_sens);
            tmp_master_data = master_data(idx,:);
            tmp_master_label = master_label(idx);

            %Use 80% of the data for training the model
            spt = rand(length(tmp_master_label),1)> 0.8;
            test = tmp_master_data(spt,:);
            train = tmp_master_data(spt==0,:);
            test_lab = tmp_master_label(spt,:);
            train_lab = tmp_master_label(spt==0,:);
            %Set up template Tree
            %See https://www.mathworks.com/help/stats/templatetree.html for list of
            %hyperparameters
            %%%SET PARAMETERS HERE!!!!!!
            t = templateTree('Reproducible',true,'MinLeafSize',7,'MaxNumSplits',34);
            Mdl1 = fitcensemble(train,train_lab,'Method','AdaBoostM1','NumLearningCycles',442,'LearnRate',0.94414,'NPrint',0);
            Mdl{j} = Mdl1;
        end


        %Now find the correct concentration:
        indx = find((kan_r==0)&(drug_conc==conc(cc))&(contains(drugNm,'kan')));

        S = struct();
        S.meanIntG = intMatG(indx,1:deltaT(dT));
        S.maxIntG = maxIntMatG(indx,1:deltaT(dT));
        S.minIntG = minIntMatG(indx,1:deltaT(dT));
        S.stdIntG = minIntMatG(indx,1:deltaT(dT));
        
        %Calculate Feature Matrix:
        [master_data,mastLab] = calcFeatureMatrix(S,deltaT(dT));

        %Normalize data using the same mu and sigma as original datawset
        master_data = (master_data-mu_n)./std_n;

        master_data = master_data(:,keep_cols);
        master_data = master_data(sum(isnan(master_data'))==0,:);
        %Reapply PCA
        master_data = (master_data - mu)*coeff(:,1:pca_dim);
        for j=1:kFoldCnt
            [predict_labels,predict_score] = predict(Mdl{j},master_data);
            sensitiveCells(cc,dT,j) = sum(predict_labels==0)/length(predict_labels);
        end
    end
end

save('titration_kan_sensitive.mat','sensitiveCells','conc','deltaT')


fig = figure('Color','w','Position',[900 1200 450 250]);
ax1 = subplot(1,1,1); hold on
time_pts = 1:1:length(deltaT);
cm_plasma = plasma(length(time_pts));
for t = 1:length(time_pts)
    shadedErrorBar(1:length(conc),squeeze(sensitiveCells(:,t,:))'*100,{@mean,@std},'lineProps',{'color',cm_plasma(t,:),'markerfacecolor',cm_plasma(t,:),'linewidth',2})
    if t==1
        xlabel('[Kan (\mug/mL)]')
        ylabel('% Sensitive')
        xticks(1:length(conc))
        xticklabels(conc)
    end
end
colormap(cm_plasma)
cb = colorbar();
time_pts = 1:2:length(deltaT);
cb.Ticks = linspace(min(cb.Ticks),max(cb.Ticks),length(time_pts));
cb.TickLabels = deltaT(time_pts);
cb.FontSize = 8;
cb.Label.String = '\Delta t';
set(gca,'Linewidth',2,'FontSize',8,'FontName','Arial')
saveas(fig,'deltaT_kan_gcamp_titration.pdf')





figure()
plot(1:length(conc),squeeze(sensitiveCells(:,end,:))'*100)

