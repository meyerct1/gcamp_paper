%Change into the correct directory
cd '~/Repos/GCAMP_paper/Data_Code/Figure 5/'

%% Step 1: Read in data generated from the "extract_GMmS_TIF_2color_parallel.m" file
%Clear everything and start a timer
clear; close all; tic
saveModel_name = '022321_baggedTree';      %Name of models
%Set active directory
activeDir      = '/mnt/kralj_celery/E coli GC antibioitic/*/2021-01-15_Results/matFiles/';
flist          = dir([activeDir '*' filesep '*data.mat']); %Current file list
nfiles         = length(flist); %Number of files

tmp = cellfun(@(x) strsplit(x,'/'),{flist.folder},'UniformOutput',false);
tmp = vertcat(tmp{:});
tmp = tmp(:,5);
disp('Using experiments:')
unique(tmp)


%Read in the feature matrices
cellCnt = 0;
for i = 1:length(flist)
    load([flist(i).folder filesep flist(i).name])
    cellCnt = cellCnt + S.ncells;
end
nframes        = size(S.cellMeanIntG,2);

%Pre allocate features matricies
drug_conc = zeros(cellCnt,1);kan_r=zeros(cellCnt,1);drugNm = cell(cellCnt,1);expNm = cell(cellCnt,1);wellNm = cell(cellCnt,1);  %Information on drug treatment and resistance status
cellArea  = zeros(cellCnt,nframes);    %Change in cell area over time
%Feature matrices on the trajectories for both Red and Green ch
intMatG   = zeros(cellCnt,nframes); minIntMatG = zeros(cellCnt,nframes); maxIntMatG = zeros(cellCnt,nframes); stdIntMatG = zeros(cellCnt,nframes);
intMatR   = zeros(cellCnt,nframes); minIntMatR = zeros(cellCnt,nframes); maxIntMatR = zeros(cellCnt,nframes); stdIntMatR = zeros(cellCnt,nframes);
matbckG = zeros(length(S.time),length(flist));matbckR = zeros(length(S.time),length(flist));
%Feature matrix on the segmentation used to filter cells
segArea   = zeros(cellCnt,1);segPerimeter=zeros(cellCnt,1);segMajAxisLen=zeros(cellCnt,1);segMinAxisLen=zeros(cellCnt,1);segCircularity=zeros(cellCnt,1);
%For each mat file, load and append the data to the appropriate matrix
cellCnt = [1 1];
for i = 1:length(flist)
    disp(['Loading file ' num2str(i)])
    load([flist(i).folder filesep flist(i).name])
    if ~isempty(S.cellMeanIntG)
        cellCnt(2) = cellCnt(1)+S.ncells-1;
        matbckG(:,i)                          = S.bckG;
        matbckR(:,i)                          = S.bckR;        
        intMatG(cellCnt(1):cellCnt(2),:)      = S.cellMeanIntG-S.bckG';  %Subtract the background!
        intMatR(cellCnt(1):cellCnt(2),:)      = S.cellMeanIntR-S.bckR';  %Subtract the background!
        minIntMatG(cellCnt(1):cellCnt(2),:)   = S.cellMinIntG;
        minIntMatR(cellCnt(1):cellCnt(2),:)   = S.cellMinIntR;
        maxIntMatG(cellCnt(1):cellCnt(2),:)   = S.cellMaxIntG;
        maxIntMatR(cellCnt(1):cellCnt(2),:)   = S.cellMaxIntR;
        stdIntMatG(cellCnt(1):cellCnt(2),:)   = S.cellStdIntG;
        stdIntMatR(cellCnt(1):cellCnt(2),:)   = S.cellStdIntR;
        cellArea(cellCnt(1):cellCnt(2),:)     = S.cellArea;
        drug_conc(cellCnt(1):cellCnt(2))      = ones(S.ncells,1)*S.currConc;
        kan_r(cellCnt(1):cellCnt(2))          = ones(S.ncells,1)*contains(S.currCell,'KanR');
        tmp = split(flist(i).folder,filesep);
        tmp1 = tmp{end};
        tmp2 = tmp{end-3};
        for j = cellCnt(1):cellCnt(2)
            drugNm{j}                         = S.currDrug;
            wellNm{j}                         = tmp1;
            expNm{j}                          = tmp2;
        end
        segArea(cellCnt(1):cellCnt(2))        = S.segArea;
        segMajAxisLen(cellCnt(1):cellCnt(2))  = S.segMajAxisLen;
        segMinAxisLen(cellCnt(1):cellCnt(2))  = S.segMinAxisLen;
        segCircularity(cellCnt(1):cellCnt(2)) = S.segCircularity;
        segPerimeter(cellCnt(1):cellCnt(2))   = S.segPerimeter;
        cellCnt(1)                            = cellCnt(2)+1;
    end
end
%Deal with the strange error on circularity being infinite
segCircularity(abs(segCircularity)==Inf)=0;


%% Step 2:  "Gate" the cells based on different requirements
%Remove cells which vanished from segmentation (matArea==0)
indx    = find(sum(cellArea'==0)>0);
%Fillmissing values if possible using movmedian over 15 frames
intMatG = fillmissing(intMatG','movmean',15)';
intMatR = fillmissing(intMatR','movmean',15)';
%If still undefined, remove 
indx    = union(indx,find(sum(isnan(intMatG'))>0));
indx    = union(indx,find(sum(isnan(intMatR'))>0));
%Remove cells which have segmentation parameter too different
dat = [segArea,segMajAxisLen,segMinAxisLen,segCircularity,segPerimeter];
dat = normalize(dat);
[~,score,~,~,explained,~] = pca(dat); %Run PCA to visualize what cells are being discarded
dist_mat = mean(squareform(pdist(score)));
data_keep = .98;  %What percentage to keep?  #############################
tmp = sort(dist_mat);
indx  = union(indx,find(dist_mat>tmp(floor(length(dist_mat)*data_keep))));
%Visualize cells
fig = figure('color','w');
hold on
scatter(score(:,1),score(:,2)); 
scatter(score(indx,1),score(indx,2));
legend({'keep','remove'})
xlabel('PCA1');ylabel('PCA2');
saveas(fig,'MultiDim_gating')

%Which indices to keep
to_keep    = setdiff(1:length(dist_mat),indx);
intMatG    = intMatG(to_keep,:);
intMatR    = intMatR(to_keep,:);
minIntMatG = minIntMatG(to_keep,:);
minIntMatR = minIntMatR(to_keep,:);
maxIntMatG = maxIntMatG(to_keep,:);
maxIntMatR = maxIntMatR(to_keep,:);
stdIntMatG = stdIntMatG(to_keep,:);
stdIntMatR = stdIntMatR(to_keep,:);
cellArea   = cellArea(to_keep,:);
drug_conc  = drug_conc(to_keep);
kan_r      = kan_r(to_keep);
drugNm     = drugNm(to_keep);
wellNm     = wellNm(to_keep);
expNm      = expNm(to_keep);



%% For different deltaT estimate the predictivity of the RandomForest learning algorithms
%Calculate feature matrices based on 1:deltaT frames
deltaT = 10:10:240;
kFoldCnt = 10;
indx = find(((kan_r==1)&(contains(drugNm,'kan'))) | (((kan_r==0)&(drug_conc==100)&(contains(drugNm,'kan'))) | ((kan_r==0)&(drug_conc==0))));
conc = unique(drug_conc(indx)); 
sensitiveCells = zeros(length(conc),length(deltaT),kFoldCnt);

numWorkers = 24;
p = gcp('nocreate'); % If no pool, do not create new one. ~14Gb/exp
if isempty(p)
    parpool(numWorkers);
end

for cc = 1:length(conc)
    parfor dT = 1:length(deltaT)
        indx = find(((kan_r==1)&(contains(drugNm,'kan'))) | (((kan_r==0)&(drug_conc==100)&(contains(drugNm,'kan'))) | ((kan_r==0)&(drug_conc==0))));
        rng(5); %Set random number generator for reproducibility
        disp(['Model: ' num2str(deltaT(dT)) ', Conc: ' num2str(conc(cc))])
        S = struct();
        S.meanIntG = intMatG(indx,1:deltaT(dT));
        S.maxIntG = maxIntMatG(indx,1:deltaT(dT));
        S.minIntG = minIntMatG(indx,1:deltaT(dT));
        S.stdIntG = minIntMatG(indx,1:deltaT(dT));
        nms = fieldnames(S);
        master_data  = []; mastLab=[];
        for j = 1:length(nms)
            [mat1,labs1] = calcTrajFeatures(S.(nms{j}));
            for l=1:length(labs1)
                labs1{l} = [labs1{l} '_' nms{j}];
            end
            master_data = [master_data,mat1];
            mastLab = [mastLab,labs1];
            for i=[5,10]
                [mat1,labs1] = calcTrajFeatures(movmean(S.(nms{j}),ceil(deltaT(dT)/i),2));
                for l=1:length(labs1)
                    labs1{l} = [labs1{l} '_' nms{j} '_movmean_deltaT_window_' num2str(ceil(deltaT(dT)/i))];
                end
                master_data = [master_data,mat1];
                mastLab = [mastLab,labs1];
            end
            for i=[5,10]
                [mat1,labs1] = calcTrajFeatures(movstd(S.(nms{j}),ceil(deltaT(dT)/i),[],2));
                for l=1:length(labs1)
                    labs1{l} = [labs1{l} '_' nms{j} '_movstd_deltaT_window_' num2str(ceil(deltaT(dT)/i))];
                end
                master_data = [master_data,mat1];
                mastLab = [mastLab,labs1];
            end

            %Now normalize matrix to T==0
            tmp = S.(nms{j});tmp = tmp./tmp(:,1);
            [mat1,labs1] = calcTrajFeatures(tmp);
            for l=1:length(labs1)
                labs1{l} = [labs1{l} '_' nms{j} '_norm'];
            end
            master_data = [master_data,mat1];
            mastLab = [mastLab,labs1];
            for i=[5,10]
                [mat1,labs1] = calcTrajFeatures(movmean(tmp,ceil(deltaT(dT)/i),2));
                for l=1:length(labs1)
                    labs1{l} = [labs1{l} '_' nms{j} '_norm_movmean_deltaT_window_' num2str(ceil(deltaT(dT)/i))];
                end
                master_data = [master_data,mat1];
                mastLab = [mastLab,labs1];
            end
            for i=[5,10]
                [mat1,labs1] = calcTrajFeatures(movstd(tmp,ceil(deltaT(dT)/i),[],2));
                for l=1:length(labs1)
                    labs1{l} = [labs1{l} '_' nms{j} '_norm_movstd_deltaT_window_' num2str(ceil(deltaT(dT)/i))];
                end
                master_data = [master_data,mat1];
                mastLab = [mastLab,labs1];
            end
        end

        %Use label cells Label cells as either Resistant or Sensitive.  Include untreated
        %sensitive as resistant cells!
        tmp_kan_r = kan_r(indx);tmp_drug_conc=drug_conc(indx);tmp_drug_nm = drugNm(indx);
        master_label = ones(length(tmp_kan_r),1);
        %Sensitive cells are zero
        master_label((tmp_kan_r==0)&(tmp_drug_conc==100)&(contains(tmp_drug_nm,'kan'))) = 0;
        %Normalize data
        mu_n = mean(master_data);std_n = std(master_data);
        master_data = (master_data-mu_n)./std_n;

        keep_cols = find(sum(isnan(master_data))~=length(master_data));
        master_data = master_data(:,keep_cols);
        master_label = master_label(sum(isnan(master_data'))==0);
        master_data = master_data(sum(isnan(master_data'))==0,:);
        %Uncomment to examine how class labels separate
        [coeff,score,~,~,explained,mu] = pca(master_data);

        %PCA downsampling...
        %Find the index that retains 95% of the variance
        expl = 0;pca_dim = 0;
        for i=1:length(explained)
            expl = expl+explained(i);
            if expl>95
                pca_dim = i;
                break
            end
        end
        master_data = score(:,1:pca_dim);
        Mdl = {};
        for j = 1:kFoldCnt
            %Keep an equal amount of sensitive and resistant cells for training
            mn = min([sum(master_label==1),sum(master_label==0)]);
            idx_sens = find(master_label==0);idx_res = find(master_label==1);
            idx_sens = idx_sens(randperm(length(idx_sens)));
            idx_res = idx_res(randperm(length(idx_res)));
            idx_sens = idx_sens(1:mn);idx_res=idx_res(1:mn);
            idx = union(idx_res,idx_sens);
            tmp_master_data = master_data(idx,:);
            tmp_master_label = master_label(idx);

            %Use 80% of the data for training the model
            spt = rand(length(tmp_master_label),1)> 0.8;
            test = tmp_master_data(spt,:);
            train = tmp_master_data(spt==0,:);
            test_lab = tmp_master_label(spt,:);
            train_lab = tmp_master_label(spt==0,:);
            %Set up template Tree
            %See https://www.mathworks.com/help/stats/templatetree.html for list of
            %hyperparameters
            %%%SET PARAMETERS HERE!!!!!!
            t = templateTree('Reproducible',true,'MinLeafSize',7,'MaxNumSplits',34);
            Mdl1 = fitcensemble(train,train_lab,'Method','AdaBoostM1','NumLearningCycles',442,'LearnRate',0.94414,'NPrint',0);
            Mdl{j} = Mdl1;
        end


        %Now find the correct concentration:
        indx = find((kan_r==0)&(drug_conc==conc(cc))&(contains(drugNm,'kan')));

        S = struct();
        S.meanIntG = intMatG(indx,1:deltaT(dT));
        S.maxIntG = maxIntMatG(indx,1:deltaT(dT));
        S.minIntG = minIntMatG(indx,1:deltaT(dT));
        S.stdIntG = minIntMatG(indx,1:deltaT(dT));
        nms = fieldnames(S);
        master_data  = []; mastLab=[];
        for j = 1:length(nms)
            [mat1,labs1] = calcTrajFeatures(S.(nms{j}));
            for l=1:length(labs1)
                labs1{l} = [labs1{l} '_' nms{j}];
            end
            master_data = [master_data,mat1];
            mastLab = [mastLab,labs1];
            for i=[5,10]
                [mat1,labs1] = calcTrajFeatures(movmean(S.(nms{j}),ceil(deltaT(dT)/i),2));
                for l=1:length(labs1)
                    labs1{l} = [labs1{l} '_' nms{j} '_movmean_deltaT_window_' num2str(ceil(deltaT(dT)/i))];
                end
                master_data = [master_data,mat1];
                mastLab = [mastLab,labs1];
            end
            for i=[5,10]
                [mat1,labs1] = calcTrajFeatures(movstd(S.(nms{j}),ceil(deltaT(dT)/i),[],2));
                for l=1:length(labs1)
                    labs1{l} = [labs1{l} '_' nms{j} '_movstd_deltaT_window_' num2str(ceil(deltaT(dT)/i))];
                end
                master_data = [master_data,mat1];
                mastLab = [mastLab,labs1];
            end

            %Now normalize matrix to T==0
            tmp = S.(nms{j});tmp = tmp./tmp(:,1);
            [mat1,labs1] = calcTrajFeatures(tmp);
            for l=1:length(labs1)
                labs1{l} = [labs1{l} '_' nms{j} '_norm'];
            end
            master_data = [master_data,mat1];
            mastLab = [mastLab,labs1];
            for i=[5,10]
                [mat1,labs1] = calcTrajFeatures(movmean(tmp,ceil(deltaT(dT)/i),2));
                for l=1:length(labs1)
                    labs1{l} = [labs1{l} '_' nms{j} '_norm_movmean_deltaT_window_' num2str(ceil(deltaT(dT)/i))];
                end
                master_data = [master_data,mat1];
                mastLab = [mastLab,labs1];
            end
            for i=[5,10]
                [mat1,labs1] = calcTrajFeatures(movstd(tmp,ceil(deltaT(dT)/i),[],2));
                for l=1:length(labs1)
                    labs1{l} = [labs1{l} '_' nms{j} '_norm_movstd_deltaT_window_' num2str(ceil(deltaT(dT)/i))];
                end
                master_data = [master_data,mat1];
                mastLab = [mastLab,labs1];
            end
        end

        %Normalize data using the same mu and sigma as original datawset
        master_data = (master_data-mu_n)./std_n;

        master_data = master_data(:,keep_cols);
        master_data = master_data(sum(isnan(master_data'))==0,:);
        %Reapply PCA
        master_data = (master_data - mu)*coeff(:,1:pca_dim);
        for j=1:kFoldCnt
            [predict_labels,predict_score] = predict(Mdl{j},master_data);
            sensitiveCells(cc,dT,j) = sum(predict_labels==0)/length(predict_labels);
        end
    end
end

save('titration_kan_sensitive.mat','sensitiveCells','conc','deltaT')


fig = figure('Color','w','Position',[900 1200 450 250]);
ax1 = subplot(1,1,1); hold on
time_pts = 12:1:length(deltaT);
cm_plasma = plasma(length(time_pts));
for t = 1:length(time_pts)
    shadedErrorBar(1:length(conc),squeeze(sensitiveCells(:,time_pts(t),:))'*100,{@mean,@std},'lineProps',{'color',cm_plasma(t,:),'markerfacecolor',cm_plasma(t,:),'linewidth',2})
    if t==1
        xlabel('[Kan (\mug/mL)]')
        ylabel('% Sensitive')
        xticks(1:length(conc))
        xticklabels(conc)
    end
end
colormap(cm_plasma)
cb = colorbar();
time_pts = 12:2:length(deltaT);
cb.Ticks = linspace(min(cb.Ticks),max(cb.Ticks),length(time_pts));
cb.TickLabels = deltaT(time_pts);
cb.FontSize = 8;
cb.Label.String = '\Delta t';
set(gca,'Linewidth',2,'FontSize',8,'FontName','Arial')
saveas(fig,'deltaT_kan_gcamp_titration.pdf')





figure()
plot(1:length(conc),squeeze(sensitiveCells(:,end,:))'*100)

