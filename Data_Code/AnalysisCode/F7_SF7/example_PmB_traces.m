cd /home/meyerct6/Repos/GCAMP_paper/Data_Code/AnalysisCode/F7_SF7
%Load in data
load('../../Datasets/02262021_PmB-PI-InfoData.mat')


S.time = 1:241;
endTime = 241;
%Example PmB traces
y = intMatG(drug_conc==10,:);

y = y-mean(y(:,10:20),2);
rng(14)
fig = figure('Color','w','Position',[100 100 150 150]);
cm_colors = parula(3);cnt =1;hold on
for i = randperm(length(y),3)
    plot(S.time,y(i,:)+150*cnt,'color',cm_colors(cnt,:),'linewidth',2)
    cnt = cnt + 1;
end
xlim([0,240])
xlabel('Time(min)')
ylabel('GCaMP6 (AU)')
yticks([])
xticklabels([0 120 240])
set(gca,'Fontsize',8,'FontName','Arial','Linewidth',2)
saveas(fig,['plots' filesep 'PmxB_example_trace.pdf'])
