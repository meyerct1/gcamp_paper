%Change into the correct directory
cd /home/meyerct6/Repos/GCAMP_paper/Data_Code/AnalysisCode/F7_SF7
addpath(genpath('/home/meyerct6/Repos/GCAMP_paper/Data_Code/Functions'))

%% Step 1: Read in data generated from the "extract_GMmS_TIF_2color_parallel.m" file
%Clear everything and start a timer
clear; close all; tic
%Load in data
load('../../Datasets/02262021_AllSingleCellInfoData.mat')



%% For the full time course (deltaT=241) find the best approach using the HyperOptimization in Matlab
deltaT = [241]; dT = 1;

%Loop through the different drugs
drugs = {'cipro','chlor','trim','PmB','gent'};
maxConc = [10,100,10,3,100,10];

for vv = 1:length(drugs)
    %Subset out the kanamycin treated and untreated cells to train model
    indx = find((((drug_conc==maxConc(vv))&(contains(drugNm,drugs{vv}))) | ((kan_r==0)&(drug_conc==0))));
    S = struct();
    S.meanIntG = intMatG(indx,1:deltaT(dT));
    S.maxIntG = maxIntMatG(indx,1:deltaT(dT));
    S.minIntG = minIntMatG(indx,1:deltaT(dT));
    S.stdIntG = minIntMatG(indx,1:deltaT(dT));
    %Calculate Feature Matrix:
    [master_data,mastLab] = calcFeatureMatrix(S);
    
    %Use label cells Label cells as either Resistant or Sensitive.  Include untreated
    %sensitive as resistant cells!
    tmp_kan_r = kan_r(indx);tmp_drug_conc=drug_conc(indx);tmp_drug_nm = drugNm(indx);
    master_label = ones(length(tmp_kan_r),1);
    %Sensitive cells are zero
    master_label((tmp_drug_conc==maxConc(vv))&(contains(tmp_drug_nm,drugs{vv}))) = 0;

    %Normalize data
    master_data = normalize(master_data);
    %Remove nan cells and columns    
    master_data = master_data(:,sum(isnan(master_data))~=length(master_data));
    master_label = master_label(sum(isnan(master_data'))==0);
    master_data = master_data(sum(isnan(master_data'))==0,:);

    %Uncomment to examine how class labels separate
    [~,score,~,~,explained,~] = pca(master_data);

    %PCA downsampling...
    %Find the index that retains 95% of the variance
    expl = 0;pca_dim = 0;
    for i=1:length(explained)
        expl = expl+explained(i);
        if expl>95
            pca_dim = i;
            break
        end
    end
    master_data = score(:,1:pca_dim);

    %Enforce equal representation of sensitive and resistant cells
    mn = min([sum(master_label==1),sum(master_label==0)]);
    idx_sens = find(master_label==0);idx_res = find(master_label==1);
    idx_sens = idx_sens(randperm(length(idx_sens)));
    idx_res = idx_res(randperm(length(idx_res)));
    idx_sens = idx_sens(1:mn);idx_res=idx_res(1:mn);
    idx = union(idx_res,idx_sens);
    master_data = master_data(idx,:);
    master_label = master_label(idx);
            
    %% Hyperparameter sweep
    %Split the data into training and testing
    rng(5)
    disp(['Starting Model Learning, drug: ' drugs{vv}])
    %Use only 5% of the data for training the model
    spt = rand(length(master_data),1)> 0.8;
    test = master_data(spt,:);
    train = master_data(spt==0,:);
    test_lab = master_label(spt,:);
    train_lab = master_label(spt==0,:);
    %Set up template Tree
    %See https://www.mathworks.com/help/stats/templatetree.html for list of
    %hyperparameters
    numWorkers = 24;
    p = gcp('nocreate'); % If no pool, do not create new one. ~14Gb/exp
    if isempty(p)
        parpool(numWorkers);
    end
    t = templateTree('Reproducible',true);

    %Find the hyperparameter set that is optimal for each drug, run a subsequent
    %hyperoptimization for number of learning cycles, learn rate, maxnumsplits
    %Use AdaBoostM1
    Mdl1 = fitcensemble(train,train_lab,'Method','AdaBoostM1','OptimizeHyperparameters',{'NumLearningCycles','LearnRate','MaxNumSplits','MinLeafSize'},'Learners',t,...
                                                    'HyperparameterOptimizationOptions',struct( 'MaxObjectiveEvaluations',200,...
                                                            'UseParallel',true,...
                                                            'Repartition',true));
    saveas(gcf,['hyperparameter-search' filesep 'Mdl1_' drugs{vv} '_HyperParameterSearch-02232021.fig'])  
    save(['hyperparameter-search' filesep 'Mdl1_' drugs{vv} '_HyperParameterSearch-02232021.mat'],'Mdl1','-v7.3')
    disp(['Finished Model Learning, drug: ' drugs{vv}])
end
