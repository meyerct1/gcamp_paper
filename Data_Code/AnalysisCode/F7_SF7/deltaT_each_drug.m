%Change into the correct directory
cd /home/meyerct6/Repos/GCAMP_paper/Data_Code/AnalysisCode/F7_SF7
addpath(genpath('/home/meyerct6/Repos/GCAMP_paper/Data_Code/Functions'))

%% Step 1: Read in data generated from the "extract_GMmS_TIF_2color_parallel.m" file
%Clear everything and start a timer
clear; close all; tic
%Load in data
load('../../Datasets/02262021_AllSingleCellInfoData.mat')

%%
%Loop through the different drugs
drugs = {'cipro','chlor','trim','PmB','gent'};
maxConc = [10,100,10,10,100];
%Optimal hyperparameters (SEE PNG images in this folder for values
nlc = [217,    482,    423,    436,    387];
lr  = [0.40054,0.69145,0.90351,0.59427,0.82293];
mls = [177,    23,     117,    10,     55];
mns = [5519,   15,     64,     58,     6];

for vv = 1:length(maxConc)
    %% For the full time course (deltaT=241) fit the model
    deltaT = [241]; dT = 1;
    %Subset out the kanamycin treated and untreated cells to train model
    indx = find((((drug_conc==maxConc(vv))&(contains(drugNm,drugs{vv}))) | ((kan_r==0)&(drug_conc==0))));
    S = struct();
    S.meanIntG = intMatG(indx,1:deltaT(dT));
    S.maxIntG = maxIntMatG(indx,1:deltaT(dT));
    S.minIntG = minIntMatG(indx,1:deltaT(dT));
    S.stdIntG = minIntMatG(indx,1:deltaT(dT));
    %Calculate Feature Matrix:
    [master_data,mastLab] = calcFeatureMatrix(S,deltaT(dT));

    %Use label cells Label cells as either Resistant or Sensitive.  Include untreated
    %sensitive as resistant cells!
    tmp_kan_r = kan_r(indx);tmp_drug_conc=drug_conc(indx);tmp_drug_nm = drugNm(indx);
    master_label = ones(length(tmp_kan_r),1);
    %Sensitive cells are zero
    master_label((tmp_drug_conc==maxConc(vv))&(contains(tmp_drug_nm,drugs{vv}))) = 0;

    %Normalize data
    master_data = normalize(master_data);
    %Remove nan cells and columns    
    master_data = master_data(:,sum(isnan(master_data))~=length(master_data));
    master_label = master_label(sum(isnan(master_data'))==0);
    master_data = master_data(sum(isnan(master_data'))==0,:);

    %Uncomment to examine how class labels separate
    [~,score,~,~,explained,~] = pca(master_data);

    %PCA downsampling...
    %Find the index that retains 95% of the variance
    expl = 0;pca_dim = 0;
    for i=1:length(explained)
        expl = expl+explained(i);
        if expl>95
            pca_dim = i;
            break
        end
    end
    master_data = score(:,1:pca_dim);

    %% Fit full model
    %Split the data into training and testing
    rng(5)
    disp(['Starting Model Learning, drug: ' drugs{vv}])
    %Use only 5% of the data for training the model
    spt = rand(length(master_data),1)> 0.8;
    test = master_data(spt,:);
    train = master_data(spt==0,:);
    test_lab = master_label(spt,:);
    train_lab = master_label(spt==0,:);
    %Set up template Tree
    %See https://www.mathworks.com/help/stats/templatetree.html for list of
    %hyperparameters
    numWorkers = 24;
    p = gcp('nocreate'); % If no pool, do not create new one. ~14Gb/exp
    if isempty(p)
        parpool(numWorkers);
    end

    kFoldCnt = 10;
    %% Select best model!
    %Best according to hyperparameter optimization
    t = templateTree('Reproducible',true,'MinLeafSize',mls(vv),'MaxNumSplits',mns(vv));
    Mdl2 = fitcensemble(train,train_lab,'Method','AdaBoostM1','NumLearningCycles',nlc(vv),'LearnRate',lr(vv),'CrossVal','on','KFold',kFoldCnt,'NPrint',0);
    save(['models' filesep 'Mdl2_' drugs{vv} '.mat'],'Mdl2','-v7.3')

    modelLosses = kfoldLoss(Mdl2,'mode','individual');
    [predict_labels,predict_score] = predict(Mdl2.Trained{find(modelLosses==min(modelLosses))}, test);
    %% Plots for best model
    auc = zeros(2,1);
    fig = figure('Color','w','Position',[900 1200 215 130]);
    ax = subplot(1,1,1);
    hold on
    for k = 0
        [xVal,yVal,~,auc(k+1)] = perfcurve(test_lab(1:end),predict_score(1:end,k+1),k);
        stp = round(length(xVal)*.01);
        plot(ax,xVal(1:stp:end),yVal(1:stp:end),'Linewidth',2);
        text(0.25,.15*(k+1),strcat('AUC=',num2str(auc(k+1))),'FontSize',8);
    end
    plot(xVal(1:stp:end),xVal(1:stp:end),'--r','linewidth',2)
    xlabel('False Positive Rate');
    ylabel('True Positive Rate');
    title({['Cell count: ' num2str(length(master_data))], ['Sensitive: ' num2str(sum(master_label)==0)]})
    set(gca,'Linewidth',2,'FontSize',8,'FontName','Arial')
    saveas(fig,['plots' filesep 'fullTime_' drugs{vv} '_roc.pdf'])


    cp = classperf(test_lab,predict_labels);
    Mdl = Mdl2;

    %Plot the loss as a function of training rounds
    kflc = kfoldLoss(Mdl,'Mode','cumulative');
    fig = figure('Color','w','Position',[900 1200 215 130]);
    plot(kflc*100,'linewidth',2);
    ylabel({'10-fold','Error rate'});
    xlabel('Learning cycle');
    set(gca,'Linewidth',2,'FontSize',8,'FontName','Arial')
    tightfig
    saveas(fig,['plots' filesep 'fullTime_' drugs{vv} '_learningCycle.pdf'])

    fig = figure('Color','w','Position',[900 1200 215 130]);
    plot(modelLosses,'linewidth',2);
    xlabel('Fold')
    ylabel('Model Loss')
    set(gca,'Linewidth',2,'FontSize',8,'FontName','Arial')
    saveas(fig,['fullTime_' drugs{vv} '_kfoldloss.pdf'])

    %Plot confusion chart
    fig = figure('Color','w','Position',[900 1200 215 130]);
    [predict_labels,predict_score] = predict(Mdl.Trained{find(modelLosses==min(modelLosses))}, test);
    c = confusionchart(confusionmat(test_lab,predict_labels),{'Sens','Res'},'Normalization','row-normalized');
    set(gca,'FontSize',8,'FontName','Arial')
    saveas(fig,['plots' filesep 'fullTime_' drugs{vv} '_confusionMat.pdf'])

    %% For different deltaT estimate the predictivity of the RandomForest learning algorithms
    %Calculate feature matrices based on 1:deltaT frames
    deltaT = 10:2:240;
    kFoldCnt = 10;
    aucComp = zeros(length(deltaT),kFoldCnt);
    errorRate = zeros(length(deltaT),kFoldCnt);
    correctRate = zeros(length(deltaT),kFoldCnt);

    aucCompCon = zeros(length(deltaT),kFoldCnt);
    errorRateCon = zeros(length(deltaT),kFoldCnt);
    correctRateCon = zeros(length(deltaT),kFoldCnt);
    numWorkers = 24;
    p = gcp('nocreate'); % If no pool, do not create new one. ~14Gb/exp
    if isempty(p)
        parpool(numWorkers);
    end
    indx = find((((drug_conc==maxConc(vv))&(contains(drugNm,drugs{vv}))) | ((kan_r==0)&(drug_conc==0))));


    parfor dT = 1:length(deltaT)
        warning('off')
        rng(5); %Set random number generator for reproducibility
        disp(['Drug: ' drugs{vv} ', Model: ' num2str(deltaT(dT))])
        S = struct();
        S.meanIntG = intMatR(indx,1:deltaT(dT));
        S.maxIntG = maxIntMatR(indx,1:deltaT(dT));
        S.minIntG = minIntMatR(indx,1:deltaT(dT));
        S.stdIntG = minIntMatR(indx,1:deltaT(dT));
        
        %Calculate Feature Matrix:
        [master_data,mastLab] = calcFeatureMatrix(S,deltaT(dT));
        %Use label cells Label cells as either Resistant or Sensitive.  Include untreated
        %sensitive as resistant cells!
        tmp_kan_r = kan_r(indx);tmp_drug_conc=drug_conc(indx);tmp_drug_nm = drugNm(indx);
        master_label = ones(length(tmp_kan_r),1);
        %Sensitive cells are zero
        master_label((tmp_drug_conc==maxConc(vv))&(contains(tmp_drug_nm,drugs{vv}))) = 0;
        %Normalize data
        master_data = normalize(master_data);

        master_data = master_data(:,sum(isnan(master_data))~=length(master_data));
        master_label = master_label(sum(isnan(master_data'))==0);
        master_data = master_data(sum(isnan(master_data'))==0,:);
        %Uncomment to examine how class labels separate
        [~,score,~,~,explained,~] = pca(master_data);

        %PCA downsampling...
        %Find the index that retains 95% of the variance
        expl = 0;pca_dim = 0;
        for i=1:length(explained)
            expl = expl+explained(i);
            if expl>95
                pca_dim = i;
                break
            end
        end
        master_data = score(:,1:pca_dim);

        for j = 1:kFoldCnt
            %Keep an equal amount of sensitive and resistant cells for training
            mn = min([sum(master_label==1),sum(master_label==0)]);
            idx_sens = find(master_label==0);idx_res = find(master_label==1);
            idx_sens = idx_sens(randperm(length(idx_sens)));
            idx_res = idx_res(randperm(length(idx_res)));
            idx_sens = idx_sens(1:mn);idx_res=idx_res(1:mn);
            idx = union(idx_res,idx_sens);
            tmp_master_data = master_data(idx,:);
            tmp_master_label = master_label(idx);

            %Use 80% of the data for training the model
            spt = rand(length(tmp_master_label),1)> 0.8;
            test = tmp_master_data(spt,:);
            train = tmp_master_data(spt==0,:);
            test_lab = tmp_master_label(spt,:);
            train_lab = tmp_master_label(spt==0,:);
            %Set up template Tree
            %See https://www.mathworks.com/help/stats/templatetree.html for list of
            %hyperparameters
            %%%SET PARAMETERS HERE!!!!!!
            t = templateTree('Reproducible',true,'MinLeafSize',mls(vv),'MaxNumSplits',mns(vv));
            Mdl1 = fitcensemble(train,train_lab,'Method','AdaBoostM1','NumLearningCycles',nlc(vv),'LearnRate',lr(vv),'NPrint',0);

            [predict_labels,predict_score] = predict(Mdl1, test);
            [~,~,~,auc] = perfcurve(test_lab,predict_score(:,1),0);
            %c = confusionmat(test_lab,predict_labels);
            cp1 = classperf(test_lab,predict_labels);
            aucComp(dT,j) = auc;
            errorRate(dT,j) = cp1.ErrorRate;
            correctRate(dT,j) = cp1.CorrectRate;


            %%Randomize and measure control
            tmp_train = train(randperm(length(train)),:);
            t = templateTree('Reproducible',true,'MinLeafSize',mls(vv),'MaxNumSplits',mns(vv));
            Mdl2 = fitcensemble(tmp_train,train_lab,'Method','AdaBoostM1','NumLearningCycles',nlc(vv),'LearnRate',lr(vv),'NPrint',0);

            [predict_labels,predict_score] = predict(Mdl2, test);
            [~,~,~,auc] = perfcurve(test_lab,predict_score(:,1),0);
            %c = confusionmat(test_lab,predict_labels);
            cp2 = classperf(test_lab,predict_labels);
            aucCompCon(dT,j) = auc;
            errorRateCon(dT,j) = cp2.ErrorRate;
            correctRateCon(dT,j) = cp2.CorrectRate;
        end
    end

    save(['models' filesep 'deltaT-' drugs{vv} '.mat'],'deltaT','aucComp','aucCompCon','errorRate','errorRateCon','correctRate','correctRateCon')

    fig = figure('Color','w','Position',[900 1200 200 120]);
    ax1 = subplot(1,1,1);
    shadedErrorBar(deltaT,errorRate'*100,{@mean,@std},'lineProps',{'r','markerfacecolor','r','linewidth',2})
    shadedErrorBar(deltaT,errorRateCon'*100,{@mean,@std},'lineProps',{'b','markerfacecolor','b','linewidth',2})
    xlabel('\Deltat')
    ylabel('Error Rate')
    ylim([0,60])
    try
        p = xline(min(deltaT(mean(errorRate')<.05)),'c','linewidth',2);
        set(get(get(p,'Annotation'),'LegendInformation'),'IconDisplayStyle','off')
    end
    legend({'Training Data','Scramble Control'},'Location','East')
    tightfig
    set(gca,'Linewidth',2,'FontSize',8,'FontName','Arial')
    saveas(fig,['plots' filesep 'deltaT_ml_' drugs{vv} '_error.pdf'])

    fig = figure('Color','w','Position',[900 1200 200 120]);
    ax1 = subplot(1,1,1);
    shadedErrorBar(deltaT,aucComp',{@mean,@std},'lineProps',{'r','markerfacecolor','r','linewidth',2})
    shadedErrorBar(deltaT,aucCompCon',{@mean,@std},'lineProps',{'b','markerfacecolor','b','linewidth',2})
    xlabel('\Deltat')
    ylabel('ROC AUC')
    ylim([.4,1])
    try
        p = xline(min(deltaT(mean(errorRate')<.05)),'c','linewidth',2);
        set(get(get(p,'Annotation'),'LegendInformation'),'IconDisplayStyle','off')
    end
    legend({'Training Data','Scramble Control'},'Location','East')
    tightfig
    set(gca,'Linewidth',2,'FontSize',8,'FontName','Arial')
    saveas(fig,['plots' filesep 'deltaT_ml_' drugs{vv} '_auc.pdf'])

end




drugs = {'cipro','chlor','trim','PmB','gent','kan'};
model_performance = zeros(length(drugs),1);
for vv=1:length(drugs)
    load(['deltaT-' drugs{vv} '.mat'])
    try
        model_performance(vv) = min(deltaT(mean(errorRate')<.05));
    catch
        model_performance(vv) = NaN;
    end
end
model_performance(isnan(model_performance))=241;
    
fig = figure('Color','w','Position',[900 1200 200 120]);
ax1 = subplot(1,1,1);
[c,i] = sort(model_performance);
bar(c)
xticks(1:length(drugs))
xticklabels(drugs(i))
xtickangle(45)
ylabel('\Delta t')
title({'Minimum trace w/', 'classifier error <5%'})
set(gca,'Linewidth',2,'FontSize',8,'FontName','Arial')
tightfig
saveas(fig,['plots' filesep 'deltaT_drug_comparison.pdf'])

    
