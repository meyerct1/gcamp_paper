cd ~/Repos/GCAMP_paper/Data_Code/Figure1/

%%Step 1: Read in data of interest
%Clear everything and start a timer
clear; close all;tic
%Set active directory
activeDir = '/mnt/kralj_celery/E coli GC antibioitic/*_cm/2021-01-15_Results/matFiles/';
%activeDir    = '/mnt/kralj_celery/E coli GC antibioitic/2021-01-11-kan_dilution_gcamp_JMK/2021-01-15_Results/matFiles/';
flist        = dir([activeDir '*' filesep '*data.mat']); %Current file list
nfiles       = length(flist); %Number of files

%Read in the feature matrices
intMatG = [];intMatR = []; drug_conc=[];kan_r=[];matArea = [];segArea=[];segPerimeter=[];segMajAxisLen=[];segMinAxisLen=[];segCircularity=[];

for i = 1:length(flist)
    load([flist(i).folder filesep flist(i).name])
    intMatG        = [intMatG;S.cellMeanIntG];
    intMatR        = [intMatR;S.cellMeanIntR];
    matArea        = [matArea;S.cellArea];
    drug_conc      = [drug_conc;ones(S.ncells,1)*S.currConc];
    kan_r          = [kan_r;ones(S.ncells,1)*contains(S.currCell,'KanR')];
    segArea        = [segArea;S.segArea];
    segMajAxisLen  = [segMajAxisLen; S.segMajAxisLen];
    segMinAxisLen  = [segMinAxisLen; S.segMinAxisLen];
    segCircularity = [segCircularity;S.segCircularity];
    segPerimeter   = [segPerimeter; S.segPerimeter];
end
%Select the correct cells based on features
%Remove cells which vanished from segmentation (matArea==0)
indx = find(sum(matArea'==0)>0);
segCircularity(abs(segCircularity)==Inf)=0;
intMatG = fillmissing(intMatG','movmedian',15)';
intMatR = fillmissing(intMatR','movmedian',15)';
indx = union(indx,find(sum(isnan(intMatG'))>0));
indx = union(indx,find(sum(isnan(intMatR'))>0));

dat = [segArea,segMajAxisLen,segMinAxisLen,segCircularity,segPerimeter];
dat = normalize(dat);
[coeff,score,latent,tsquared,explained,mu] = pca(dat);
dist_mat = mean(squareform(pdist(score)));
data_keep = .95;
tmp = sort(dist_mat);
indx  = union(indx,find(dist_mat>tmp(floor(length(dist_mat)*data_keep))));
figure()
hold on
scatter(score(:,1),score(:,2))
scatter(score(indx,1),score(indx,2))
legend('keep','remove')

to_keep = setdiff(1:length(dist_mat),indx);
intMatG        = intMatG(to_keep,:);
intMatR        = intMatR(to_keep,:);
matArea        = matArea(to_keep,:);
drug_conc      = drug_conc(to_keep);
kan_r          = kan_r(to_keep);

sdMatG = movstd((intMatG./movmedian(intMatG',45)')',30)';

%%
%Area vs STD over time

fig = figure('Color','w','Position',[900 1200 600 100]);
ax1 = subplot(1,2,1);
shadedErrorBar(S.time/60.,sdMatG((drug_conc==100)&(kan_r==0),:),{@mean,@std},'lineProps',{'b','markerfacecolor','b','linewidth',2})
shadedErrorBar(S.time/60.,sdMatG((drug_conc==100)&(kan_r==1),:),{@mean,@std},'lineProps',{'k','markerfacecolor','k','linewidth',2})
ylabel('GCAMP SD')
xlim([0,max(S.time/60.)])
set(gca,'Linewidth',2,'FontSize',8,'FontName','Arial')
xticks([0,120,240])
xlabel('Time (min)')
ax2 = subplot(1,2,2);
ars = matArea((drug_conc==100)&(kan_r==0),:)
ars = ars./ars(:,1)
arr = matArea((drug_conc==100)&(kan_r==1),:)
arr = arr./arr(:,1)
shadedErrorBar(S.time/60.,ars,{@mean,@std},'lineProps',{'b','markerfacecolor','b','linewidth',2})
shadedErrorBar(S.time/60.,arr,{@mean,@std},'lineProps',{'k','markerfacecolor','k','linewidth',2})
legend('Sensitive','Resistant',"Location","Northwest","FontSize",8,"FontName","Arial")
legend boxoff
ylabel('Cell Area')
xlim([0,max(S.time/60.)])
set(gca,'Linewidth',2,'FontSize',8,'FontName','Arial')
xticks([0,120,240])
xlabel('Time (min)')
tightfig
saveas(fig,'Figure3_area_vs_gcampSD.pdf')



conc = [];kanR=[];pctBrightR=[];
for i = 1:length(flist)
    load([flist(i).folder filesep flist(i).name])
    conc      = [conc;S.currConc];
    pctBrightR     = [pctBrightR;S.pctBrightR];
    kanR          = [kanR;contains(S.currCell,'KanR')];
end

pctBrightR = squeeze(pctBrightR);
pctBrightR = pctBrightR./pctBrightR(:,1);
figure()
indx = (conc==100)&(kanR==1)
hold on
p1 = plot(pctBrightR(indx,:)','color','b')
indx = (conc==100)&(kanR==0)
p2 = plot(pctBrightR(indx,:)','color','r')
legend('Resistant','Resistant','Resistant','Sensitive','Sensitive','Sensitive')


