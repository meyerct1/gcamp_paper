cd /home/meyerct6/Repos/GCAMP_paper/Data_Code/AnalysisCode/F4
%Load in data
load('../../Datasets/02262021_AllSingleCellInfoData.mat')

%Calculate Ca transients
sdMatG = movstd((intMatG./movmedian(intMatG',45)')',30)';
S.time = 1:241
%% Generate Figure
%Area vs STD over time
fig = figure('Color','w','Position',[900 1200 600 100]);
ax1 = subplot(1,2,1);
sens_indx = find((drug_conc==100)&(kan_r==0)&(contains(drugNm,'kan')));
res_indx =  find(((kan_r==1)&(contains(drugNm,'kan')))|((drug_conc==0)&(kan_r==0)&(contains(drugNm,'kan'))));
shadedErrorBar(S.time(5:end),sdMatG(sens_indx,5:end),{@mean,@std},'lineProps',{'color',[0 150/225 1], 'markerfacecolor',[0 150/225 1],'linewidth',2})
shadedErrorBar(S.time(5:end),sdMatG(res_indx,5:end),{@mean,@std},'lineProps',{'k','markerfacecolor','k','linewidth',2})
ylabel('GCaMP6 std')
xlim([0,max(S.time)])
set(gca,'Linewidth',2,'FontSize',8,'FontName','Arial')
xticks([0,120,240])
xlabel('Time (min)')
ax2 = subplot(1,2,2);
ars = cellArea(sens_indx,:);
ars = ars-mean(ars(:,10:20),2);
arr = cellArea(res_indx,:);
arr = arr-mean(arr(:,10:20),2);
shadedErrorBar(S.time(5:end),ars(:,5:end),{@mean,@std},'lineProps',{'color',[0 150/225 1],'markerfacecolor',[0 150/225 1],'linewidth',2})
shadedErrorBar(S.time(5:end),arr(:,5:end),{@mean,@std},'lineProps',{'k','markerfacecolor','k','linewidth',2})
legend('Sensitive','Resistant',"Location","Northwest","FontSize",8,"FontName","Arial")
legend boxoff
ylabel('Cell Area')
xlim([0,max(S.time)])
set(gca,'Linewidth',2,'FontSize',8,'FontName','Arial')
xticks([0,120,240])
xlabel('Time (min)')
tightfig
saveas(fig,'Figure4_area_vs_gcampSD.pdf')


p1 = zeros(241,1);
p2 = zeros(241,1);
kstat1 = zeros(241,1);
kstat2 = zeros(241,1);

for i = 1:241
    [h,p1(i),kstat1(i)] = kstest2(ars(:,i),arr(:,i));
    [h,p2(i),kstat2(i)] = kstest2(sdMatG(sens_indx,i),sdMatG(res_indx,i));
end
    
    
find(kstat2>0.25,1)
find(kstat1>0.25,1)


