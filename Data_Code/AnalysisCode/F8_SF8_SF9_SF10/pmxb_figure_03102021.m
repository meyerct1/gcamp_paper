cd /home/meyerct6/Repos/GCAMP_paper/Data_Code/AnalysisCode/F8_SF8_SF9_SF10
addpath(genpath('/home/meyerct6/Repos/GCAMP_paper/Data_Code/Functions'))

%Load in data
load('../../Datasets/03072021_PmB-PI-InfoData.mat')
dintMatR = intMatR - mean(intMatR(:,10:20)')';
dintMatG = intMatG - mean(intMatG(:,10:20)')';
ca_comp   = zeros(size(dintMatG,1),1);
egta_comp = zeros(size(dintMatG,1),1);
cccp_comp = zeros(size(dintMatG,1),1);
drug_conc_comp = drug_conc;

load('../../Datasets/03032021_PmB-PI_EGTA-InfoData.mat')
dintMatR = [dintMatR; intMatR - mean(intMatR(:,10:20)')'];
dintMatG = [dintMatG; intMatG - mean(intMatG(:,10:20)')'];
ca_comp = [ca_comp;ca];
egta_comp = [egta_comp;egta];
cccp_comp = [cccp_comp;cccp];
drug_conc_comp = [drug_conc_comp;drug_conc];

load('../../Datasets/03122021_PmB-PI_EGTA-InfoData.mat')
dintMatR = [dintMatR; intMatR - mean(intMatR(:,10:20)')'];
dintMatG = [dintMatG; intMatG - mean(intMatG(:,10:20)')'];
ca_comp = [ca_comp;ca];
egta_comp = [egta_comp;egta];
cccp_comp = [cccp_comp;cccp];
drug_conc_comp = [drug_conc_comp;drug_conc];

ca = ca_comp;
egta = egta_comp;
cccp = cccp_comp;
drug_conc = drug_conc_comp;

%Index of the three experiments
indx = []
indx(:,1) = (egta==0) & (ca==0);
indx(:,2) = ((egta==1));
indx(:,3) = ((ca==1));
cnt = 1;
conc = unique(drug_conc);
conc = conc(end:-1:1);

%% Figure A.  GCaMP6 and PI in aligned time. + CCCP
fig = figure('Color','w','Position',[900 1200 750 200]);cnt = 0;
for vv=1:3
    cnt = cnt + 1;
    subplot(1,3,cnt)
    %Select conditions
    cc = 0; c= 1;
    %Get the cell indices
    idx = ((indx(:,vv)) & (cccp == cc) & (drug_conc==conc(c)));
    %Get title

    %Align the traces using derivative of GCaMP6 signal
    tmp = diff(dintMatG(idx,:)')';
    G = dintMatG(idx,:); R = dintMatR(idx,:);
    %Establish the threshold based on when the signal has died out
    thresh = mean(mean(tmp(:,150:end)));
    thresh_std = std(std(tmp(:,150:end)'));
    %Find the align time
    srt = [];
    for i=1:length(idx)
        try
            srt(i) = find(movmean(abs(tmp(i,:)),3)>(thresh+5*thresh_std),1,'last');
        end
    end
    %Subset for only the traces that are reasonable to align
    G = G(srt>40 & srt < 100,:); R = R(srt>40 & srt<100,:);  srt = srt(srt>40 & srt<100);
    %Align traces
    mm = min(srt); mx = max(srt);ww = size(G,2);
    wind = 2*min([ww-mx-1,mm-1]);
    intMatG_align = zeros(size(G,1),wind+1);
    intMatR_align = zeros(size(R,1),wind+1);
    for i = 1:size(G,1)
        intMatG_align(i,:) = G(i,srt(i)-wind/2:srt(i)+wind/2);
        intMatR_align(i,:) = R(i,srt(i)-wind/2:srt(i)+wind/2);
    end
    %Plot
    shadedErrorBar(-wind/2:wind/2,intMatG_align,{@mean,@std},'lineprops',{'g','markerfacecolor','g','linewidth',2});hold on;
    shadedErrorBar(-wind/2:wind/2,intMatR_align,{@mean,@std},'lineprops',{'r','markerfacecolor','r','linewidth',2})
    xlabel('Aligned Time')
    ylabel('Fluorescence (AU)')
    if vv ==2
        ttl = ['+EGTA [5\muM]'];
    elseif vv == 3
        ttl = ['+Ca2+ [1mM]'];
    else
        ttl = ['PmB [' num2str(conc(c)) '\mug/mL]'];
    end
    title(ttl,'Fontsize',8)
    set(gca,'Fontsize',8,'FontName','Arial','Linewidth',2)
end
saveas(fig,'plots/gcamp_pi_timealigned_pmb10.pdf')



%% Figure A.  Algorithm for aligning time using gcamp signal
vv = 1; cc = 0; c= 1;
%Get the cell indices
idx = ((indx(:,vv)) & (cccp == cc) & (drug_conc==conc(c)));
%Align the traces using derivative of GCaMP6 signal
tmp = diff(dintMatG(idx,:)')';
G = dintMatG(idx,:); R = dintMatR(idx,:);
%Establish the threshold based on when the signal has died out
thresh = mean(mean(tmp(:,150:end)));
thresh_std = std(std(tmp(:,150:end)'));
cm_colors = parula(3);
rng(5)
fig = figure('Color','w','Position',[900 1200 700 200]);cnt = 1;
for i = randperm(size(G,1),3)
    subplot(1,2,1); hold on
    x_loc = find(movmean(abs(tmp(i,:)),3)>thresh+5*thresh_std,1,'last')
    plot(G(i,:)+80*(cnt-1),'linewidth',2,'color',cm_colors(cnt,:))
    xline(x_loc,'--','linewidth',1,'color',cm_colors(cnt,:))
    xlabel('Time(min)')
    ylabel('GCaMP6 (AU)')
    yticks([])
    xticks([0 120 240])
    set(gca,'Fontsize',8,'FontName','Arial','Linewidth',2)
    subplot(1,2,2); hold on
    plot(movmean(abs(tmp(i,:)),3)+30*(cnt-1),'linewidth',2,'color',cm_colors(cnt,:))
    xlabel('Time(min)')
    ylabel('d/dt(GCaMP6)')
    yticks([])
    xticks([0 120 240])
    set(gca,'Fontsize',8,'FontName','Arial','Linewidth',2) 
    yline(thresh+5*thresh_std+30*(cnt-1),'--','linewidth',1,'color',cm_colors(cnt,:))
    cnt = cnt + 1;
end
tightfig

saveas(fig,'plots/show_gcamp_time_alignment.pdf')




%% Figure A.  GCaMP6 and PI in aligned time.
fig = figure('Color','w','Position',[900 1200 750 200]);cnt = 0;
for vv=1:3
    cnt = cnt + 1;
    subplot(1,3,cnt)
    %Select conditions
    cc = 1; c= 1;
    %Get the cell indices
    idx = ((indx(:,vv)) & (cccp == cc) & (drug_conc==conc(c)));
    %Get title

    %Align the traces using derivative of GCaMP6 signal
    tmp = diff(dintMatG(idx,:)')';
    G = dintMatG(idx,:); R = dintMatR(idx,:);
    %Establish the threshold based on when the signal has died out
    thresh = mean(mean(tmp(:,150:end)));
    thresh_std = std(std(tmp(:,150:end)'));
    %Find the align time
    srt = [];
    for i=1:length(idx)
        try
            srt(i) = find(movmean(abs(tmp(i,:)),3)>(thresh+5*thresh_std),1,'last');
        end
    end
    %Subset for only the traces that are reasonable to align
    G = G(srt>40 & srt < 100,:); R = R(srt>40 & srt<100,:);  srt = srt(srt>40 & srt<100);
    %Align traces
    mm = min(srt); mx = max(srt);ww = size(G,2);
    wind = 2*min([ww-mx-1,mm-1]);
    intMatG_align = zeros(size(G,1),wind+1);
    intMatR_align = zeros(size(R,1),wind+1);
    for i = 1:size(G,1)
        intMatG_align(i,:) = G(i,srt(i)-wind/2:srt(i)+wind/2);
        intMatR_align(i,:) = R(i,srt(i)-wind/2:srt(i)+wind/2);
    end
    %Plot
    shadedErrorBar(-wind/2:wind/2,intMatG_align,{@mean,@std},'lineprops',{'g','markerfacecolor','g','linewidth',2});hold on;
    shadedErrorBar(-wind/2:wind/2,intMatR_align,{@mean,@std},'lineprops',{'r','markerfacecolor','r','linewidth',2})
    xlabel('Aligned Time')
    ylabel('Fluorescence (AU)')
    if vv ==2
        ttl = ['+EGTA [5\muM]+CCCP [50\muM]'];
    elseif vv == 3
        ttl = ['+Ca2+ [1mM]+CCCP [50\muM]'];
    else
        ttl = ['PmB [' num2str(conc(c)) '\mug/mL]'];
    end
    title(ttl,'Fontsize',8)
    set(gca,'Fontsize',8,'FontName','Arial','Linewidth',2)
end
saveas(fig,'plots/gcamp_pi_timealigned_pmb10+CCCP.pdf')




%Index of the three experiments
indx = []
indx(:,1) = (egta==0) & (ca==0);
indx(:,2) = ((egta==1));
indx(:,3) = ((ca==1));
cnt = 1;
conc = unique(drug_conc);
conc = conc(end:-1:1);


dataG_arr = {};dataR_arr = {};
label = {};cnt = 0;
cccp_arr = [];
egta_arr = [];
ca_arr = [];
conc_arr = [];
threshR = 12;
%% Figure A.  GCaMP6 and PI in aligned time. + CCCP
for cc = 0:1
    for vv=1:3
        for c = 1:length(conc)-1
            %Get the cell indices
            idx = ((indx(:,vv)) & (cccp == cc) & (drug_conc==conc(c)));
            %Get title
            if sum(idx)>0
                cnt = cnt + 1;

                %Align the traces using derivative of GCaMP6 signal
                tmp = diff(dintMatG(idx,:)')';
                G = dintMatG(idx,:); R = dintMatR(idx,:);
                %Establish the threshold based on when the signal has died out
                thresh = mean(mean(tmp(:,150:end)));
                thresh_std = std(std(tmp(:,150:end)'));
                %Find the align time
                srt = [];
                for i=1:length(idx)
                    try
                        srt(i) = find(movmean(abs(tmp(i,:)),3)>(thresh+5*thresh_std),1,'last');
                    end
                end
                %Subset for only the traces that are reasonable to align
                min_win = 25;mx_win = 225;
                G = G(srt>min_win & srt < mx_win,:); R = R(srt>min_win & srt<mx_win,:);  srt = srt(srt>min_win & srt<mx_win);
                %Align traces
                mm = min(srt); mx = max(srt);ww = size(G,2);
                wind = 2*min([ww-mx-1,mm-1]);
                intMatG_align = zeros(size(G,1),wind+1);
                intMatR_align = zeros(size(R,1),wind+1);
                for i = 1:size(G,1)
                    intMatG_align(i,:) = G(i,srt(i)-wind/2:srt(i)+wind/2);
                    intMatR_align(i,:) = R(i,srt(i)-wind/2:srt(i)+wind/2);
                end
                %Find PI influx
                Rstr = [];
                Rsrt = zeros(size(intMatR_align,1),1)*nan;
                for i=1:size(intMatR_align,1)
                    if sum(intMatR_align(i,:)>threshR)>1
                        Rsrt(i) = find(intMatR_align(i,:)>threshR,1)-wind/2;
                    end
                end
                dataG_arr{cnt} = srt;dataR_arr{cnt}=Rsrt;
                cccp_arr(cnt)=cc; egta_arr(cnt)=unique(egta(idx));
                ca_arr(cnt)=unique(ca(idx)); conc_arr(cnt)=conc(c);
            end
        end
    end
end

T = array2table([conc_arr;cccp_arr;egta_arr;ca_arr]','VariableNames',{'Conc','CCCP','EGTA','Ca2+'})

figure_names = {'gcamp_aligned_pi_dose_flux.pdf','10pmb_gcamp_aligned_egta_ca_pi_dose_flux.pdf','1pmb_gcamp_aligned_egta_ca_pi_dose_flux.pdf','10pmb_cccp_impact.pdf','1pmb_cccp_impact.pdf','10pmb_cccp_impact-single.pdf'};
ids = {[1,2,3],[1,4,6],[3,5,7],[1,8,4,10,6,12],[3,9,5,11,7,13],[1,8]};
for i = 1:length(ids)
    fig = figure('Color','w','Position',[900 1200 190 200]); 
    [h, L, MX, MED, bw] = violin(dataR_arr(ids{i}),'medc',[],'facecolor',[1 .95 0],'facealpha',1,'edgecolor','k');
    lab = {};
    for j = 1:length(ids{i})
        lab{j} =['PmB: ' num2str(conc_arr(ids{i}(j))) ', CCCP: ' num2str(cccp_arr(ids{i}(j))) ', EGTA: ' num2str(egta_arr(ids{i}(j))) ', Ca: ' num2str(ca_arr(ids{i}(j)))];
    end
    %set(gca,'xtick',1:length(ids{i}))
    %set(gca,'xticklabels',lab)
    xtickangle(45)
    set(gca,'Fontsize',8,'FontName','Arial','Linewidth',2)
    ylabel({'Time to PI entry','after Ca2+ blink'})
    L.FontSize= 8;
    L.Location = 'northwest';
    tightfig;
    saveas(fig,['plots' filesep figure_names{i}])
end
 disp('p-val PmB(10)<PmB(3), gcamp alignment')   
[p,h,stats] = ranksum(dataR_arr{1},dataR_arr{2},'tail','left');
disp(p)
 disp('p-val PmB(3)<PmB(1), gcamp alignment')   
[p,h,stats] = ranksum(dataR_arr{2},dataR_arr{3},'tail','left');
disp(p)
 disp('p-val PmB(10)<PmB(10)+EGTA, gcamp alignment')   
[p,h,stats] = ranksum(dataR_arr{1},dataR_arr{4},'tail','left');
disp(p)
 disp('p-val PmB(10)<PmB(10)+ca, gcamp alignment')   
[p,h,stats] = ranksum(dataR_arr{1},dataR_arr{6},'tail','left');
disp(p)
disp('p-val PmB(10)+ca!=PmB(10)+EGTA, gcamp alignment')   
[p,h,stats] = ranksum(dataR_arr{6},dataR_arr{4});
disp(p)


 disp('p-val PmB(1)<PmB(1)+EGTA, gcamp alignment')   
[p,h,stats] = ranksum(dataR_arr{3},dataR_arr{5},'tail','left');
disp(p)
 disp('p-val PmB(1)<PmB(1)+ca, gcamp alignment')   
[p,h,stats] = ranksum(dataR_arr{3},dataR_arr{7},'tail','left');
disp(p)
disp('p-val PmB(1)+ca<PmB(1)+EGTA, gcamp alignment')   
[p,h,stats] = ranksum(dataR_arr{7},dataR_arr{5},'tail','left');
disp(p)




thresh = mean(std(dintMatR(:,10:40))')*5;


data_arr = {};
cccp_arr = [];
egta_arr = [];
ca_arr = [];
conc_arr = [];
label = {};
thresh_arr = {};
cnt =  1;
for vv = 1:3
    for c = 1:length(conc)-1
        for cc = 0:1
            idx = indx(:,vv) & (cccp==cc) & (drug_conc==conc(c));
            if sum(idx)>0
                tmp = (dintMatR(idx,:)-mean(dintMatR(idx,10:20)')');
                tmp = movmean(tmp,10);
                srt = zeros(size(tmp,1),1)*nan;
                for i=1:size(tmp,1)
                    if sum(tmp(i,:)>thresh)>1
                        srt(i) = find(tmp(i,:)>thresh,1);
                    end
                end
                srt = srt(~isnan(srt));
                data_arr{cnt} = srt;
                cccp_arr(cnt)=cc; egta_arr(cnt)=unique(egta(idx));
                ca_arr(cnt)=unique(ca(idx)); conc_arr(cnt)=conc(c);
                cnt = cnt + 1;
            end
        end
    end
end


T = array2table([1:length(conc_arr);conc_arr;cccp_arr;egta_arr;ca_arr]','VariableNames',{'idx','Conc','CCCP','EGTA','Ca2+'});

figure_names = {'dose_dependent_pi_entry.pdf','10pmb_egta_ca_pi_entry.pdf','1pmb_egta_ca_pi_entry.pdf','10pmb_cccp_pi.pdf'};
ids = {[1,3,4],[1,6,10],[4,8,12],[1,2]};
for i = 1:length(ids)
    fig = figure('Color','w','Position',[900 1200 190 200]); 
    [h, L, MX, MED, bw] = violin(data_arr(ids{i}),'medc',[],'facecolor','r','facealpha',.6,'edgecolor','k');
    lab = {};
    for j = 1:length(ids{i})
        lab{j} =['PmB: ' num2str(conc_arr(ids{i}(j))) ', CCCP: ' num2str(cccp_arr(ids{i}(j))) ', EGTA: ' num2str(egta_arr(ids{i}(j))) ', Ca: ' num2str(ca_arr(ids{i}(j)))];
    end
    %set(gca,'xtick',1:length(ids{i}))
    %set(gca,'xticklabels',lab)
    xtickangle(45)
    set(gca,'Fontsize',8,'FontName','Arial','Linewidth',2)
    ylabel({'Single-cell','Time to PI entry'})
    L.FontSize= 8;
    L.Location = 'northwest';
    tightfig;
    saveas(fig,['plots' filesep figure_names{i}])
end
    




 disp('p-val PmB(10)<PmB(3), pi alignment')   
[p,h,stats] = ranksum(data_arr{1},data_arr{3},'tail','left');
disp(p)
 disp('p-val PmB(3)<PmB(1), pi alignment')   
[p,h,stats] = ranksum(data_arr{3},data_arr{4},'tail','left');
disp(p)
 disp('p-val PmB(10)<PmB(10)+EGTA, pi alignment')   
[p,h,stats] = ranksum(data_arr{1},data_arr{6},'tail','left');
disp(p)
 disp('p-val PmB(10)<PmB(10)+ca, pi alignment')   
[p,h,stats] = ranksum(data_arr{1},data_arr{10},'tail','left');
disp(p)
disp('p-val PmB(10)+ca!=PmB(10)+EGTA, pi alignment')   
[p,h,stats] = ranksum(data_arr{6},data_arr{10});
disp(p)


 disp('p-val PmB(10)<PmB(10)+CCCP, pi alignment')   
[p,h,stats] = ranksum(data_arr{1},data_arr{2},'tail','left');
disp(p)
