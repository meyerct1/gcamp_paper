cd /home/meyerct6/Repos/GCAMP_paper/Data_Code/AnalysisCode/F8_SF8_SF9_SF10
addpath(genpath('/home/meyerct6/Repos/GCAMP_paper/Data_Code/Functions'))

%Load in data
load('../../Datasets/03072021_PmB-PI-InfoData.mat')
dintMatR = intMatR - mean(intMatR(:,10:20)')';
dintMatG = intMatG - mean(intMatG(:,10:20)')';
ca_comp   = zeros(size(dintMatG,1),1);
egta_comp = zeros(size(dintMatG,1),1);
cccp_comp = zeros(size(dintMatG,1),1);
drug_conc_comp = drug_conc;

load('../../Datasets/03032021_PmB-PI_EGTA-InfoData.mat')
dintMatR = [dintMatR; intMatR - mean(intMatR(:,10:20)')'];
dintMatG = [dintMatG; intMatG - mean(intMatG(:,10:20)')'];
ca_comp = [ca_comp;ca];
egta_comp = [egta_comp;egta];
cccp_comp = [cccp_comp;cccp];
drug_conc_comp = [drug_conc_comp;drug_conc];

load('../../Datasets/03122021_PmB-PI_EGTA-InfoData.mat')
dintMatR = [dintMatR; intMatR - mean(intMatR(:,10:20)')'];
dintMatG = [dintMatG; intMatG - mean(intMatG(:,10:20)')'];
ca_comp = [ca_comp;ca];
egta_comp = [egta_comp;egta];
cccp_comp = [cccp_comp;cccp];
drug_conc_comp = [drug_conc_comp;drug_conc];

ca = ca_comp;
egta = egta_comp;
cccp = cccp_comp;
drug_conc = drug_conc_comp;

%Index of the three experiments
indx = []
indx(:,1) = (egta==0) & (ca==0);
indx(:,2) = ((egta==1));
indx(:,3) = ((ca==1));
cnt = 1;
conc = unique(drug_conc);
conc = conc(end:-1:1);


dataG_arr = {};dataR_arr = {};
label = {};cnt = 0;
cccp_arr = [];
egta_arr = [];
ca_arr = [];
conc_arr = [];
threshR = 12;
%% Figure A.  GCaMP6 and PI in aligned time. + CCCP
for cc = 0:1
    for vv=1:3
        for c = 1:length(conc)-1
            %Get the cell indices
            idx = ((indx(:,vv)) & (cccp == cc) & (drug_conc==conc(c)));
            %Get title
            if sum(idx)>0
                cnt = cnt + 1;

                %Align the traces using derivative of GCaMP6 signal
                tmp = diff(dintMatG(idx,:)')';
                G = dintMatG(idx,:); R = dintMatR(idx,:);
                %Establish the threshold based on when the signal has died out
                thresh = mean(mean(tmp(:,150:end)));
                thresh_std = std(std(tmp(:,150:end)'));
                %Find the align time
                srt = [];
                for i=1:length(idx)
                    try
                        srt(i) = find(movmean(abs(tmp(i,:)),3)>(thresh+5*thresh_std),1,'last');
                    end
                end
                %Subset for only the traces that are reasonable to align
                min_win = 25;mx_win = 225;
                G = G(srt>min_win & srt < mx_win,:); R = R(srt>min_win & srt<mx_win,:);  srt = srt(srt>min_win & srt<mx_win);
                %Align traces
                mm = min(srt); mx = max(srt);ww = size(G,2);
                wind = 2*min([ww-mx-1,mm-1]);
                intMatG_align = zeros(size(G,1),wind+1);
                intMatR_align = zeros(size(R,1),wind+1);
                for i = 1:size(G,1)
                    intMatG_align(i,:) = G(i,srt(i)-wind/2:srt(i)+wind/2);
                    intMatR_align(i,:) = R(i,srt(i)-wind/2:srt(i)+wind/2);
                end
                %Find PI influx
                Rstr = [];
                Rsrt = zeros(size(intMatR_align,1),1)*nan;
                for i=1:size(intMatR_align,1)
                    if sum(intMatR_align(i,:)>threshR)>1
                        Rsrt(i) = find(intMatR_align(i,:)>threshR,1);
                    end
                end
                dataG_arr{cnt} = srt;dataR_arr{cnt}=Rsrt;
                cccp_arr(cnt)=cc; egta_arr(cnt)=unique(egta(idx));
                ca_arr(cnt)=unique(ca(idx)); conc_arr(cnt)=conc(c);
            end
        end
    end
end

T = array2table([conc_arr;cccp_arr;egta_arr;ca_arr]','VariableNames',{'Conc','CCCP','EGTA','Ca2+'})


ids = {[1,2,3],[1,4,6],[3,5,7],[1,8,4,10,6,12],[3,9,5,11,7,13]};
for i = 1:length(ids)
fig = figure('Color','w','Position',[900 1200 500 600]);
    [h, L, MX, MED, bw] = violin(dataR_arr(ids{i}),'medc',[],'facecolor','r','facealpha',.6,'edgecolor','k');
    lab = {};
    for j = 1:length(ids{i})
        lab{j} =['PmB: ' num2str(conc_arr(ids{i}(j))) ', CCCP: ' num2str(cccp_arr(ids{i}(j))) ', EGTA: ' num2str(egta_arr(ids{i}(j))) ', Ca: ' num2str(ca_arr(ids{i}(j)))];
    end
    set(gca,'xtick',1:length(ids{i}))
    set(gca,'xticklabels',lab)
    xtickangle(45)
    set(gca,'Fontsize',8,'FontName','Arial','Linewidth',2)
    ylabel({'Single-cell','Time to PI entry'})
    L.FontSize= 8;
    L.Location = 'northwest';
end
    



    
