cd /home/meyerct6/Repos/GCAMP_paper/Data_Code/AnalysisCode/F8_SF8_SF9_SF10
%Load in data
load('../../Datasets/03072021_PmB-PI-InfoData.mat')
dintMatR = intMatR - mean(intMatR(:,10:20)')';
dintMatG = intMatG - mean(intMatG(:,10:20)')';
ca_comp   = zeros(size(dintMatG,1),1);
egta_comp = zeros(size(dintMatG,1),1);
cccp_comp = zeros(size(dintMatG,1),1);
drug_conc_comp = drug_conc;

load('../../Datasets/03032021_PmB-PI_EGTA-InfoData.mat')
dintMatR = [dintMatR; intMatR - mean(intMatR(:,10:20)')'];
dintMatG = [dintMatG; intMatG - mean(intMatG(:,10:20)')'];
ca_comp = [ca_comp;ca];
egta_comp = [egta_comp;egta];
cccp_comp = [cccp_comp;cccp];
drug_conc_comp = [drug_conc_comp;drug_conc];

ca = ca_comp;
egta = egta_comp;
cccp = cccp_comp;
drug_conc = drug_conc_comp;


indx = []
indx(:,1) = (egta==0) & (ca==0);
indx(:,2) = ((egta==1));
indx(:,3) = ((ca==1));
cnt = 1;
conc = unique(drug_conc);
fig = figure('Color','w','Position',[900 1200 2500 700]);
for i = 1:3
    for c = 4%:length(conc)
        for cc = 0:1
            idx = ((indx(:,i)) & (cccp == cc) & (drug_conc==conc(c)));
            subplot(1,5,cnt); hold on;
            if sum(idx)>0
                plot(movstd(dintMatG(idx,:),3)')
                ttl = ['Ca2: ' num2str(unique(ca(idx))) ', EGTA: ' num2str(unique(egta(idx))) ', PmB: ' num2str(conc(c)) ', CCCP: ' num2str(cc)];
                title(ttl)
                cnt = cnt + 1;
            end
        end
    end
end





indx = []
indx(:,1) = (egta==0) & (ca==0);
indx(:,2) = ((egta==1));
indx(:,3) = ((ca==1));
cnt = 1;
conc = unique(drug_conc);
fig = figure('Color','w','Position',[900 1200 2500 700]);
for i = 1:3
    for c = 4%:length(conc)
        for cc = 0:1
            idx = ((indx(:,i)) & (cccp == cc) & (drug_conc==conc(c)));
            subplot(1,5,cnt); hold on;
            if sum(idx)>0
                rng(5)
                idx1 = randperm(sum(idx),10);
                idx2 = find(idx);
                plot(dintMatG(idx2(idx1),:)')
                ttl = ['Ca2: ' num2str(unique(ca(idx))) ', EGTA: ' num2str(unique(egta(idx))) ', PmB: ' num2str(conc(c)) ', CCCP: ' num2str(cc)];
                title(ttl)
                cnt = cnt + 1;
            end
        end
    end
end


fig = figure('Color','w','Position',[900 1200 2500 700]);
for vv = 1:3
    subplot(1,3,vv)
    c= 4;
    cc=0;
    idx = ((indx(:,vv)) & (cccp == cc) & (drug_conc==conc(c)));
    ttl = ['Ca2: ' num2str(unique(ca(idx))) ', EGTA: ' num2str(unique(egta(idx))) ', PmB: ' num2str(conc(c)) ', CCCP: ' num2str(cc)];
    title(ttl)

    tmp = diff(dintMatG(idx,:)')';
    G = dintMatG(idx,:);
    R = dintMatR(idx,:);
    thresh = mean(mean(tmp(:,150:end)));
    thresh_std = std(std(tmp(:,150:end)'));

    srt = [];
    for i=1:length(idx)
        try
            srt(i) = find(movmean(abs(tmp(i,:)),3)>(thresh+5*thresh_std),1,'last');
        end
    end
    %figure;hist(srt,50)
%     xlabel('Time to PI exceeding threshold')
%     ylabel('Count')
%     set(gca,'Fontsize',15,'FontName','Arial','Linewidth',2)
    G = G(srt>40 & srt < 100,:); R = R(srt>40 & srt<100,:);  srt = srt(srt>40 & srt<100);

    mm = min(srt); mx = max(srt);ww = size(G,2);
    wind = 2*min([ww-mx-1,mm-1]);
    intMatG_align = zeros(size(G,1),wind+1);
    intMatR_align = zeros(size(R,1),wind+1);
    for i = 1:size(G,1)
        intMatG_align(i,:) = G(i,srt(i)-wind/2:srt(i)+wind/2);
        intMatR_align(i,:) = R(i,srt(i)-wind/2:srt(i)+wind/2);
    end

    shadedErrorBar(1:size(intMatG_align,2),intMatG_align,{@mean,@std},'lineProps',{'g','markerfacecolor','g','linewidth',2})
    shadedErrorBar(1:size(intMatR_align,2),intMatR_align,{@mean,@std},'lineProps',{'r','markerfacecolor','r','linewidth',2})
    xlabel('Aligned Time (by d/dt(GCaMP6))')
    ylabel('Fluorescence (AU)')
    if vv == 1
        legend({'GcAMP6','PI'},'location','northwest')
    end
    set(gca,'Fontsize',15,'FontName','Arial','Linewidth',2)
end




















fig = figure('Color','w','Position',[900 1200 2500 1400]);
cnt = 0;
for cc = 0:1
    for vv = 1:3
        cnt = cnt + 1;
        subplot(2,3,cnt)
        c= 2;
        %cc=0;
        if cc == 1 && vv == 1
            continue
        else
            idx = ((indx(:,vv)) & (cccp == cc) & (drug_conc==conc(c)));
            ttl = ['Ca2: ' num2str(unique(ca(idx))) ', EGTA: ' num2str(unique(egta(idx))) ', PmB: ' num2str(conc(c)) ', CCCP: ' num2str(cc)];
            title(ttl)

            tmp = diff(dintMatG(idx,:)')';
            G = dintMatG(idx,:);
            R = dintMatR(idx,:);
            thresh = mean(mean(tmp(:,150:end)));
            thresh_std = std(std(tmp(:,150:end)'));

            srt = [];
            for i=1:length(idx)
                try
                    srt(i) = find(movmean(abs(tmp(i,:)),3)>(thresh+5*thresh_std),1,'last');
                end
            end
            %figure;hist(srt,50)
        %     xlabel('Time to PI exceeding threshold')
        %     ylabel('Count')
        %     set(gca,'Fontsize',15,'FontName','Arial','Linewidth',2)
            G = G(srt>40 & srt < 100,:); R = R(srt>40 & srt<100,:);  srt = srt(srt>40 & srt<100);

            mm = min(srt); mx = max(srt);ww = size(G,2);
            wind = 2*min([ww-mx-1,mm-1]);
            intMatG_align = zeros(size(G,1),wind+1);
            intMatR_align = zeros(size(R,1),wind+1);
            for i = 1:size(G,1)
                intMatG_align(i,:) = G(i,srt(i)-wind/2:srt(i)+wind/2);
                intMatR_align(i,:) = R(i,srt(i)-wind/2:srt(i)+wind/2);
            end

            shadedErrorBar(-wind/2:wind/2,intMatG_align,{@mean,@std},'lineProps',{'g','markerfacecolor','g','linewidth',2})
            shadedErrorBar(-wind/2:wind/2,intMatR_align,{@mean,@std},'lineProps',{'r','markerfacecolor','r','linewidth',2})
            xlabel('Aligned Time (by d/dt(GCaMP6))')
            ylabel('Fluorescence (AU)')
            if vv == 1
                legend({'GcAMP6','PI'},'location','northwest')
            end
            set(gca,'Fontsize',15,'FontName','Arial','Linewidth',2)
        end
    end
end

              










                        

cc = 0;drug_conc
indx = indx0 & (cccp==cc) & (drug_conc==conc(c));


S.time = 1:241;
endTime = 241;
conc = unique(drug_conc);
conc = conc(end:-1:1);

%% Generate Figure
% %Area vs STD over time
% fig = figure('Color','w','Position',[900 1200 2500 700]);
% mx_x = [90,90,240,240];
% for i = 1:4
%     ax1 = subplot(1,4,i); hold on
%     shadedErrorBar(S.time(1:endTime),intMatG(drug_conc==conc(i),1:endTime)-intMatG(drug_conc==conc(i),1),{@mean,@std},'lineProps',{'g','markerfacecolor','g','linewidth',2})
%     shadedErrorBar(S.time(1:endTime),intMatR(drug_conc==conc(i),1:endTime)-intMatR(drug_conc==conc(i),1),{@mean,@std},'lineProps',{'r','markerfacecolor','r','linewidth',2})
%     xlim([0,mx_x(i)])
%     xlabel('Time(min)')
%     ylabel('Fluorescence (AU)')
%     legend({'GcAMP6','PI'})
%     set(gca,'Fontsize',15,'FontName','Arial','Linewidth',2)
% end

%Establish the threshold for PI staining.
dintMatR= intMatR - mean(intMatR(:,10:20)')';
thresh = mean(std(dintMatR(:,10:20)))*3;


fig = figure('Color','w','Position',[900 1200 2500 700]);
for vv = 1:3
    subplot(2,4,vv)
    dintMatR = (intMatR(drug_conc==conc(vv),:)-mean(intMatR(drug_conc==conc(vv),10:20)')');
    dintMatR = movmean(dintMatR,10);
    srt = [];
    for i=1:length(dintMatR)
        srt(i) = find(dintMatR(i,:)>thresh,1);
    end
    hist(srt,50)
    xlabel('Time to PI exceeding threshold')
    ylabel('Count')
    set(gca,'Fontsize',15,'FontName','Arial','Linewidth',2)

    subplot(2,4,vv+4)
    dintMatG = (intMatG(drug_conc==conc(vv),:)-mean(intMatG(drug_conc==conc(vv),10:20)')');
    mm = min(srt); mx = max(srt);ww = size(intMatG,2);
    wind = 2*min([ww-mx-1,mm-1]);
    intMatG_align = zeros(length(dintMatG),wind+1);
    intMatR_align = zeros(length(dintMatR),wind+1);
    for i = 1:length(dintMatG)
        intMatG_align(i,:) = dintMatG(i,srt(i)-wind/2:srt(i)+wind/2);
        intMatR_align(i,:) = dintMatR(i,srt(i)-wind/2:srt(i)+wind/2);
    end

    shadedErrorBar(1:size(intMatG_align,2),intMatG_align,{@mean,@std},'lineProps',{'g','markerfacecolor','g','linewidth',2})
    shadedErrorBar(1:size(intMatR_align,2),intMatR_align,{@mean,@std},'lineProps',{'r','markerfacecolor','r','linewidth',2})
    xlabel('Aligned Time')
    ylabel('Fluorescence (AU)')
    if vv == 1
        legend({'GcAMP6','PI'},'location','northwest')
    end
    set(gca,'Fontsize',15,'FontName','Arial','Linewidth',2)
end

subplot(2,4,8)
dintMatR = (intMatR(drug_conc==0,:)-mean(intMatR(drug_conc==0,20:30)')');
dintMatR = movmean(dintMatR,10);
dintMatG = (intMatG(drug_conc==0,:)-mean(intMatG(drug_conc==0,20:30)')');
intMatG_align = dintMatG;
intMatR_align = dintMatR;
shadedErrorBar(1:size(intMatG_align,2),intMatG_align,{@mean,@std},'lineProps',{'g','markerfacecolor','g','linewidth',2})
shadedErrorBar(1:size(intMatR_align,2),intMatR_align,{@mean,@std},'lineProps',{'r','markerfacecolor','r','linewidth',2})
xlabel('Real Time')
ylabel('Fluorescence (AU)')
set(gca,'Fontsize',15,'FontName','Arial','Linewidth',2)







%%%%%%%%%%%%%%%%%%%%%%
%Examine PI dynamics in presences of calcium and egta +- cccp at different
%doses.

%Load in data
load('../../Datasets/03032021_PmB-PI_EGTA-InfoData.mat')
S.time = 1:241;
endTime = 241;
%% Generate Figure
%Area vs STD over time
%EGTA figure
conc = unique(drug_conc);
indx1 = ((egta==1));
indx2 = ((ca==1));

fig = figure('Color','w','Position',[900 1200 2500 700]); 
cnt = 1;
for c = 1:length(conc)
    for cc = 0:1
        subplot(3,2,cnt)
        indx = indx1 & (cccp==cc) & (drug_conc==conc(c));
        shadedErrorBar(S.time(1:endTime),intMatG(indx,1:endTime)-mean(intMatG(indx,10:20)')',{@mean,@std},'lineProps',{'g','markerfacecolor','g','linewidth',2})
        shadedErrorBar(S.time(1:endTime),intMatR(indx,1:endTime)-mean(intMatR(indx,10:20)')',{@mean,@std},'lineProps',{'r','markerfacecolor','r','linewidth',2})
        ttl = ['EGTA+, PmB Conc: ' num2str(conc(c)) ', CCCP: ' num2str(cc)];
        title(ttl)
        xlabel('Time(min)')
        ylabel('Fluorescence (AU)')
        legend({'GcAMP6','PI'})  
        cnt = cnt + 1;
        ylim([-125 125])
    end
end

fig = figure('Color','w','Position',[900 1200 2500 700]); 
cnt = 1;
for c = 1:length(conc)
    for cc = 0:1
        subplot(3,2,cnt)
        indx = indx2 & (cccp==cc) & (drug_conc==conc(c));
        shadedErrorBar(S.time(1:endTime),intMatG(indx,1:endTime)-mean(intMatG(indx,10:20)')',{@mean,@std},'lineProps',{'g','markerfacecolor','g','linewidth',2})
        shadedErrorBar(S.time(1:endTime),intMatR(indx,1:endTime)-mean(intMatR(indx,10:20)')',{@mean,@std},'lineProps',{'r','markerfacecolor','r','linewidth',2})
        ttl = ['Ca+, PmB Conc: ' num2str(conc(c)) ', CCCP: ' num2str(cc)];
        title(ttl)
        xlabel('Time(min)')
        ylabel('Fluorescence (AU)')
        legend({'GcAMP6','PI'}) 
        cnt = cnt + 1;
        ylim([-125 125])
    end
end



cd /home/meyerct6/Repos/GCAMP_paper/Data_Code/AnalysisCode/F9
%Load in data
load('../../Datasets/03072021_PmB-PI-InfoData.mat')
%Establish the threshold for PI staining.
dintMatR= intMatR - mean(intMatR(:,10:20)')';
thresh = mean(std(dintMatR(:,10:20)))*3;
conc = unique(drug_conc);
conc = conc(end:-1:1);

data_arr = {};
label = {};
thresh_arr = {};
cnt =  1;
for vv = 1:length(conc)-1
    dintMatR = (intMatR(drug_conc==conc(vv),:)-mean(intMatR(drug_conc==conc(vv),10:20)')');
    dintMatR = movmean(dintMatR,10);
    srt = zeros(size(dintMatR,1),1)*nan;
    for i=1:length(dintMatR)
        if sum(dintMatR(i,:)>thresh)>1
            srt(i) = find(dintMatR(i,:)>thresh,1);
        end
    end
    data_arr{cnt} = srt;
    label{cnt} = ['Ca-/EGTA-, PmB Conc: ' num2str(conc(vv)) ', CCCP: ' num2str(0)];
    cnt = cnt + 1;
end

load('../../Datasets/03032021_PmB-PI_EGTA-InfoData.mat')
%Establish the threshold for PI staining.
dintMatR = intMatR - mean(intMatR(:,10:20)')';
thresh = mean(std(dintMatR(:,10:20)))*3;
conc = unique(drug_conc);
conc = conc(end:-1:1);
indx1 = ((egta==1));
indx2 = ((ca==1));
for c = 1:length(conc)-1
    for cc = 0:1
        indx = indx1 & (cccp==cc) & (drug_conc==conc(c));
        dintMatR = (intMatR(indx,:)-mean(intMatR(indx,10:20)')');
        dintMatR = movmean(dintMatR,10);
        srt = zeros(size(dintMatR,1),1)*nan;
        for i=1:size(dintMatR,1)
            if sum(dintMatR(i,:)>thresh)>1
                srt(i) = find(dintMatR(i,:)>thresh,1);
            end
        end
        data_arr{cnt} = srt;
        label{cnt} = ['Ca-/EGTA+, PmB Conc: ' num2str(conc(c)) ', CCCP: ' num2str(cc)];
        cnt = cnt + 1;
    end
end
for c = 1:length(conc)-1
    for cc = 0:1
        indx = indx2 & (cccp==cc) & (drug_conc==conc(c));
        dintMatR = (intMatR(indx,:)-mean(intMatR(indx,10:20)')');
        dintMatR = movmean(dintMatR,10);
        srt = zeros(size(dintMatR,1),1)*nan;
        for i=1:size(dintMatR,1)
            if sum(dintMatR(i,:)>thresh)>1
                srt(i) = find(dintMatR(i,:)>thresh,1);
            end
        end
        data_arr{cnt} = srt;
        label{cnt} = ['Ca+/EGTA-, PmB Conc: ' num2str(conc(c)) ', CCCP: ' num2str(cc)];
        cnt = cnt + 1;
    end
end



fig = figure('Color','w','Position',[900 1200 2500 700]); 
[h, L, MX, MED, bw] = violin(data_arr,'bw',10)
set(gca,'xtick',1:length(label))
set(gca,'xticklabels',label)
xtickangle(45)

fig = figure('Color','w','Position',[900 1200 2500 700]); 
bar(MED)
set(gca,'xtick',1:length(label))
set(gca,'xticklabels',label)
xtickangle(45)
ylim([40,200])






%Load in data
load('../../Datasets/03032021_PmB-PI_EGTA-InfoData.mat')
dintMatR= intMatR - mean(intMatR(:,10:20)')';
thresh = mean(std(dintMatR(:,10:20)))*3;
S.time = 1:241;
endTime = 241;
%% Generate Figure
%Area vs STD over time
%EGTA figure
conc = unique(drug_conc);
conc = conc(end:-1:1);
indx1 = ((egta==1));
indx2 = ((ca==1));

fig = figure('Color','w','Position',[900 1200 2500 700]); 
cnt = 1;
for c = 1:length(conc)-1
    for cc = 0:1
        subplot(2,2,cnt)
        indx = indx1 & (cccp==cc) & (drug_conc==conc(c));

        dintMatR = (intMatR(indx,:)-mean(intMatR(indx,10:20)')');
        dintMatR = movmean(dintMatR,10);
        srt = zeros(size(dintMatR,1),1);
        for i=1:size(dintMatR,1)
            if sum(dintMatR(i,:)>thresh)>1
                srt(i) = find(dintMatR(i,:)>thresh,1);
            end
        end
        dintMatG = (intMatG(indx,:)-mean(intMatG(indx,10:20)')');
        dintMatR = dintMatR(srt>0,:); dintMatG = dintMatG(srt>0,:); srt = srt(srt>0);
        mm = min(srt); mx = max(srt);ww = size(intMatG,2);
        wind = 2*min([ww-mx-1,mm-1]);
        intMatG_align = zeros(length(dintMatG),wind+1);
        intMatR_align = zeros(length(dintMatR),wind+1);
        for i = 1:size(dintMatG,1)
            intMatG_align(i,:) = dintMatG(i,srt(i)-wind/2:srt(i)+wind/2);
            intMatR_align(i,:) = dintMatR(i,srt(i)-wind/2:srt(i)+wind/2);
        end
        shadedErrorBar(1:size(intMatG_align,2),intMatG_align,{@mean,@std},'lineProps',{'g','markerfacecolor','g','linewidth',2})
        shadedErrorBar(1:size(intMatR_align,2),intMatR_align,{@mean,@std},'lineProps',{'r','markerfacecolor','r','linewidth',2})
        ttl = ['Ca-/EGTA+, PmB Conc: ' num2str(conc(c)) ', CCCP: ' num2str(cc)];
        title(ttl)
        xlabel('Time(min)')
        ylabel('Fluorescence (AU)')
        legend({'GcAMP6','PI'})  
        cnt = cnt + 1;
        ylim([-125 125])
    end
end
fig = figure('Color','w','Position',[900 1200 2500 700]); 
cnt = 1;
for c = 1:length(conc)-1
    for cc = 0:1
        subplot(2,2,cnt)
        indx = indx2 & (cccp==cc) & (drug_conc==conc(c));

        dintMatR = (intMatR(indx,:)-mean(intMatR(indx,10:20)')');
        dintMatR = movmean(dintMatR,10);
        srt = zeros(size(dintMatR,1),1);
        for i=1:size(dintMatR,1)
            if sum(dintMatR(i,:)>thresh)>1
                srt(i) = find(dintMatR(i,:)>thresh,1);
            end
        end
        dintMatG = (intMatG(indx,:)-mean(intMatG(indx,10:20)')');
        dintMatR = dintMatR(srt>0,:); dintMatG = dintMatG(srt>0,:); srt = srt(srt>0);
        mm = min(srt); mx = max(srt);ww = size(intMatG,2);
        wind = 2*min([ww-mx-1,mm-1]);
        intMatG_align = zeros(length(dintMatG),wind+1);
        intMatR_align = zeros(length(dintMatR),wind+1);
        for i = 1:size(dintMatG,1)
            intMatG_align(i,:) = dintMatG(i,srt(i)-wind/2:srt(i)+wind/2);
            intMatR_align(i,:) = dintMatR(i,srt(i)-wind/2:srt(i)+wind/2);
        end
        shadedErrorBar(1:size(intMatG_align,2),intMatG_align,{@mean,@std},'lineProps',{'g','markerfacecolor','g','linewidth',2})
        shadedErrorBar(1:size(intMatR_align,2),intMatR_align,{@mean,@std},'lineProps',{'r','markerfacecolor','r','linewidth',2})
        ttl = ['Ca+/EGTA-, PmB Conc: ' num2str(conc(c)) ', CCCP: ' num2str(cc)];
        title(ttl)
        xlabel('Time(min)')
        ylabel('Fluorescence (AU)')
        legend({'GcAMP6','PI'})  
        cnt = cnt + 1;
        ylim([-125 125])
    end
end
