cd /home/meyerct6/Repos/GCAMP_paper/Data_Code/AnalysisCode/F8_SF8_SF9_SF10
%Load in data
load('../../Datasets/03032021_PmB-PI_EGTA-InfoData.mat')

S.time = 1:241
endTime = 241
%% Generate Figure
%Area vs STD over time
fig = figure('Color','w','Position',[900 1200 2500 700]);
mx_x = [90,90,240];
conc = 5;
well_indx = {'A11','A12','A10'};
for i = 1:3
    ax1 = subplot(1,3,i); hold on
    indx = find(contains(wellNm,well_indx{i}));
    shadedErrorBar(S.time(1:endTime),intMatG(indx,1:endTime)-intMatG(indx,1),{@mean,@std},'lineProps',{'g','markerfacecolor','g','linewidth',2})
    shadedErrorBar(S.time(1:endTime),intMatR(indx,1:endTime)-intMatR(indx,1),{@mean,@std},'lineProps',{'r','markerfacecolor','r','linewidth',2})
    xlim([0,mx_x(i)])
    xlabel('Time(min)')
    ylabel('Fluorescence (AU)')
    legend({'GcAMP6','PI'})
    set(gca,'Fontsize',15,'FontName','Arial','Linewidth',2)
end



fig = figure('Color','w','Position',[900 1200 2500 700]);
for vv = 1:3
    subplot(2,4,vv)
    dintMatR = (intMatR(drug_conc==conc(vv),:)-mean(intMatR(drug_conc==conc(vv),20:30)')');
    thresh = mean(mean(dintMatR(:,20:30)'))+3*max(std(dintMatR(:,20:30)'));
    dintMatR = movmean(dintMatR,10);
    srt = []
    for i=1:length(dintMatR)
        srt(i) = find(dintMatR(i,:)>thresh,1);
    end
    hist(srt,50)
    xlabel('Time to PI exceeding threshold')
    ylabel('Count')
    set(gca,'Fontsize',15,'FontName','Arial','Linewidth',2)

    subplot(2,4,vv+4)
    dintMatG = (intMatG(drug_conc==conc(vv),:)-mean(intMatG(drug_conc==conc(vv),20:30)')');
    mm = min(srt); mx = max(srt);ww = size(intMatG,2);
    wind = 2*min([ww-mx-1,mm-1]);
    intMatG_align = zeros(length(dintMatG),wind+1);
    intMatR_align = zeros(length(dintMatR),wind+1);
    for i = 1:length(dintMatG)
        intMatG_align(i,:) = dintMatG(i,srt(i)-wind/2:srt(i)+wind/2);
        intMatR_align(i,:) = dintMatR(i,srt(i)-wind/2:srt(i)+wind/2);
    end

    shadedErrorBar(1:size(intMatG_align,2),intMatG_align,{@mean,@std},'lineProps',{'g','markerfacecolor','g','linewidth',2})
    shadedErrorBar(1:size(intMatR_align,2),intMatR_align,{@mean,@std},'lineProps',{'r','markerfacecolor','r','linewidth',2})
    xlabel('Aligned Time')
    ylabel('Fluorescence (AU)')
    if vv == 1
        legend({'GcAMP6','PI'},'location','northwest')
    end
    set(gca,'Fontsize',15,'FontName','Arial','Linewidth',2)
end

subplot(2,4,8)
dintMatR = (intMatR(drug_conc==0,:)-mean(intMatR(drug_conc==0,20:30)')');
dintMatR = movmean(dintMatR,10);
dintMatG = (intMatG(drug_conc==0,:)-mean(intMatG(drug_conc==0,20:30)')');
intMatG_align = dintMatG;
intMatR_align = dintMatR;
shadedErrorBar(1:size(intMatG_align,2),intMatG_align,{@mean,@std},'lineProps',{'g','markerfacecolor','g','linewidth',2})
shadedErrorBar(1:size(intMatR_align,2),intMatR_align,{@mean,@std},'lineProps',{'r','markerfacecolor','r','linewidth',2})
xlabel('Real Time')
ylabel('Fluorescence (AU)')
set(gca,'Fontsize',15,'FontName','Arial','Linewidth',2)


