%Read in tifs


%%Script to analyze the GCAMP signal in mScarlet and GCAMP labeled E coli
%%cells over a short time course exposure to antibiotic
%Assumes file structure from the circlescope with a "Well_treatments.xlsx"
%file to label the different conditions

%Modified by Christian Meyer 01.07.2020, Commented up to date
%%Step 1:  Set up experiment to process
%Clear everything and start a timer
clear; close all;tic
%Set active directory
activeDir    = '/mnt/kralj_celery/E coli GC antibioitic/2021-03-03-fluorescein_diffusion_test_cm';
cd(activeDir) %Move into the experimental directory
flist        = dir('*.tif'); %Current file list
nfiles       = length(flist); %Number of files
dt = 1;
nColor = 1;


%% Step 3: For each image load in stack and process (correct, register, segment, extract traces)
% Load in each pair of colors individually, process in a parallel loop
warning('off');
well = {};mean_int=[];cnt=1;
for f = 1:nfiles/nColor
   warning('off')
    %Get image information
    fName = cell(nColor,1);
    fName{1} = flist((nColor*f) - (nColor-1)).name;
    for cc = 2:nColor
        fName{cc} = [fName{1}(1:end-6) clist{(nColor*(f-1)) + (cc)}];
    end
    disp([num2str(f) ' of ' num2str(nfiles/nColor) ': ' fName{1}]);
    %Get stack info
    tifInfo = imfinfo(fName{1});    nframes = length(tifInfo);
    %Create time vector
    time = 60*(0:nframes-1)*dt;
    
    %Read in image stack
    G = bigread2(fName{1});
    [ysize,xsize,nframes] = size(G);
    
    tmp = regexp(fName{1},'_','split');
    replicate = ['_' tmp{2}];
    tmp = regexp(tmp{1},' - ','split');
    tmp = tmp{2};
    if str2double(tmp(2:end)) < 10
        currWell = [tmp(1) '0' tmp(end)];
    else
        currWell = tmp;
    end
    mean_int = [mean_int;mean(G,[1,2])];
    well{cnt} = currWell;
    cnt = cnt + 1;
end

mean_int = squeeze(mean_int);
fig = figure('color','w','Position',[10 10 500 200])
hold on
shadedErrorBar(time,mean_int(contains(well,'A'),:),{@mean,@std},'lineProps',{'r','markerfacecolor','r','linewidth',2})
shadedErrorBar(time,mean_int(contains(well,'E'),:),{@mean,@std},'lineProps',{'b','markerfacecolor','b','linewidth',2})
legend({'Pad','Liquid'},'location','southeast')
xlabel('Time (min)')
ylabel('Intensity (AU)')
set(gca,'linewidth',2,'fontsize',8)
saveas(fig,'pad_diffusion_max_conc.pdf')




fig = figure('color','w','Position',[10 10 350 240])
hold on
wlls = {'A','B','C'};
cm_cols = plasma(4);
for j=1:length(wlls)
    shadedErrorBar(time/60,mean_int(contains(well,wlls{j}),:),{@mean,@std},'lineProps',{'color',cm_cols(j,:),'markerfacecolor',cm_cols(j,:),'linewidth',2})
    disp(mean(mean_int(contains(well,wlls{j}),end)))
end
legend({'Pad [1X]','Pad [1/2X]','Pad [1/4X]'},'location','northoutside','numcolumns',5)
xlabel('Time (min)')
ylabel('Intensity (AU)')
set(gca,'linewidth',2,'fontsize',8)
saveas(fig,'pad_diffusion_all_conc.pdf')




fig = figure('color','w','Position',[10 10 350 240])
hold on
wlls = {'A','B','C'};
cm_cols = plasma(4);
for j=1:length(wlls)
    plot(time/60,mean(mean_int(contains(well,wlls{j}),:)./mean_int(contains(well,wlls{1}),:)),'color',cm_cols(j,:),'markerfacecolor',cm_cols(j,:),'linewidth',2)
end
legend({'Pad [1X]','Pad [1/2X]','Pad [1/4X]'},'location','northoutside','numcolumns',5)
xlabel('Time (min)')
ylabel('Ratio')
set(gca,'linewidth',2,'fontsize',8)
saveas(fig,'pad_diffusion_ratio_conc.pdf')




