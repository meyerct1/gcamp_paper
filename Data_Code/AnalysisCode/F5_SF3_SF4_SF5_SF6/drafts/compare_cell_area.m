
%Change into the correct directory
cd '~/Repos/GCAMP_paper/Data_Code/Figure 4/'

%% Step 1: Read in data generated from the "extract_GMmS_TIF_2color_parallel.m" file
%Clear everything and start a timer
clear; close all; tic
%Load in data
load('../../Datasets/02262021_AllSingleCellInfoData.mat')


%% Hyperparameter sweep for svm
indx = find(((kan_r==1)&(contains(drugNm,'kan'))) | (((kan_r==0)&(drug_conc==100)&(contains(drugNm,'kan'))) | ((kan_r==0)&(drug_conc==0))));
deltaT = [241];dT=1;
master_data = cellArea(indx,1:deltaT(dT));
tmp_kan_r = kan_r(indx);tmp_drug_conc=drug_conc(indx);tmp_drug_nm = drugNm(indx);
master_label = ones(length(tmp_kan_r),1);
%Sensitive cells are zero
master_label((tmp_kan_r==0)&(tmp_drug_conc==100)&(contains(tmp_drug_nm,'kan'))) = 0;


%% Hyperparameter sweep
%Split the data into training and testing
rng(5)
disp(['Starting Model Learning, deltaT: ' num2str(deltaT)])
%Use only 5% of the data for training the model
spt = rand(length(master_data),1)> 0.8;
test = master_data(spt,:);
train = master_data(spt==0,:);
test_lab = master_label(spt,:);
train_lab = master_label(spt==0,:);
%Set up template Tree
%See https://www.mathworks.com/help/stats/templatetree.html for list of
%hyperparameters
numWorkers = 24;
p = gcp('nocreate'); % If no pool, do not create new one. ~14Gb/exp
if isempty(p)
    parpool(numWorkers);
end

Mdl1 = fitcsvm(train,train_lab,'OptimizeHyperparameters','auto',...
                    'HyperparameterOptimizationOptions',...
              struct('AcquisitionFunctionName','expected-improvement-plus',...
                    'MaxObjectiveEvaluations',200,...
                    'UseParallel',true,...
                    'Repartition',true));

                                   

saveas(gcf,'Mdl1_HyperParameterSearch-02232021-CellArea.fig')  
save('Mdl1_HyperParameterSearch-02232021-CellArea.mat','Mdl1','-v7.3')



Mdl2 = fitcsvm(train,train_lab,'BoxConstraint',XXX,'KernelScale',XXX);

%% Plots for best model
auc = zeros(2,1);
fig = figure('Color','w','Position',[900 1200 215 130]);
ax = subplot(1,1,1);
hold on
for k = 0
    [xVal,yVal,~,auc(k+1)] = perfcurve(test_lab(1:end),predict_score(1:end,k+1),k);
    stp = round(length(xVal)*.01);
    plot(ax,xVal(1:stp:end),yVal(1:stp:end),'Linewidth',2);
    text(0.25,.15*(k+1),strcat('AUC=',num2str(auc(k+1))),'FontSize',8);
end
plot(xVal(1:stp:end),xVal(1:stp:end),'--r','linewidth',2)
xlabel('False Positive Rate');
ylabel('True Positive Rate');
set(gca,'Linewidth',2,'FontSize',8,'FontName','Arial')
saveas(fig,'fullTime_kan_CELLAREA_roc.pdf')


cp = classperf(test_lab,predict_labels);
Mdl = Mdl2;

%Plot the loss as a function of training rounds
kflc = kfoldLoss(Mdl,'Mode','cumulative');
fig = figure('Color','w','Position',[900 1200 215 130]);
plot(kflc*100,'linewidth',2);
ylabel({'10-fold','Error rate'});
xlabel('Learning cycle');
set(gca,'Linewidth',2,'FontSize',8,'FontName','Arial')
tightfig
saveas(fig,'fullTime_kan_CELLAREA_learningCycle.pdf')

fig = figure('Color','w','Position',[900 1200 215 130]);
plot(modelLosses,'linewidth',2);
xlabel('Fold')
ylabel('Model Loss')
set(gca,'Linewidth',2,'FontSize',8,'FontName','Arial')
saveas(fig,'fullTime_kan_CELLAREA_kfoldLoss.pdf')

%Plot confusion chart
fig = figure('Color','w','Position',[900 1200 215 130]);
[predict_labels,predict_score] = predict(Mdl.Trained{find(modelLosses==min(modelLosses))}, test);
c = confusionchart(confusionmat(test_lab,predict_labels),{'Sens','Res'},'Normalization','row-normalized');
set(gca,'FontSize',8,'FontName','Arial')
saveas(fig,'fullTime_kan_CELLAREA_confusionMat.pdf')






%% For different deltaT estimate the predictivity of the RandomForest learning algorithms
%Calculate feature matrices based on 1:deltaT frames
deltaT = 10:2:240;
kFoldCnt = 10;
aucComp = zeros(length(deltaT),kFoldCnt);
errorRate = zeros(length(deltaT),kFoldCnt);
correctRate = zeros(length(deltaT),kFoldCnt);

aucCompCon = zeros(length(deltaT),kFoldCnt);
errorRateCon = zeros(length(deltaT),kFoldCnt);
correctRateCon = zeros(length(deltaT),kFoldCnt);
numWorkers = 24;
p = gcp('nocreate'); % If no pool, do not create new one. ~14Gb/exp
if isempty(p)
    parpool(numWorkers);
end
indx = find(((kan_r==1)&(contains(drugNm,'kan'))) | (((kan_r==0)&(drug_conc==100)&(contains(drugNm,'kan'))) | ((kan_r==0)&(drug_conc==0))));

parfor dT = 1:length(deltaT)
    rng(5); %Set random number generator for reproducibility
    disp(['Model: ' num2str(deltaT(dT))])
    master_data = cellArea(indx,1:deltaT(dT));
    tmp_kan_r = kan_r(indx);tmp_drug_conc=drug_conc(indx);tmp_drug_nm = drugNm(indx);
    master_label = ones(length(tmp_kan_r),1);
    %Sensitive cells are zero
    master_label((tmp_kan_r==0)&(tmp_drug_conc==100)&(contains(tmp_drug_nm,'kan'))) = 0;

    for j = 1:kFoldCnt
        %Keep an equal amount of sensitive and resistant cells for training
        mn = min([sum(master_label==1),sum(master_label==0)]);
        idx_sens = find(master_label==0);idx_res = find(master_label==1);
        idx_sens = idx_sens(randperm(length(idx_sens)));
        idx_res = idx_res(randperm(length(idx_res)));
        idx_sens = idx_sens(1:mn);idx_res=idx_res(1:mn);
        idx = union(idx_res,idx_sens);
        tmp_master_data = master_data(idx,:);
        tmp_master_label = master_label(idx);

        %Use 80% of the data for training the model
        spt = rand(length(tmp_master_label),1)> 0.8;
        test = tmp_master_data(spt,:);
        train = tmp_master_data(spt==0,:);
        test_lab = tmp_master_label(spt,:);
        train_lab = tmp_master_label(spt==0,:);
        %Set up template Tree
        %See https://www.mathworks.com/help/stats/templatetree.html for list of
        %hyperparameters
        %%%SET PARAMETERS HERE!!!!!!
        Mdl1 = fitcsvm(train,train_lab,'BoxConstraint',XXX,'KernelScale',XXX);

        [predict_labels,predict_score] = predict(Mdl1, test);
        [~,~,~,auc] = perfcurve(test_lab,predict_score(:,1),0);
        %c = confusionmat(test_lab,predict_labels);
        cp1 = classperf(test_lab,predict_labels);
        aucComp(dT,j) = auc;
        errorRate(dT,j) = cp1.ErrorRate;
        correctRate(dT,j) = cp1.CorrectRate;


        %%Randomize and measure control
        tmp_train = train(randperm(length(train)),:);
        Mdl2 = fitcsvm(train,train_lab,'BoxConstraint',XXX,'KernelScale',XXX);

        [predict_labels,predict_score] = predict(Mdl2, test);
        [~,~,~,auc] = perfcurve(test_lab,predict_score(:,1),0);
        %c = confusionmat(test_lab,predict_labels);
        cp2 = classperf(test_lab,predict_labels);
        aucCompCon(dT,j) = auc;
        errorRateCon(dT,j) = cp2.ErrorRate;
        correctRateCon(dT,j) = cp2.CorrectRate;
    end
end

save('deltaT_vs_kan_CELLAREA-Model.mat','deltaT','aucComp','aucCompCon','errorRate','errorRateCon','correctRate','correctRateCon')

fig = figure('Color','w','Position',[900 1200 200 120]);
ax1 = subplot(1,1,1);
shadedErrorBar(deltaT,errorRate'*100,{@mean,@std},'lineProps',{'r','markerfacecolor','r','linewidth',2})
shadedErrorBar(deltaT,errorRateCon'*100,{@mean,@std},'lineProps',{'b','markerfacecolor','b','linewidth',2})
xlabel('\Deltat')
ylabel('Error Rate')
ylim([0,60])
p = xline(min(deltaT(mean(errorRate')<.05)),'c','linewidth',2);
set(get(get(p,'Annotation'),'LegendInformation'),'IconDisplayStyle','off')
legend({'Training Data','Scramble Control'},'Location','East')
tightfig
set(gca,'Linewidth',2,'FontSize',8,'FontName','Arial')
saveas(fig,'deltaT_kan_ml_CELLAREA_error.pdf')

fig = figure('Color','w','Position',[900 1200 200 120]);
ax1 = subplot(1,1,1);
shadedErrorBar(deltaT,aucComp',{@mean,@std},'lineProps',{'r','markerfacecolor','r','linewidth',2})
shadedErrorBar(deltaT,aucCompCon',{@mean,@std},'lineProps',{'b','markerfacecolor','b','linewidth',2})
xlabel('\Deltat')
ylabel('ROC AUC')
ylim([.4,1])
p = xline(min(deltaT(mean(errorRate')<.05)),'c','linewidth',2);
set(get(get(p,'Annotation'),'LegendInformation'),'IconDisplayStyle','off')
legend({'Training Data','Scramble Control'},'Location','East')
tightfig
set(gca,'Linewidth',2,'FontSize',8,'FontName','Arial')
saveas(fig,'deltaT_kan_ml_CELLAREA_auc.pdf')





