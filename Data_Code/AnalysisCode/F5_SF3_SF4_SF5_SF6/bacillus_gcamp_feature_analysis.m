cd '/home/meyerct6/Repos/GCAMP_paper/Data_Code/AnalysisCode/F5_SF3_SF4_SF5_SF6'
addpath(genpath('/home/meyerct6/Repos/GCAMP_paper/Data_Code/Functions'))

load('../../Datasets/04232021_bacillus_kan_gcamp.mat')

%% For the full time course (deltaT=241) find the best approach using the HyperOptimization in Matlab
deltaT = [241]; dT = 1;

%Subset out the kanamycin treated and untreated cells to train model
indx = find(((kan_r==1)&(contains(drugNm,'Kan'))) | (((kan_r==0)&(drug_conc==100)&(contains(drugNm,'Kan'))) | ((kan_r==0)&(drug_conc==0))));
S = struct();
S.meanIntG = intMatG(indx,1:deltaT(dT));
S.maxIntG = maxIntMatG(indx,1:deltaT(dT));
S.minIntG = minIntMatG(indx,1:deltaT(dT));
S.stdIntG = minIntMatG(indx,1:deltaT(dT));
tmp_kan_r = kan_r(indx);tmp_drug_conc=drug_conc(indx);tmp_drug_nm = drugNm(indx);
master_label = ones(length(tmp_kan_r),1);
%Sensitive cells are zero
master_label((tmp_kan_r==0)&(tmp_drug_conc==100)&(contains(tmp_drug_nm,'Kan'))) = 0;

%Calculate Feature Matrix:
[master_data,mastLab] = calcFeatureMatrix(S,deltaT(1));

dat = S.meanIntG;

tst_ids = find(contains(mastLab,'Min_meanIntG_norm'));
for i = 1:length(tst_ids)
    if strcmp(mastLab{tst_ids(i)},'Min_meanIntG_norm')==1
        disp(tst_ids(i))
        mtch_id = tst_ids(i);
        break
    end
end


fig = figure('Color','w','Position',[900 1200 215 130]);
h = boxplot(master_data(:,mtch_id),master_label,'boxstyle','outline','colors',['b','k'],'plotstyle','traditional','symbol','','notch','on');
set(h,'LineWidth',2)
xticklabels({'Sensitive','Resistant'})
title('Feature: Min(GCaMP6), normalized','FontSize',8,'Fontweight','normal')
ylabel('min(GCaMP6)')
set(gca,'Linewidth',2,'FontSize',8,'FontName','Arial')
saveas(fig,'plots/bacillus_feature_minGCaMP6.pdf')


[h,p] = ttest2(master_data(master_label==0,mtch_id),master_data(master_label==1,mtch_id));

% 
% 
% shadedErrorBar(1:size(dat,2),dat(master_label==1,:),{@mean,@std},'lineprops',{'r','markerfacecolor','r','linewidth',2})
% hold on
% shadedErrorBar(1:size(dat,2),dat(master_label==0,:),{@mean,@std},'lineprops',{'b','markerfacecolor','b','linewidth',2})