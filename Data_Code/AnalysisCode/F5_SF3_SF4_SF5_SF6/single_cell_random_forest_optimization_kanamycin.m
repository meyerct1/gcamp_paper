%Change into the correct directory
cd '/home/meyerct6/Repos/GCAMP_paper/Data_Code/AnalysisCode/F5_SF3_SF4_SF5_SF6'
addpath(genpath('/home/meyerct6/Repos/GCAMP_paper/Data_Code/Functions'))

%% Step 1: Read in data generated from the "extract_GMmS_TIF_2color_parallel.m" file
%Clear everything and start a timer
clear; close all; tic
%Load in data
load('../../Datasets/02262021_AllSingleCellInfoData.mat')


%% For the full time course (deltaT=241) find the best approach using the HyperOptimization in Matlab
deltaT = [241]; dT = 1;

%Subset out the kanamycin treated and untreated cells to train model
indx = find(((kan_r==1)&(contains(drugNm,'kan'))) | (((kan_r==0)&(drug_conc==100)&(contains(drugNm,'kan'))) | ((kan_r==0)&(drug_conc==0))));
S = struct();
S.meanIntG = intMatG(indx,1:deltaT(dT));
S.maxIntG = maxIntMatG(indx,1:deltaT(dT));
S.minIntG = minIntMatG(indx,1:deltaT(dT));
S.stdIntG = minIntMatG(indx,1:deltaT(dT));

%Calculate Feature Matrix:
[master_data,mastLab] = calcFeatureMatrix(S,deltaT(dT));

%Label cells as either Resistant or Sensitive.  Include untreated
%sensitive as resistant cells!
tmp_kan_r = kan_r(indx);tmp_drug_conc=drug_conc(indx);tmp_drug_nm = drugNm(indx);
master_label = ones(length(tmp_kan_r),1);
%Sensitive cells are zero
master_label((tmp_kan_r==0)&(tmp_drug_conc==100)&(contains(tmp_drug_nm,'kan'))) = 0;

%Normalize data
master_data = normalize(master_data);
%Remove nan cells and columns    
master_data = master_data(:,sum(isnan(master_data))~=length(master_data));
master_label = master_label(sum(isnan(master_data'))==0);
master_data = master_data(sum(isnan(master_data'))==0,:);

%PCA dimensionality reduction
[coeff,score,~,~,explained,mu] = pca(master_data);
% figure()
% hold on
% scatter(score(master_label==0,1),score(master_label==0,2))
% scatter(score(master_label==1,1),score(master_label==1,2))

%Find the index that retains 95% of the variance
expl = 0;
for i=1:length(explained)
    expl = expl+explained(i);
    if expl>95
        pca_dim = i;
        break
    end
end
master_data = score(:,1:pca_dim);

%% Hyperparameter sweep
%Split the data into training and testing
rng(5)
disp(['Starting Model Learning, deltaT: ' num2str(deltaT)])
%Use only 5% of the data for training the model
spt = rand(length(master_data),1)> 0.8;
test = master_data(spt,:);
train = master_data(spt==0,:);
test_lab = master_label(spt,:);
train_lab = master_label(spt==0,:);
%Set up template Tree
%See https://www.mathworks.com/help/stats/templatetree.html for list of
%hyperparameters
numWorkers = 24;
p = gcp('nocreate'); % If no pool, do not create new one. ~14Gb/exp
if isempty(p)
    parpool(numWorkers);
end
t = templateTree('Reproducible',true);

%Save model as a compact ensemble
Mdl0 = fitcensemble(train,train_lab,'OptimizeHyperparameters','all',...
                                    'Learners',t,...
                                    'HyperparameterOptimizationOptions',struct('AcquisitionFunctionName','expected-improvement-plus',...
                                                                                'MaxObjectiveEvaluations',200,...
                                                                                'UseParallel',true,...
                                                                                'Repartition',true));
                                                                            
saveas(gcf,'hyperparameter-search/Mdl0_HyperParameterSearch-02232021.fig')  
save('models/Mdl0_HyperParameterSearch-02232021.mat','Mdl0','-v7.3')

%Find the hyperparameter set that is optimal for each drug, run a subsequent
%hyperoptimization for number of learning cycles, learn rate, maxnumsplits
%Use AdaBoostM1
Mdl1 = fitcensemble(train,train_lab,'Method','AdaBoostM1','OptimizeHyperparameters',{'NumLearningCycles','LearnRate','MaxNumSplits','MinLeafSize'},'Learners',t,...
                                                'HyperparameterOptimizationOptions',struct( 'MaxObjectiveEvaluations',200,...
                                                        'UseParallel',true,...
                                                        'Repartition',true));
saveas(gcf,'hyperparameter-search/Mdl1_HyperParameterSearch-02232021.fig')  
save('models/Mdl1_HyperParameterSearch-02232021.mat','Mdl0','-v7.3')

kFoldCnt = 10;
%% Select best model!
rng(5);
%Best according to hyperparameter optimization
t = templateTree('Reproducible',true,'MinLeafSize',7,'MaxNumSplits',34);
Mdl2 = fitcensemble(train,train_lab,'Method','AdaBoostM1','NumLearningCycles',442,'LearnRate',0.94414,'CrossVal','on','KFold',kFoldCnt,'NPrint',0);

save('models/Mdl2_gCAMP_kan.mat','Mdl2','-v7.3')
modelLosses = kfoldLoss(Mdl2,'mode','individual');
[predict_labels,predict_score] = predict(Mdl2.Trained{find(modelLosses==min(modelLosses))}, test);

%% Plots for best model
auc = zeros(2,1);
fig = figure('Color','w','Position',[900 1200 215 130]);
ax = subplot(1,1,1);
hold on
for k = 0
    [xVal,yVal,~,auc(k+1)] = perfcurve(test_lab(1:end),predict_score(1:end,k+1),k);
    stp = round(length(xVal)*.01);
    plot(ax,xVal(1:stp:end),yVal(1:stp:end),'Linewidth',2);
    text(0.25,.15*(k+1),strcat('AUC=',num2str(auc(k+1))),'FontSize',8);
end
plot(xVal(1:stp:end),xVal(1:stp:end),'--r','linewidth',2)
xlabel('False Positive Rate');
ylabel('True Positive Rate');
set(gca,'Linewidth',2,'FontSize',8,'FontName','Arial')
saveas(fig,'plots/fullTime_kan_gcamp_roc.pdf')


cp = classperf(test_lab,predict_labels);
Mdl = Mdl2;

%Plot the loss as a function of training rounds
kflc = kfoldLoss(Mdl,'Mode','cumulative');
fig = figure('Color','w','Position',[900 1200 215 130]);
plot(kflc*100,'linewidth',2);
ylabel({'10-fold','Error rate'});
xlabel('Learning cycle');
set(gca,'Linewidth',2,'FontSize',8,'FontName','Arial')
tightfig
saveas(fig,'plots/fullTime_kan_gcamp_learningCycle.pdf')

fig = figure('Color','w','Position',[900 1200 215 130]);
plot(modelLosses,'linewidth',2);
xlabel('Fold')
ylabel('Model Loss')
ylim([0 .11])
set(gca,'Linewidth',2,'FontSize',8,'FontName','Arial')
saveas(fig,'plots/fullTime_kan_gcamp_kfoldLoss.pdf')

%Plot confusion chart
fig = figure('Color','w','Position',[900 1200 215 130]);
[predict_labels,predict_score] = predict(Mdl.Trained{find(modelLosses==min(modelLosses))}, test);
c = confusionchart(confusionmat(test_lab,predict_labels),{'Sens','Res'},'Normalization','row-normalized');
set(gca,'FontSize',8,'FontName','Arial')
saveas(fig,'plots/fullTime_kan_gcamp_confusionMat.pdf')

% PLot the feature importance
imp = predictorImportance(Mdl.Trained{find(modelLosses==min(modelLosses))});
fig = figure('Color','w','Position',[900 1200 215 130]);
b = bar(sort(imp),'edgealpha',0);
xticks([])
yticks([])
ylabel({'Relative Predictor' ,'Importance'});
xlabel('PCA component')
set(gca,'Linewidth',2,'FontSize',8,'FontName','Arial')
saveas(fig,'plots/fullTime_kan_gcamp_featureImportance.pdf')


[m,i1] = max(imp);
[s,i2] = sort(coeff(:,i1));
%Top loadings
for i=0:9
    disp(mastLab{i2(end-i)})
end
%Top loadings
for i=1:10
    disp(mastLab{i2(i)})
end
fig = figure('Color','w','Position',[900 1200 215 130]);
b = bar(sort(coeff(:,i1)),'edgealpha',0);
xticks([])
ylabel('Eigenvalue');
xlabel('Feature')
title({['PCA ' num2str(i1)] ,['Variance Explained: ' num2str(explained(i1),2) '%']})
set(gca,'Linewidth',2,'FontSize',8,'FontName','Arial')
saveas(fig,'plots/fullTime_kan_gcamp_mostImportantComponent.pdf')






