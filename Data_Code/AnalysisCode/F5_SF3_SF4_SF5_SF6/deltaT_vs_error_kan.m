%Change into the correct directory
cd '/home/meyerct6/Repos/GCAMP_paper/Data_Code/AnalysisCode/F5_SF3_SF4_SF5_SF6'
addpath(genpath('/home/meyerct6/Repos/GCAMP_paper/Data_Code/Functions'))

%% Step 1: Read in data generated from the "extract_GMmS_TIF_2color_parallel.m" file
%Clear everything and start a timer
clear; close all; tic
%Load in data
load('../../Datasets/02262021_AllSingleCellInfoData.mat')


%% For different deltaT estimate the predictivity of the RandomForest learning algorithms
%Calculate feature matrices based on 1:deltaT frames
deltaT = 10:2:240;
kFoldCnt = 10;
aucComp = zeros(length(deltaT),kFoldCnt);
errorRate = zeros(length(deltaT),kFoldCnt);
correctRate = zeros(length(deltaT),kFoldCnt);

aucCompCon = zeros(length(deltaT),kFoldCnt);
errorRateCon = zeros(length(deltaT),kFoldCnt);
correctRateCon = zeros(length(deltaT),kFoldCnt);

numWorkers = 24;  %Number of workers!
p = gcp('nocreate'); % If no pool, do not create new one. 
if isempty(p)
    parpool(numWorkers);
end
indx = find(((kan_r==1)&(contains(drugNm,'kan'))) | (((kan_r==0)&(drug_conc==100)&(contains(drugNm,'kan'))) | ((kan_r==0)&(drug_conc==0))));


parfor dT = 1:length(deltaT)
    rng(5); %Set random number generator for reproducibility
    disp(['Model: ' num2str(deltaT(dT))])
    S = struct();
    S.meanIntG = intMatG(indx,1:deltaT(dT));
    S.maxIntG = maxIntMatG(indx,1:deltaT(dT));
    S.minIntG = minIntMatG(indx,1:deltaT(dT));
    S.stdIntG = minIntMatG(indx,1:deltaT(dT));

    %Calculate Feature Matrix:
    [master_data,mastLab] = calcFeatureMatrix(S,deltaT(dT));


    %Use label cells Label cells as either Resistant or Sensitive.  Include untreated
    %sensitive as resistant cells!
    tmp_kan_r = kan_r(indx);tmp_drug_conc=drug_conc(indx);tmp_drug_nm = drugNm(indx);
    master_label = ones(length(tmp_kan_r),1);
    %Sensitive cells are zero
    master_label((tmp_kan_r==0)&(tmp_drug_conc==100)&(contains(tmp_drug_nm,'kan'))) = 0;
    %Normalize data
    master_data = normalize(master_data);
    
    master_data = master_data(:,sum(isnan(master_data))~=length(master_data));
    master_label = master_label(sum(isnan(master_data'))==0);
    master_data = master_data(sum(isnan(master_data'))==0,:);
    %Uncomment to examine how class labels separate
    [~,score,~,~,explained,~] = pca(master_data);

    %PCA downsampling...
    %Find the index that retains 95% of the variance
    expl = 0;pca_dim = 0;
    for i=1:length(explained)
        expl = expl+explained(i);
        if expl>95
            pca_dim = i;
            break
        end
    end
    master_data = score(:,1:pca_dim);

    for j = 1:kFoldCnt
        %Keep an equal amount of sensitive and resistant cells for training
        mn = min([sum(master_label==1),sum(master_label==0)]);
        idx_sens = find(master_label==0);idx_res = find(master_label==1);
        idx_sens = idx_sens(randperm(length(idx_sens)));
        idx_res = idx_res(randperm(length(idx_res)));
        idx_sens = idx_sens(1:mn);idx_res=idx_res(1:mn);
        idx = union(idx_res,idx_sens);
        tmp_master_data = master_data(idx,:);
        tmp_master_label = master_label(idx);

        %Use 80% of the data for training the model
        spt = rand(length(tmp_master_label),1)> 0.8;
        test = tmp_master_data(spt,:);
        train = tmp_master_data(spt==0,:);
        test_lab = tmp_master_label(spt,:);
        train_lab = tmp_master_label(spt==0,:);
        %Set up template Tree
        %See https://www.mathworks.com/help/stats/templatetree.html for list of
        %hyperparameters
        %%%SET PARAMETERS HERE!!!!!!
        t = templateTree('Reproducible',true,'MinLeafSize',7,'MaxNumSplits',34);
        Mdl1 = fitcensemble(train,train_lab,'Method','AdaBoostM1','NumLearningCycles',442,'LearnRate',0.94414,'NPrint',0);

        [predict_labels,predict_score] = predict(Mdl1, test);
        [~,~,~,auc] = perfcurve(test_lab,predict_score(:,1),0);
        %c = confusionmat(test_lab,predict_labels);
        cp1 = classperf(test_lab,predict_labels);
        aucComp(dT,j) = auc;
        errorRate(dT,j) = cp1.ErrorRate;
        correctRate(dT,j) = cp1.CorrectRate;


        %%Randomize and measure control
        tmp_train = train(randperm(length(train)),:);
        t = templateTree('Reproducible',true,'MinLeafSize',7,'MaxNumSplits',34);
        Mdl2 = fitcensemble(tmp_train,train_lab,'Method','AdaBoostM1','NumLearningCycles',442,'LearnRate',0.94414,'NPrint',0);

        [predict_labels,predict_score] = predict(Mdl2, test);
        [~,~,~,auc] = perfcurve(test_lab,predict_score(:,1),0);
        %c = confusionmat(test_lab,predict_labels);
        cp2 = classperf(test_lab,predict_labels);
        aucCompCon(dT,j) = auc;
        errorRateCon(dT,j) = cp2.ErrorRate;
        correctRateCon(dT,j) = cp2.CorrectRate;
    end
end

save('models/deltaT_vs_kanModel.mat','deltaT','aucComp','aucCompCon','errorRate','errorRateCon','correctRate','correctRateCon')

%% Make plot
fig = figure('Color','w','Position',[900 1200 200 120]);
ax1 = subplot(1,1,1);
shadedErrorBar(deltaT,errorRate'*100,{@mean,@std},'lineProps',{'r','markerfacecolor','r','linewidth',2})
shadedErrorBar(deltaT,errorRateCon'*100,{@mean,@std},'lineProps',{'b','markerfacecolor','b','linewidth',2})
xlabel('\Deltat')
ylabel('Error Rate')
ylim([0,60])
p = xline(min(deltaT(mean(errorRate')<.05)),'c','linewidth',2);
set(get(get(p,'Annotation'),'LegendInformation'),'IconDisplayStyle','off')
legend({'Training Data','Scramble Control'},'Location','East')
tightfig
set(gca,'Linewidth',2,'FontSize',8,'FontName','Arial')
saveas(fig,'plots/deltaT_kan_ml_gcamp_error.pdf')

fig = figure('Color','w','Position',[900 1200 200 120]);
ax1 = subplot(1,1,1);
shadedErrorBar(deltaT,aucComp',{@mean,@std},'lineProps',{'r','markerfacecolor','r','linewidth',2})
shadedErrorBar(deltaT,aucCompCon',{@mean,@std},'lineProps',{'b','markerfacecolor','b','linewidth',2})
xlabel('\Deltat')
ylabel('ROC AUC')
ylim([.4,1])
p = xline(min(deltaT(mean(errorRate')<.05)),'c','linewidth',2);
set(get(get(p,'Annotation'),'LegendInformation'),'IconDisplayStyle','off')
legend({'Training Data','Scramble Control'},'Location','East')
tightfig
set(gca,'Linewidth',2,'FontSize',8,'FontName','Arial')
saveas(fig,'plots/deltaT_kan_ml_gcamp_auc.pdf')


