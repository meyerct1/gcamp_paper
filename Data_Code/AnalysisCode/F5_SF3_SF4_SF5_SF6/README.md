compare_cell_area.m
SVM for cell area (F5, SF4)

compare_svm_gcampstd.m
SVM model for calcium transients (GCaMP6 std) (SF3)

deltaT_vs_error_kan.m
Random forest models as a function of deltaT for kan treated e. coli (F5, SF3, SF4, SF5)

gcamp_bacillus_kan_model.m
Random forest model building for B. subtilis GCaMP6 signal. (SF6)

bacillus_gcamp_feature_analysis.m
Feature analysis of bacillus model (SF6)

mscarlet_kan_model.m
Random forest models for discriminating based on mscarlet (F5, SF4)

single_cell_random_forest_optimization_kanamycin.m
Building the full random forest model for kanamycin treated gcamp cells (F5, SF3, SF4, SF5)



