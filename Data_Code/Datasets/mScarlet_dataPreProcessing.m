cd /home/meyerct6/Repos/GCAMP_paper/Data_Code/Datasets
%Which expierment to use:
activeDir      = '/mnt/kralj_celery/E coli GC antibioitic/*/2021-03-02_Results/matFiles/';  %Modify for local data mount point!
flist          = dir([activeDir '*' filesep '*data.mat']); %Current file list
nfiles         = length(flist); %Number of files

%Print the directories being used:
tmp = cellfun(@(x) strsplit(x,'/'),{flist.folder},'UniformOutput',false);
tmp = vertcat(tmp{:});
tmp = tmp(:,5);
disp('Using experiments:')
unique(tmp)

%% Step 1: Read in the feature matrices
cellCnt = 0;
for i = 1:length(flist)
    load([flist(i).folder filesep flist(i).name])
    cellCnt = cellCnt + S.ncells;
end
nframes        = size(S.cellMeanIntR,2);

%Pre allocate features matricies
drug_conc = zeros(cellCnt,1);kan_r=zeros(cellCnt,1);drugNm = cell(cellCnt,1);expNm = cell(cellCnt,1);wellNm = cell(cellCnt,1);  %Information on drug treatment and resistance status
cellArea  = zeros(cellCnt,nframes);    %Change in cell area over time
%Feature matrices on the trajectories for both Red and Green ch
intMatG   = zeros(cellCnt,nframes); minIntMatG = zeros(cellCnt,nframes); maxIntMatG = zeros(cellCnt,nframes); stdIntMatG = zeros(cellCnt,nframes);
intMatR   = zeros(cellCnt,nframes); minIntMatR = zeros(cellCnt,nframes); maxIntMatR = zeros(cellCnt,nframes); stdIntMatR = zeros(cellCnt,nframes);
matbckG = zeros(length(S.time),length(flist));matbckR = zeros(length(S.time),length(flist));
%Feature matrix on the segmentation used to filter cells
segArea   = zeros(cellCnt,1);segPerimeter=zeros(cellCnt,1);segMajAxisLen=zeros(cellCnt,1);segMinAxisLen=zeros(cellCnt,1);segCircularity=zeros(cellCnt,1);
%For each mat file, load and append the data to the appropriate matrix
cellCnt = [1 1];
for i = 1:length(flist)
    disp(['Loading file ' num2str(i)])
    load([flist(i).folder filesep flist(i).name])
    if ~isempty(S.cellMeanIntR)
        cellCnt(2) = cellCnt(1)+S.ncells-1;
        matbckR(:,i)                          = S.bckR;        
        intMatR(cellCnt(1):cellCnt(2),:)      = S.cellMeanIntR-S.bckR';  %Subtract the background!
        minIntMatR(cellCnt(1):cellCnt(2),:)   = S.cellMinIntR;
        maxIntMatR(cellCnt(1):cellCnt(2),:)   = S.cellMaxIntR;
        stdIntMatR(cellCnt(1):cellCnt(2),:)   = S.cellStdIntR;
        cellArea(cellCnt(1):cellCnt(2),:)     = S.cellArea;
        drug_conc(cellCnt(1):cellCnt(2))      = ones(S.ncells,1)*S.currConc;
        kan_r(cellCnt(1):cellCnt(2))          = ones(S.ncells,1)*contains(S.currCell,'KanR');
        tmp = split(flist(i).folder,filesep);
        tmp1 = tmp{end};
        tmp2 = tmp{end-3};
        for j = cellCnt(1):cellCnt(2)
            drugNm{j}                         = S.currDrug;
            wellNm{j}                         = tmp1;
            expNm{j}                          = tmp2;
        end
        segArea(cellCnt(1):cellCnt(2))        = S.segArea;
        segMajAxisLen(cellCnt(1):cellCnt(2))  = S.segMajAxisLen;
        segMinAxisLen(cellCnt(1):cellCnt(2))  = S.segMinAxisLen;
        segCircularity(cellCnt(1):cellCnt(2)) = S.segCircularity;
        segPerimeter(cellCnt(1):cellCnt(2))   = S.segPerimeter;
        cellCnt(1)                            = cellCnt(2)+1;
    end
end

%Which indices to keep
to_keep        = 1:cellCnt(2);
%% Step 3: Subset matrices and save...
intMatG        = intMatG(to_keep,:);
intMatR        = intMatR(to_keep,:);
minIntMatG     = minIntMatG(to_keep,:);
minIntMatR     = minIntMatR(to_keep,:);
maxIntMatG     = maxIntMatG(to_keep,:);
maxIntMatR     = maxIntMatR(to_keep,:);
stdIntMatG     = stdIntMatG(to_keep,:);
stdIntMatR     = stdIntMatR(to_keep,:);
cellArea       = cellArea(to_keep,:);
drug_conc      = drug_conc(to_keep);
kan_r          = kan_r(to_keep);
drugNm         = drugNm(to_keep);
wellNm         = wellNm(to_keep);
expNm          = expNm(to_keep);
segArea        = segArea(to_keep);
segMajAxisLen  = segMajAxisLen(to_keep);
segMinAxisLen  = segMinAxisLen(to_keep);
segCircularity = segCircularity(to_keep);
segPerimeter   = segPerimeter(to_keep);




endTime = 241;

%Deal with the strange error on circularity being infinite
segCircularity(abs(segCircularity)==Inf)=0;

%% Step 2:  "Gate" the cells based on different requirements
%Remove cells which vanished from segmentation (matArea==0)
%Fillmissing values if possible using movmedian over 15 frames
intMatR(:,1:endTime) = fillmissing(intMatR(:,1:endTime)','movmean',15)';
%If still undefined, remove 
indx    = find(sum(isnan(intMatG'))>0);
indx    = union(indx,find(sum(isnan(intMatR'))>0));

%Remove cells based on multidimentional gating of outliers to identify
%bad segmentation
dat = [segArea,segMajAxisLen,segMinAxisLen,segCircularity,segPerimeter];
dat = normalize(dat);
[~,score,~,~,explained,~] = pca(dat); %Run PCA to visualize what cells are being discarded
dist_mat = mean(squareform(pdist(score))); %Calculate the mean rmsd of every cell
data_keep = .98;  %What percentage to keep?  #############################
tmp = sort(dist_mat);
indx  = union(indx,find(dist_mat>tmp(floor(length(dist_mat)*data_keep))));
%Visualize cells
fig = figure('color','w');
hold on
scatter(score(:,1),score(:,2)); 
scatter(score(indx,1),score(indx,2));
legend({'keep','remove'})
xlabel('PCA1');ylabel('PCA2');
saveas(fig,'MultiDim_gating')

%Which indices to keep
to_keep    = setdiff(1:length(dist_mat),indx);
%% Step 3: Subset matrices and save...
intMatG    = intMatG(to_keep,:);
intMatR    = intMatR(to_keep,:);
minIntMatG = minIntMatG(to_keep,:);
minIntMatR = minIntMatR(to_keep,:);
maxIntMatG = maxIntMatG(to_keep,:);
maxIntMatR = maxIntMatR(to_keep,:);
stdIntMatG = stdIntMatG(to_keep,:);
stdIntMatR = stdIntMatR(to_keep,:);
cellArea   = cellArea(to_keep,:);
drug_conc  = drug_conc(to_keep);
kan_r      = kan_r(to_keep);
drugNm     = drugNm(to_keep);
wellNm     = wellNm(to_keep);
expNm      = expNm(to_keep);


save('03022021-mScarlet-InfoData.mat','intMatG','intMatR',...
                                       'minIntMatG','minIntMatR',...
                                       'maxIntMatG','maxIntMatR',...
                                       'stdIntMatG','stdIntMatR',...
                                       'cellArea','drug_conc','kan_r',...
                                       'drugNm','wellNm','expNm')
                                   
                                   

                                   
                                  
