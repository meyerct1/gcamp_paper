%Change into the correct directory
cd ~/Repos/GCAMP_paper/Data_Code/Figure3/


%% Step 1: Read in data generated from the "extract_GMmS_TIF_2color_parallel.m" file
%Clear everything and start a timer
clear; close all; tic
%Set active directory
activeDir      = '/mnt/kralj_celery/E coli GC antibioitic/';
flist          = dir([activeDir '*/*Results/matFiles/*/*data.mat']); %Current file list
nfiles         = length(flist); %Number of files

%Read in the feature matrices
cellCnt = 0;
for i = 1:length(flist)
    load([flist(i).folder filesep flist(i).name])
    if ~isempty(S.cellMeanIntG)
        cellCnt = cellCnt + S.ncells;
    end
end
nframes        = size(S.cellMeanIntG,2);

drug_conc = zeros(cellCnt,1);kan_r=zeros(cellCnt,1);drugNm = cell(cellCnt,1);  %Information on drug treatment and resistance status
cellArea  = zeros(cellCnt,nframes);    %Change in cell area over time
%Feature matrices on the trajectories for both Red and Green ch
intMatG   = zeros(cellCnt,nframes); minIntMatG = zeros(cellCnt,nframes); maxIntMatG = zeros(cellCnt,nframes); stdIntMatG = zeros(cellCnt,nframes);
intMatR   = zeros(cellCnt,nframes); minIntMatR = zeros(cellCnt,nframes); maxIntMatR = zeros(cellCnt,nframes); stdIntMatR = zeros(cellCnt,nframes);
%Feature matrix on the segmentation used to filter cells
segArea   = zeros(cellCnt,1);segPerimeter=zeros(cellCnt,1);segMajAxisLen=zeros(cellCnt,1);segMinAxisLen=zeros(cellCnt,1);segCircularity=zeros(cellCnt,1);
%For each mat file, load and append the data to the appropriate matrix
cellCnt = [1 1];
for i = 1:length(flist)
    disp(['Loading file ' num2str(i)])
    load([flist(i).folder filesep flist(i).name])
    if ~isempty(S.cellMeanIntG)
        cellCnt(2) = cellCnt(1)+S.ncells-1;
        intMatG(cellCnt(1):cellCnt(2),:)      = S.cellMeanIntG;
        intMatR(cellCnt(1):cellCnt(2),:)      = S.cellMeanIntR;
        minIntMatG(cellCnt(1):cellCnt(2),:)   = S.cellMinIntG;
        minIntMatR(cellCnt(1):cellCnt(2),:)   = S.cellMinIntR;
        maxIntMatG(cellCnt(1):cellCnt(2),:)   = S.cellMaxIntG;
        maxIntMatR(cellCnt(1):cellCnt(2),:)   = S.cellMaxIntR;
        stdIntMatG(cellCnt(1):cellCnt(2),:)   = S.cellStdIntG;
        stdIntMatR(cellCnt(1):cellCnt(2),:)   = S.cellStdIntR;
        cellArea(cellCnt(1):cellCnt(2),:)     = S.cellArea;
        drug_conc(cellCnt(1):cellCnt(2))      = ones(S.ncells,1)*S.currConc;
        kan_r(cellCnt(1):cellCnt(2))          = ones(S.ncells,1)*contains(S.currCell,'KanR');
        for j = cellCnt(1):cellCnt(2)
            drugNm{j}                         = S.currDrug;
        end
        segArea(cellCnt(1):cellCnt(2))        = S.segArea;
        segMajAxisLen(cellCnt(1):cellCnt(2))  = S.segMajAxisLen;
        segMinAxisLen(cellCnt(1):cellCnt(2))  = S.segMinAxisLen;
        segCircularity(cellCnt(1):cellCnt(2)) = S.segCircularity;
        segPerimeter(cellCnt(1):cellCnt(2))   = S.segPerimeter;
        cellCnt(1)                            = cellCnt(2)+1;
    end
end
%Deal with the strange error on circularity being infinite
segCircularity(abs(segCircularity)==Inf)=0;


%% Step 2:  "Gate" the cells based on different requirements
%Remove cells which vanished from segmentation (matArea==0)
indx    = find(sum(cellArea'==0)>0);
%Fillmissing values if possible using movmedian over 15 frames
intMatG = fillmissing(intMatG','movmean',15)';
intMatR = fillmissing(intMatR','movmean',15)';
%If still undefined, remove 
indx    = union(indx,find(sum(isnan(intMatG'))>0));
indx    = union(indx,find(sum(isnan(intMatR'))>0));
%Remove cells which have segmentation parameter too different
dat = [segArea,segMajAxisLen,segMinAxisLen,segCircularity,segPerimeter];
dat = normalize(dat);
[~,score,~,~,explained,~] = pca(dat); %Run PCA to visualize what cells are being discarded
dist_mat = mean(squareform(pdist(score)));
data_keep = .95;  %What percentage to keep?  #############################
tmp = sort(dist_mat);
indx  = union(indx,find(dist_mat>tmp(floor(length(dist_mat)*data_keep))));
%Visualize cells
fig = figure();
hold on
scatter(score(:,1),score(:,2)); scatter(score(indx,1),score(indx,2))
legend('keep','remove')
xlabel('PCA1');ylabel('PCA2');
saveas(fig,'MultiDim_gating')

%Which indices to keep
to_keep    = setdiff(1:length(dist_mat),indx);
intMatG    = intMatG(to_keep,:);
intMatR    = intMatR(to_keep,:);
minIntMatG = minIntMatG(to_keep,:);
minIntMatR = minIntMatR(to_keep,:);
maxIntMatG = maxIntMatG(to_keep,:);
maxIntMatR = maxIntMatR(to_keep,:);
stdIntMatG = stdIntMatG(to_keep,:);
stdIntMatR = stdIntMatR(to_keep,:);
cellArea   = cellArea(to_keep,:);
drug_conc  = drug_conc(to_keep);
kan_r      = kan_r(to_keep);
drugNm     = drugNm(to_keep);


%Loop through the different drugs
drugs = {'cipro','chlor','trim','PmB','gent','kan'};
maxConc = [10,100,10,3,100,100];

for vv=1:6
    %% For different deltaT estimate the predictivity of the RandomForest learning algorithms
    %Calculate feature matrices based on 1:deltaT frames
    deltaT = 10:2:240;
    kFoldCnt = 10;
    aucComp = zeros(length(deltaT),kFoldCnt);
    errorRate = zeros(length(deltaT),kFoldCnt);
    correctRate = zeros(length(deltaT),kFoldCnt);
    aucCompCon = zeros(length(deltaT),kFoldCnt);
    errorRateCon = zeros(length(deltaT),kFoldCnt);
    correctRateCon = zeros(length(deltaT),kFoldCnt);
    

    numWorkers = 24;
    p = gcp('nocreate'); % If no pool, do not create new one. ~14Gb/exp
    if isempty(p)
        parpool(numWorkers);
    end
    
%     % << for an existing Confision Matrix 'ConfusionMat' >>
%     precision = @(confusionMat) diag(confusionMat)./sum(confusionMat,2);
%     % And another for recall
%     recall = @(confusionMat) diag(confusionMat)./sum(confusionMat,1)';

    parfor dT = 1:length(deltaT)
        %Add to structure
        S = struct();
        S.meanIntG = intMatG(:,1:deltaT(dT));
        S.maxIntG = maxIntMatG(:,1:deltaT(dT));
        S.minIntG = minIntMatG(:,1:deltaT(dT));
        S.stdIntG = minIntMatG(:,1:deltaT(dT));
        nms = fieldnames(S);
        master_data  = []; mastLab=[];
        for j = 1:length(nms)
            [mat1,labs1] = calcTrajFeatures(S.(nms{j}));
            for l=1:length(labs1)
                labs1{l} = [labs1{l} '_' nms{j}];
            end
            master_data = [master_data,mat1];
            mastLab = [mastLab,labs1];
            for i=[2,5]
                [mat1,labs1] = calcTrajFeatures(movmean(S.(nms{j}),ceil(deltaT(dT)/i),2));
                for l=1:length(labs1)
                    labs1{l} = [labs1{l} '_' nms{j} '_movmean_deltaT_window_' num2str(ceil(deltaT(dT)/i))];
                end
                master_data = [master_data,mat1];
                mastLab = [mastLab,labs1];
            end
            for i=[2,5]
                [mat1,labs1] = calcTrajFeatures(movstd(S.(nms{j}),ceil(deltaT(dT)/i),[],2));
                for l=1:length(labs1)
                    labs1{l} = [labs1{l} '_' nms{j} '_movstd_deltaT_window_' num2str(ceil(deltaT(dT)/i))];
                end
                master_data = [master_data,mat1];
                mastLab = [mastLab,labs1];
            end

            %Now normalize matrix to T==0
            tmp = S.(nms{j});tmp = tmp./tmp(:,1);
            [mat1,labs1] = calcTrajFeatures(tmp);
            for l=1:length(labs1)
                labs1{l} = [labs1{l} '_' nms{j} '_norm'];
            end
            master_data = [master_data,mat1];
            mastLab = [mastLab,labs1];
            for i=[2,5]
                [mat1,labs1] = calcTrajFeatures(movmean(tmp,ceil(deltaT(dT)/i),2));
                for l=1:length(labs1)
                    labs1{l} = [labs1{l} '_' nms{j} '_norm_movmean_deltaT_window_' num2str(ceil(deltaT(dT)/i))];
                end
                master_data = [master_data,mat1];
                mastLab = [mastLab,labs1];
            end
            for i=[2,5]
                [mat1,labs1] = calcTrajFeatures(movstd(tmp,ceil(deltaT(dT)/i),[],2));
                for l=1:length(labs1)
                    labs1{l} = [labs1{l} '_' nms{j} '_norm_movstd_deltaT_window_' num2str(ceil(deltaT(dT)/i))];
                end
                master_data = [master_data,mat1];
                mastLab = [mastLab,labs1];
            end
        end

        
        indx = find((kan_r==1) | ((kan_r==0)&(drug_conc==maxConc(vv))&contains(drugNm,drugs{vv})) | ((kan_r==0)&(drug_conc==0)));
        %T = array2table(master_data,'VariableNames',mastLab);
        %Label cells as either Resistant or Sensitive.  Include untreated
        %sensitive as resistant cells!
        master_data = master_data(indx,:);
        tmp_kan_r = kan_r(indx);tmp_drug_conc=drug_conc(indx);tmp_drug_nm = drugNm(indx);
        master_label = ones(length(tmp_kan_r),1);
        %Sensitive cells are zero
        master_label(((tmp_kan_r==0)&(tmp_drug_conc==maxConc(vv))&contains(tmp_drug_nm,drugs{vv}))) = 0;
        
        nan_indx = sum(isnan(master_data'))==0;
        master_data = master_data(nan_indx,:);
        master_label = master_label(nan_indx,:);
        %Normalize data
        master_data = normalize(master_data);
        master_label = master_label(sum(isnan(master_data))==0);
        master_data = master_data(:,sum(isnan(master_data))==0);

        
        %Uncomment to examine how class labels separate
        [~,score,~,~,explained,~] = pca(master_data);
        master_data = score(:,explained>1);
    %     figure()
    %     hold on
    %     scatter(score(master_label==0,1),score(master_label==0,2))
    %     scatter(score(master_label==1,1),score(master_label==1,2))
    %     
    
        %% Begin training model
        %Split the data into training and testing
        disp(['Starting Model Learning, deltaT: ' num2str(deltaT(dT))])
        rng(5)
        for j=1:kFoldCnt
            %Keep an equal amount of sensitive and resistant cells for training
            mn = min([sum(master_label==1),sum(master_label==0)]);
            idx_sens = find(master_label==0);idx_res = find(master_label==1);
            idx_sens = idx_sens(randperm(length(idx_sens)));
            idx_res = idx_res(randperm(length(idx_res)));
            idx_sens = idx_sens(1:mn);idx_res=idx_res(1:mn);
            idx = union(idx_res,idx_sens);
            tmp_master_data = master_data(idx,:);
            tmp_master_label = master_label(idx);

            %Use 80% of the data for training the model
            spt = rand(length(tmp_master_label),1)> 0.8;
            test = tmp_master_data(spt,:);
            train = tmp_master_data(spt==0,:);
            test_lab = tmp_master_label(spt,:);
            train_lab = tmp_master_label(spt==0,:);
            %Set up template Tree
            %See https://www.mathworks.com/help/stats/templatetree.html for list of
            %hyperparameters
            t = templateTree('Reproducible',true,'MinLeafSize',16,'MaxNumSplits',47);
            Mdl1 = fitcensemble(train,train_lab,'Method','AdaBoostM1','NumLearningCycles',80,'LearnRate',0.24094,'NPrint',0);

            [predict_labels,predict_score] = predict(Mdl1, test);
            [~,~,~,auc] = perfcurve(test_lab,predict_score(:,1),0);
            %c = confusionmat(test_lab,predict_labels);
            cp1 = classperf(test_lab,predict_labels);
            aucComp(dT,j) = auc;
            errorRate(dT,j) = cp1.ErrorRate;
            correctRate(dT,j) = cp1.CorrectRate;
            
            
            %%Randomize and measure control
            %Use 80% of the data for training the model
            tmp_train = train(randperm(length(train)),:);
            t = templateTree('Reproducible',true,'MinLeafSize',16,'MaxNumSplits',47);
            Mdl2 = fitcensemble(tmp_train,train_lab,'Method','AdaBoostM1','NumLearningCycles',80,'LearnRate',0.24094,'NPrint',0);

            [predict_labels,predict_score] = predict(Mdl2, test);
            [~,~,~,auc] = perfcurve(test_lab,predict_score(:,1),0);
            %c = confusionmat(test_lab,predict_labels);
            cp2 = classperf(test_lab,predict_labels);
            aucCompCon(dT,j) = auc;
            errorRateCon(dT,j) = cp2.ErrorRate;
            correctRateCon(dT,j) = cp2.CorrectRate;
            
            
            
        end
    end

    indx = find((kan_r==1) | ((kan_r==0)&(drug_conc==maxConc(vv))&contains(drugNm,drugs{vv})) | ((kan_r==0)&(drug_conc==0)));
    tmp_kan_r = kan_r(indx);tmp_drug_conc=drug_conc(indx);tmp_drug_nm = drugNm(indx);
    master_label = ones(length(tmp_kan_r),1);
    %Sensitive cells are zero
    master_label(((tmp_kan_r==0)&(tmp_drug_conc==maxConc(vv))&contains(tmp_drug_nm,drugs{vv}))) = 0;
    mn = min([sum(master_label==1),sum(master_label==0)]);
            
    S              = struct();
    S.aucComp      = aucComp;
    S.errorRate    = errorRate;
    S.correctRate  = correctRate;
    S.aucCompCon      = aucCompCon;
    S.errorRateCon    = errorRateCon;
    S.correctRateCon  = correctRateCon;
    S.deltaT       = deltaT;
    S.totCnt       = mn;
    S.drug         = drugs{vv};
    
    save([drugs{vv} '_model.mat'],'S')
end





flist = dir('*_model.mat');
for f=1:length(flist)
    load(flist(f).name)
    deltaT         = S.deltaT;
    aucComp        = S.aucComp;
    errorRate      = S.errorRate;
    correctRate    = S.correctRate;
    drug          = S.drug;
    aucCompCon     = S.aucCompCon;
    errorRateCon   = S.errorRateCon;
    correctRateCon = S.correctRateCon;
    
    disp(['Used ' num2str(2*S.totCnt) ' cells to train ' drug ' model'])

    fig = figure('Color','w','Position',[900 1200 1000 300]);
    ax1 = subplot(1,3,1);
    shadedErrorBar(deltaT,aucComp',{@mean,@std},'lineProps',{'r','markerfacecolor','r','linewidth',2})
    shadedErrorBar(deltaT,aucCompCon',{@mean,@std},'lineProps',{'b','markerfacecolor','b','linewidth',2})
    title([drug ' ROC AUC'])
    xlabel('deltaT')
    ylabel('AUC')
    ylim([.2,1])
    set(gca,'Linewidth',2,'FontSize',8,'FontName','Arial')

    ax2 = subplot(1,3,2);
    shadedErrorBar(deltaT,errorRate'*100.,{@mean,@std},'lineProps',{'r','markerfacecolor','r','linewidth',2})
    shadedErrorBar(deltaT,errorRateCon'*100.,{@mean,@std},'lineProps',{'b','markerfacecolor','b','linewidth',2})
    title('Error Rate')
    ylabel('Rate (%)')
    xlabel('deltaT')
    ylim([0,50])
    set(gca,'Linewidth',2,'FontSize',8,'FontName','Arial')

    ax3 = subplot(1,3,3);
    shadedErrorBar(deltaT,correctRate'*100.,{@mean,@std},'lineProps',{'r','markerfacecolor','r','linewidth',2})
    shadedErrorBar(deltaT,correctRateCon'*100.,{@mean,@std},'lineProps',{'b','markerfacecolor','b','linewidth',2})
    title('Correct Rate')
    ylabel('Rate (%)')
    xlabel('deltaT')
    ylim([50,100])
    set(gca,'Linewidth',2,'FontSize',8,'FontName','Arial')
    tightfig
    saveas(fig,[drug '_deltaT_ml_quality.pdf'])
end











flist = dir('*_model.mat');
for f=1:length(flist)
    load(flist(f).name)
    deltaT         = S.deltaT;
    aucComp        = S.aucComp;
    errorRate      = S.errorRate;
    correctRate    = S.correctRate;
    drug          = S.drug;
    aucCompCon     = S.aucCompCon;
    errorRateCon   = S.errorRateCon;
    correctRateCon = S.correctRateCon;
    
    disp(['Used ' num2str(2*S.totCnt) ' cells to train ' drug ' model'])

    fig = figure('Color','w','Position',[900 1200 700 200]);
    ax2 = subplot(1,3,2);
    shadedErrorBar(deltaT,errorRate'*100.,{@mean,@std},'lineProps',{'r','markerfacecolor','r','linewidth',2})
    shadedErrorBar(deltaT,errorRateCon'*100.,{@mean,@std},'lineProps',{'b','markerfacecolor','b','linewidth',2})
    title(['Error Rate: ' drug ])
    ylabel('Rate (%)')
    xlabel('deltaT')
    ylim([0,60])
    set(gca,'Linewidth',2,'FontSize',8,'FontName','Arial')
    tightfig
    saveas(fig,[drug '_deltaT_ml_quality_error.pdf'])
end




