%Change into the correct directory
cd ~/Repos/GCAMP_paper/Data_Code/Figure3/

%% Step 1: Read in data generated from the "extract_GMmS_TIF_2color_parallel.m" file
%Clear everything and start a timer
clear; close all; tic
%Set active directory
activeDir      = '/mnt/kralj_celery/E coli GC antibioitic/';
flist          = dir([activeDir '*/*Results/matFiles/*/*data.mat']); %Current file list
nfiles         = length(flist); %Number of files

%Read in all the files to get a number of cells to 
cellCnt = 0;
for i = 1:length(flist)
    load([flist(i).folder filesep flist(i).name])
    if ~isempty(S.cellMeanIntG)
        cellCnt = cellCnt + S.ncells;
    end
end
nframes        = size(S.cellMeanIntG,2);

drug_conc = zeros(cellCnt,1);kan_r=zeros(cellCnt,1);drugNm = cell(cellCnt,1);expNm = cell(cellCnt,1);wellNm = cell(cellCnt,1);  %Information on drug treatment and resistance status
cellArea  = zeros(cellCnt,nframes);    %Change in cell area over time
%Feature matrices on the trajectories for both Red and Green ch
intMatG   = zeros(cellCnt,nframes); minIntMatG = zeros(cellCnt,nframes); maxIntMatG = zeros(cellCnt,nframes); stdIntMatG = zeros(cellCnt,nframes);
intMatR   = zeros(cellCnt,nframes); minIntMatR = zeros(cellCnt,nframes); maxIntMatR = zeros(cellCnt,nframes); stdIntMatR = zeros(cellCnt,nframes);
%Feature matrix on the segmentation used to filter cells
segArea   = zeros(cellCnt,1);segPerimeter=zeros(cellCnt,1);segMajAxisLen=zeros(cellCnt,1);segMinAxisLen=zeros(cellCnt,1);segCircularity=zeros(cellCnt,1);
%For each mat file, load and append the data to the appropriate matrix
cellCnt = [1 1];
for i = 1:length(flist)
    disp(['Loading file ' num2str(i)])
    load([flist(i).folder filesep flist(i).name])
    if ~isempty(S.cellMeanIntG)
        cellCnt(2) = cellCnt(1)+S.ncells-1;
        intMatG(cellCnt(1):cellCnt(2),:)      = S.cellMeanIntG;
        intMatR(cellCnt(1):cellCnt(2),:)      = S.cellMeanIntR;
        minIntMatG(cellCnt(1):cellCnt(2),:)   = S.cellMinIntG;
        minIntMatR(cellCnt(1):cellCnt(2),:)   = S.cellMinIntR;
        maxIntMatG(cellCnt(1):cellCnt(2),:)   = S.cellMaxIntG;
        maxIntMatR(cellCnt(1):cellCnt(2),:)   = S.cellMaxIntR;
        stdIntMatG(cellCnt(1):cellCnt(2),:)   = S.cellStdIntG;
        stdIntMatR(cellCnt(1):cellCnt(2),:)   = S.cellStdIntR;
        cellArea(cellCnt(1):cellCnt(2),:)     = S.cellArea;
        drug_conc(cellCnt(1):cellCnt(2))      = ones(S.ncells,1)*S.currConc;
        kan_r(cellCnt(1):cellCnt(2))          = ones(S.ncells,1)*contains(S.currCell,'KanR');
        tmp = split(flist(i).folder,filesep);
        tmp1 = tmp{end};
        tmp2 = tmp{end-3};
        for j = cellCnt(1):cellCnt(2)
            drugNm{j}                         = S.currDrug;
            wellNm{j}                         = tmp1;
            expNm{j}                          = tmp2;
        end
        segArea(cellCnt(1):cellCnt(2))        = S.segArea;
        segMajAxisLen(cellCnt(1):cellCnt(2))  = S.segMajAxisLen;
        segMinAxisLen(cellCnt(1):cellCnt(2))  = S.segMinAxisLen;
        segCircularity(cellCnt(1):cellCnt(2)) = S.segCircularity;
        segPerimeter(cellCnt(1):cellCnt(2))   = S.segPerimeter;
        cellCnt(1)                            = cellCnt(2)+1;
    end
end
%Deal with the strange error on circularity being infinite
segCircularity(abs(segCircularity)==Inf)=0;


%% Step 2:  "Gate" the cells based on different requirements
%Remove cells which vanished from segmentation (matArea==0)
indx    = find(sum(cellArea'==0)>0);
%Fillmissing values if possible using movmedian over 15 frames
intMatG = fillmissing(intMatG','movmean',15)';
intMatR = fillmissing(intMatR','movmean',15)';
%If still undefined, remove 
indx    = union(indx,find(sum(isnan(intMatG'))>0));
indx    = union(indx,find(sum(isnan(intMatR'))>0));
%Remove cells which have segmentation parameter too different
dat = [segArea,segMajAxisLen,segMinAxisLen,segCircularity,segPerimeter];
dat = normalize(dat);
[~,score,~,~,~,~] = pca(dat); %Run PCA to visualize what cells are being discarded
dist_mat = mean(squareform(pdist(score)));
data_keep = .95;  %What percentage to keep?  #############################
tmp = sort(dist_mat);
indx  = union(indx,find(dist_mat>tmp(floor(length(dist_mat)*data_keep))));
%Visualize cells
fig = figure();
hold on
scatter(score(:,1),score(:,2)); scatter(score(indx,1),score(indx,2))
legend('keep','remove')
xlabel('PCA1');ylabel('PCA2');
saveas(fig,'MultiDim_gating')

%Which indices to keep
to_keep    = setdiff(1:length(dist_mat),indx);
intMatG    = intMatG(to_keep,:);
intMatR    = intMatR(to_keep,:);
minIntMatG = minIntMatG(to_keep,:);
minIntMatR = minIntMatR(to_keep,:);
maxIntMatG = maxIntMatG(to_keep,:);
maxIntMatR = maxIntMatR(to_keep,:);
stdIntMatG = stdIntMatG(to_keep,:);
stdIntMatR = stdIntMatR(to_keep,:);
cellArea   = cellArea(to_keep,:);
drug_conc  = drug_conc(to_keep);
kan_r      = kan_r(to_keep);
drugNm     = drugNm(to_keep);
wellNm     = wellNm(to_keep);
expNm      = expNm(to_keep);







%Loop through the different drugs
drugs = {'kan'};
maxConc = [100];

vv = 1
%% For different deltaT estimate the predictivity of the RandomForest learning algorithms
%Calculate feature matrices based on 1:deltaT frames
deltaT = 120;
kFoldCnt = 10;
aucComp = zeros(length(deltaT),kFoldCnt);
errorRate = zeros(length(deltaT),kFoldCnt);
correctRate = zeros(length(deltaT),kFoldCnt);
aucCompCon = zeros(length(deltaT),kFoldCnt);
errorRateCon = zeros(length(deltaT),kFoldCnt);
correctRateCon = zeros(length(deltaT),kFoldCnt);



%     % << for an existing Confision Matrix 'ConfusionMat' >>
%     precision = @(confusionMat) diag(confusionMat)./sum(confusionMat,2);
%     % And another for recall
%     recall = @(confusionMat) diag(confusionMat)./sum(confusionMat,1)';
dT = 1;
%Add to structure
S = struct();
S.meanIntG = intMatG(:,1:deltaT(dT));
S.maxIntG = maxIntMatG(:,1:deltaT(dT));
S.minIntG = minIntMatG(:,1:deltaT(dT));
S.stdIntG = minIntMatG(:,1:deltaT(dT));
nms = fieldnames(S);
master_data  = []; mastLab=[];
for j = 1:length(nms)
    [mat1,labs1] = calcTrajFeatures(S.(nms{j}));
    for l=1:length(labs1)
        labs1{l} = [labs1{l} '_' nms{j}];
    end
    master_data = [master_data,mat1];
    mastLab = [mastLab,labs1];
    for i=[2,5]
        [mat1,labs1] = calcTrajFeatures(movmean(S.(nms{j}),ceil(deltaT(dT)/i),2));
        for l=1:length(labs1)
            labs1{l} = [labs1{l} '_' nms{j} '_movmean_deltaT_window_' num2str(ceil(deltaT(dT)/i))];
        end
        master_data = [master_data,mat1];
        mastLab = [mastLab,labs1];
    end
    for i=[2,5]
        [mat1,labs1] = calcTrajFeatures(movstd(S.(nms{j}),ceil(deltaT(dT)/i),[],2));
        for l=1:length(labs1)
            labs1{l} = [labs1{l} '_' nms{j} '_movstd_deltaT_window_' num2str(ceil(deltaT(dT)/i))];
        end
        master_data = [master_data,mat1];
        mastLab = [mastLab,labs1];
    end

    %Now normalize matrix to T==0
    tmp = S.(nms{j});tmp = tmp./tmp(:,1);
    [mat1,labs1] = calcTrajFeatures(tmp);
    for l=1:length(labs1)
        labs1{l} = [labs1{l} '_' nms{j} '_norm'];
    end
    master_data = [master_data,mat1];
    mastLab = [mastLab,labs1];
    for i=[2,5]
        [mat1,labs1] = calcTrajFeatures(movmean(tmp,ceil(deltaT(dT)/i),2));
        for l=1:length(labs1)
            labs1{l} = [labs1{l} '_' nms{j} '_norm_movmean_deltaT_window_' num2str(ceil(deltaT(dT)/i))];
        end
        master_data = [master_data,mat1];
        mastLab = [mastLab,labs1];
    end
    for i=[2,5]
        [mat1,labs1] = calcTrajFeatures(movstd(tmp,ceil(deltaT(dT)/i),[],2));
        for l=1:length(labs1)
            labs1{l} = [labs1{l} '_' nms{j} '_norm_movstd_deltaT_window_' num2str(ceil(deltaT(dT)/i))];
        end
        master_data = [master_data,mat1];
        mastLab = [mastLab,labs1];
    end
end

nan_indx1 = find(sum(isnan(master_data'))==0);
master_data = master_data(nan_indx1,:);
%Normalize data
master_data = normalize(master_data);
nan_indx2 = find(sum(isnan(master_data))==0);
master_data = master_data(:,nan_indx2);
save_master_data = master_data;


indx = find(((kan_r==1)&contains(drugNm,drugs{vv})) |...
            ((kan_r==0)&(drug_conc==maxConc(vv))&contains(drugNm,drugs{vv})) |...
            ((kan_r==0)&(drug_conc==0)&contains(drugNm,drugs{vv})));
indx = intersect(indx,nan_indx1);
%T = array2table(master_data,'VariableNames',mastLab);
%Label cells as either Resistant or Sensitive.  Include untreated
%sensitive as resistant cells!
master_data = master_data(indx,:);
tmp_kan_r = kan_r(indx);tmp_drug_conc=drug_conc(indx);tmp_drug_nm = drugNm(indx);
master_label = ones(length(tmp_kan_r),1);
%Sensitive cells are zero
master_label((tmp_kan_r==0) & (tmp_drug_conc==maxConc(vv)) & contains(tmp_drug_nm,drugs{vv})) = 0;


%Uncomment to examine how class labels separate
[coeff,score,~,~,explained,mu] = pca(master_data);
dim_keep = 0;sum_explained=0;
while sum_explained < 95
    dim_keep = dim_keep + 1;
    sum_explained = sum_explained + explained(dim_keep);
end
disp(['Keeping: ' num2str(dim_keep) ' dimensions'])
master_data = score(:,1:dim_keep);


%% Begin training model
%Split the data into training and testing
disp(['Starting Model Learning, deltaT: ' num2str(deltaT(dT))])
rng(5)
Mdl = {};
for j=1:kFoldCnt
    %Keep an equal amount of sensitive and resistant cells for training
    mn               = min([sum(master_label==1),sum(master_label==0)]);
    idx_sens         = find(master_label==0);idx_res = find(master_label==1);
    idx_sens         = idx_sens(randperm(length(idx_sens)));
    idx_res          = idx_res(randperm(length(idx_res)));
    idx_sens         = idx_sens(1:mn);idx_res=idx_res(1:mn);
    idx              = union(idx_res,idx_sens);
    tmp_master_data  = master_data(idx,:);
    tmp_master_label = master_label(idx);

    %Use 80% of the data for training the model
    spt       = rand(length(tmp_master_label),1)> 0.8;
    test      = tmp_master_data(spt,:);
    train     = tmp_master_data(spt==0,:);
    test_lab  = tmp_master_label(spt,:);
    train_lab = tmp_master_label(spt==0,:);
    %Set up template Tree
    %See https://www.mathworks.com/help/stats/templatetree.html for list of
    %hyperparameters
    t      = templateTree('Reproducible',true,'MinLeafSize',16,'MaxNumSplits',47);
    Mdl1   = fitcensemble(train,train_lab,'Method','AdaBoostM1','NumLearningCycles',80,'LearnRate',0.24094,'NPrint',0);
    Mdl{j} = Mdl1;
    
    [predict_labels,predict_score] = predict(Mdl1, test);
    [~,~,~,auc]                    = perfcurve(test_lab,predict_score(:,1),0);
    %c = confusionmat(test_lab,predict_labels);
    cp1                            = classperf(test_lab,predict_labels);
    aucComp(dT,j)                  = auc;
    errorRate(dT,j)                = cp1.ErrorRate;
    correctRate(dT,j)              = cp1.CorrectRate;

    %%Randomize and measure control
    %Use 80% of the data for training the model
    tmp_train = train(randperm(length(train)),:);
    t         = templateTree('Reproducible',true,'MinLeafSize',16,'MaxNumSplits',47);
    Mdl2      = fitcensemble(tmp_train,train_lab,'Method','AdaBoostM1','NumLearningCycles',80,'LearnRate',0.24094,'NPrint',0);

    [predict_labels,predict_score] = predict(Mdl2, test);
    [~,~,~,auc]                    = perfcurve(test_lab,predict_score(:,1),0);
    %c = confusionmat(test_lab,predict_labels);
    cp2                            = classperf(test_lab,predict_labels);
    aucCompCon(dT,j)               = auc;
    errorRateCon(dT,j)             = cp2.ErrorRate;
    correctRateCon(dT,j)           = cp2.CorrectRate;
end

%%Apply to titration data
master_data = save_master_data;

indx1 = find((kan_r==1)&contains(drugNm,drugs{vv}));
indx1 = intersect(indx1,nan_indx1);

indx2 = find((kan_r==0)&contains(drugNm,drugs{vv}));
indx2 = intersect(indx2,nan_indx1);

%Label cells as either Resistant or Sensitive.  Include untreated
%sensitive as resistant cells!
master_data1 = master_data(indx1,:);
master_data2 = master_data(indx2,:);
tmp_drug_conc1=drug_conc(indx1);
tmp_drug_conc2=drug_conc(indx2);

%Reapply PCA
master_data1 = (master_data1-mu)*coeff(:,1:dim_keep);
master_data2 = (master_data2-mu)*coeff(:,1:dim_keep);

fig = figure('color','w','position',[0 10 400 180]);
undrug = unique(tmp_drug_conc1);
perct1 = zeros(kFoldCnt,length(undrug));
perct2 = zeros(kFoldCnt,length(undrug));

for j=1:kFoldCnt
    label1 = predict(Mdl{j},master_data1);
    label2 = predict(Mdl{j},master_data2);
    for i=1:length(undrug)
        tmp = label1(tmp_drug_conc1==undrug(i));
        perct1(j,i) = sum(tmp==0)/length(tmp);
        tmp = label2(tmp_drug_conc2==undrug(i));
        perct2(j,i) = sum(tmp==0)/length(tmp);   
    end
end
hold on
shadedErrorBar(1:length(undrug),perct1*100.,{@mean,@std},'lineProps',{'k','markerfacecolor','k','linewidth',2})
shadedErrorBar(1:length(undrug),perct2*100.,{@mean,@std},'lineProps',{'b','markerfacecolor','b','linewidth',2})
xlabel('[kan]')
ylabel('% Sensitive')
xticks(1:length(undrug))
xticklabels(undrug)
legend({'Resistant','Sensitive'},'Location','northwest')
title(['Kan Titration, DeltaT: ' num2str(deltaT)])



%%Apply to titration data
master_data = save_master_data;
indx2 = find((kan_r==0)&(drug_conc==0)&contains(drugNm,drugs{vv}));
indx2 = intersect(indx2,nan_indx1);
%T = array2table(master_data,'VariableNames',mastLab);
%Label cells as either Resistant or Sensitive.  Include untreated
%sensitive as resistant cells!
master_data2 = master_data(indx2,:);
tmp_kan_r = kan_r(indx2);tmp_drug_conc=drug_conc(indx2);tmp_drug_nm = drugNm(indx2);
master_label = ones(length(tmp_kan_r),1);
%Sensitive cells are zero
master_label((tmp_kan_r==0) & (tmp_drug_conc==maxConc(vv)) & contains(tmp_drug_nm,drugs{vv})) = 0;




%Label cells as either Resistant or Sensitive.  Include untreated
%sensitive as resistant cells!
master_data2 = master_data(indx2,:);
tmp_drug_conc2=drug_conc(indx2);
tmp_nm = cell(length(indx2),1);
for i = 1:length(indx2)
    tmp_nm{i} = [expNm{indx2(i)} '_' wellNm{indx2(i)}];
end

    
master_data2 = (master_data2-mu)*coeff(:,1:dim_keep);

uncond = unique(tmp_nm);
perct2 = zeros(kFoldCnt,length(uncond));

for j=1:kFoldCnt
    label2 = predict(Mdl{j},master_data2);
    for i=1:length(uncond)
        tmp = label2(contains(tmp_nm,uncond{i}));
        perct2(j,i) = sum(tmp==0)/length(tmp);   
    end
end

