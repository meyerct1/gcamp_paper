%Change into the correct directory
cd '~/Repos/GCAMP_paper/Data_Code/Figure 4/'

%% Step 1: Read in data generated from the "extract_GMmS_TIF_2color_parallel.m" file
%Clear everything and start a timer
clear; close all; tic
saveModel_name = '022321_baggedTree';      %Name of models
%Set active directory
activeDir      = '/mnt/kralj_celery/E coli GC antibioitic/*/2021-01-15_Results/matFiles/';
flist          = dir([activeDir '*' filesep '*data.mat']); %Current file list
nfiles         = length(flist); %Number of files

tmp = cellfun(@(x) strsplit(x,'/'),{flist.folder},'UniformOutput',false);
tmp = vertcat(tmp{:});
tmp = tmp(:,5);
disp('Using experiments:')
unique(tmp)


%Read in the feature matrices
cellCnt = 0;
for i = 1:length(flist)
    load([flist(i).folder filesep flist(i).name])
    cellCnt = cellCnt + S.ncells;
end
nframes        = size(S.cellMeanIntG,2);

%Pre allocate features matricies
drug_conc = zeros(cellCnt,1);kan_r=zeros(cellCnt,1);drugNm = cell(cellCnt,1);expNm = cell(cellCnt,1);wellNm = cell(cellCnt,1);  %Information on drug treatment and resistance status
cellArea  = zeros(cellCnt,nframes);    %Change in cell area over time
%Feature matrices on the trajectories for both Red and Green ch
intMatG   = zeros(cellCnt,nframes); minIntMatG = zeros(cellCnt,nframes); maxIntMatG = zeros(cellCnt,nframes); stdIntMatG = zeros(cellCnt,nframes);
intMatR   = zeros(cellCnt,nframes); minIntMatR = zeros(cellCnt,nframes); maxIntMatR = zeros(cellCnt,nframes); stdIntMatR = zeros(cellCnt,nframes);
matbckG = zeros(length(S.time),length(flist));matbckR = zeros(length(S.time),length(flist));
%Feature matrix on the segmentation used to filter cells
segArea   = zeros(cellCnt,1);segPerimeter=zeros(cellCnt,1);segMajAxisLen=zeros(cellCnt,1);segMinAxisLen=zeros(cellCnt,1);segCircularity=zeros(cellCnt,1);
%For each mat file, load and append the data to the appropriate matrix
cellCnt = [1 1];
for i = 1:length(flist)
    disp(['Loading file ' num2str(i)])
    load([flist(i).folder filesep flist(i).name])
    if ~isempty(S.cellMeanIntG)
        cellCnt(2) = cellCnt(1)+S.ncells-1;
        matbckG(:,i)                          = S.bckG;
        matbckR(:,i)                          = S.bckR;        
        intMatG(cellCnt(1):cellCnt(2),:)      = S.cellMeanIntG-S.bckG';  %Subtract the background!
        intMatR(cellCnt(1):cellCnt(2),:)      = S.cellMeanIntR-S.bckR';  %Subtract the background!
        minIntMatG(cellCnt(1):cellCnt(2),:)   = S.cellMinIntG;
        minIntMatR(cellCnt(1):cellCnt(2),:)   = S.cellMinIntR;
        maxIntMatG(cellCnt(1):cellCnt(2),:)   = S.cellMaxIntG;
        maxIntMatR(cellCnt(1):cellCnt(2),:)   = S.cellMaxIntR;
        stdIntMatG(cellCnt(1):cellCnt(2),:)   = S.cellStdIntG;
        stdIntMatR(cellCnt(1):cellCnt(2),:)   = S.cellStdIntR;
        cellArea(cellCnt(1):cellCnt(2),:)     = S.cellArea;
        drug_conc(cellCnt(1):cellCnt(2))      = ones(S.ncells,1)*S.currConc;
        kan_r(cellCnt(1):cellCnt(2))          = ones(S.ncells,1)*contains(S.currCell,'KanR');
        tmp = split(flist(i).folder,filesep);
        tmp1 = tmp{end};
        tmp2 = tmp{end-3};
        for j = cellCnt(1):cellCnt(2)
            drugNm{j}                         = S.currDrug;
            wellNm{j}                         = tmp1;
            expNm{j}                          = tmp2;
        end
        segArea(cellCnt(1):cellCnt(2))        = S.segArea;
        segMajAxisLen(cellCnt(1):cellCnt(2))  = S.segMajAxisLen;
        segMinAxisLen(cellCnt(1):cellCnt(2))  = S.segMinAxisLen;
        segCircularity(cellCnt(1):cellCnt(2)) = S.segCircularity;
        segPerimeter(cellCnt(1):cellCnt(2))   = S.segPerimeter;
        cellCnt(1)                            = cellCnt(2)+1;
    end
end
%Deal with the strange error on circularity being infinite
segCircularity(abs(segCircularity)==Inf)=0;


%% Step 2:  "Gate" the cells based on different requirements
%Remove cells which vanished from segmentation (matArea==0)
indx    = find(sum(cellArea'==0)>0);
%Fillmissing values if possible using movmedian over 15 frames
intMatG = fillmissing(intMatG','movmean',15)';
intMatR = fillmissing(intMatR','movmean',15)';
%If still undefined, remove 
indx    = union(indx,find(sum(isnan(intMatG'))>0));
indx    = union(indx,find(sum(isnan(intMatR'))>0));
%Remove cells which have segmentation parameter too different
dat = [segArea,segMajAxisLen,segMinAxisLen,segCircularity,segPerimeter];
dat = normalize(dat);
[~,score,~,~,explained,~] = pca(dat); %Run PCA to visualize what cells are being discarded
dist_mat = mean(squareform(pdist(score)));
data_keep = .98;  %What percentage to keep?  #############################
tmp = sort(dist_mat);
indx  = union(indx,find(dist_mat>tmp(floor(length(dist_mat)*data_keep))));
%Visualize cells
fig = figure('color','w');
hold on
scatter(score(:,1),score(:,2)); 
scatter(score(indx,1),score(indx,2));
legend({'keep','remove'})
xlabel('PCA1');ylabel('PCA2');
saveas(fig,'MultiDim_gating')

%Which indices to keep
to_keep    = setdiff(1:length(dist_mat),indx);
intMatG    = intMatG(to_keep,:);
intMatR    = intMatR(to_keep,:);
minIntMatG = minIntMatG(to_keep,:);
minIntMatR = minIntMatR(to_keep,:);
maxIntMatG = maxIntMatG(to_keep,:);
maxIntMatR = maxIntMatR(to_keep,:);
stdIntMatG = stdIntMatG(to_keep,:);
stdIntMatR = stdIntMatR(to_keep,:);
cellArea   = cellArea(to_keep,:);
drug_conc  = drug_conc(to_keep);
kan_r      = kan_r(to_keep);
drugNm     = drugNm(to_keep);
wellNm     = wellNm(to_keep);
expNm      = expNm(to_keep);


%% For the full time course (deltaT=241) find the best approach using the HyperOptimization in Matlab
deltaT = [241]; dT = 1;

%Subset out the kanamycin treated and untreated cells to train model
indx = find(((kan_r==1)&(contains(drugNm,'kan'))) | (((kan_r==0)&(drug_conc==100)&(contains(drugNm,'kan'))) | ((kan_r==0)&(drug_conc==0))));
S = struct();
S.meanIntG = intMatG(indx,1:deltaT(dT));
S.maxIntG = maxIntMatG(indx,1:deltaT(dT));
S.minIntG = minIntMatG(indx,1:deltaT(dT));
S.stdIntG = minIntMatG(indx,1:deltaT(dT));
nms = fieldnames(S);
master_data  = []; mastLab=[];
for j = 1:length(nms)
    [mat1,labs1] = calcTrajFeatures(S.(nms{j}));
    for l=1:length(labs1)
        labs1{l} = [labs1{l} '_' nms{j}];
    end
    master_data = [master_data,mat1];
    mastLab = [mastLab,labs1];
    for i=[5,10]
        [mat1,labs1] = calcTrajFeatures(movmean(S.(nms{j}),ceil(deltaT(dT)/i),2));
        for l=1:length(labs1)
            labs1{l} = [labs1{l} '_' nms{j} '_movmean_deltaT_window_' num2str(ceil(deltaT(dT)/i))];
        end
        master_data = [master_data,mat1];
        mastLab = [mastLab,labs1];
    end
    for i=[5,10]
        [mat1,labs1] = calcTrajFeatures(movstd(S.(nms{j}),ceil(deltaT(dT)/i),[],2));
        for l=1:length(labs1)
            labs1{l} = [labs1{l} '_' nms{j} '_movstd_deltaT_window_' num2str(ceil(deltaT(dT)/i))];
        end
        master_data = [master_data,mat1];
        mastLab = [mastLab,labs1];
    end

    %Now normalize matrix to T==0
    tmp = S.(nms{j});tmp = tmp./tmp(:,1);
    [mat1,labs1] = calcTrajFeatures(tmp);
    for l=1:length(labs1)
        labs1{l} = [labs1{l} '_' nms{j} '_norm'];
    end
    master_data = [master_data,mat1];
    mastLab = [mastLab,labs1];
    for i=[5,10]
        [mat1,labs1] = calcTrajFeatures(movmean(tmp,ceil(deltaT(dT)/i),2));
        for l=1:length(labs1)
            labs1{l} = [labs1{l} '_' nms{j} '_norm_movmean_deltaT_window_' num2str(ceil(deltaT(dT)/i))];
        end
        master_data = [master_data,mat1];
        mastLab = [mastLab,labs1];
    end
    for i=[5,10]
        [mat1,labs1] = calcTrajFeatures(movstd(tmp,ceil(deltaT(dT)/i),[],2));
        for l=1:length(labs1)
            labs1{l} = [labs1{l} '_' nms{j} '_norm_movstd_deltaT_window_' num2str(ceil(deltaT(dT)/i))];
        end
        master_data = [master_data,mat1];
        mastLab = [mastLab,labs1];
    end
end

%Use label cells Label cells as either Resistant or Sensitive.  Include untreated
%sensitive as resistant cells!
tmp_kan_r = kan_r(indx);tmp_drug_conc=drug_conc(indx);tmp_drug_nm = drugNm(indx);
master_label = ones(length(tmp_kan_r),1);
%Sensitive cells are zero
master_label((tmp_kan_r==0)&(tmp_drug_conc==100)&(contains(tmp_drug_nm,'kan'))) = 0;

%Normalize data
master_data = normalize(master_data);
%Remove nan cells and columns    
master_data = master_data(:,sum(isnan(master_data))~=length(master_data));
master_label = master_label(sum(isnan(master_data'))==0);
master_data = master_data(sum(isnan(master_data'))==0,:);

%Uncomment to examine how class labels separate
[coeff,score,~,~,explained,~] = pca(master_data);
figure()
hold on
scatter(score(master_label==0,1),score(master_label==0,2))
scatter(score(master_label==1,1),score(master_label==1,2))

%PCA downsampling...
%Find the index that retains 95% of the variance
expl = 0;
for i=1:length(explained)
    expl = expl+explained(i);
    if expl>95
        pca_dim = i;
        break
    end
end
master_data = score(:,1:pca_dim);

%% Hyperparameter sweep
%Split the data into training and testing
rng(5)
disp(['Starting Model Learning, deltaT: ' num2str(deltaT)])
%Use only 5% of the data for training the model
spt = rand(length(master_data),1)> 0.8;
test = master_data(spt,:);
train = master_data(spt==0,:);
test_lab = master_label(spt,:);
train_lab = master_label(spt==0,:);
%Set up template Tree
%See https://www.mathworks.com/help/stats/templatetree.html for list of
%hyperparameters
numWorkers = 24;
p = gcp('nocreate'); % If no pool, do not create new one. ~14Gb/exp
if isempty(p)
    parpool(numWorkers);
end
t = templateTree('Reproducible',true);

%Save model as a compact ensemble
Mdl0 = fitcensemble(train,train_lab,'OptimizeHyperparameters','all',...
                                    'Learners',t,...
                                    'HyperparameterOptimizationOptions',struct('AcquisitionFunctionName','expected-improvement-plus',...
                                                                                'MaxObjectiveEvaluations',200,...
                                                                                'UseParallel',true,...
                                                                                'Repartition',true));
                                                                            

saveas(gcf,'Mdl0_HyperParameterSearch-02232021.fig')  
save('Mdl0_HyperParameterSearch-02232021.mat','Mdl0','-v7.3')
%Find the hyperparameter set that is optimal for each drug, run a subsequent
%hyperoptimization for number of learning cycles, learn rate, maxnumsplits
%Use AdaBoostM1
Mdl1 = fitcensemble(train,train_lab,'Method','AdaBoostM1','OptimizeHyperparameters',{'NumLearningCycles','LearnRate','MaxNumSplits','MinLeafSize'},'Learners',t,...
                                                'HyperparameterOptimizationOptions',struct( 'MaxObjectiveEvaluations',200,...
                                                        'UseParallel',true,...
                                                        'Repartition',true));
saveas(gcf,'Mdl1_HyperParameterSearch-02232021.fig')  
save('Mdl1_HyperParameterSearch-02232021.mat','Mdl0','-v7.3')

kFoldCnt = 10;
%% Select best model!
rng(5);
%Best according to hyperparameter optimization
t = templateTree('Reproducible',true,'MinLeafSize',7,'MaxNumSplits',34);
Mdl2 = fitcensemble(train,train_lab,'Method','AdaBoostM1','NumLearningCycles',442,'LearnRate',0.94414,'CrossVal','on','KFold',kFoldCnt,'NPrint',0);

save('Mdl2_gCAMP_kan.mat','Mdl2','-v7.3')
modelLosses = kfoldLoss(Mdl2,'mode','individual');
[predict_labels,predict_score] = predict(Mdl2.Trained{find(modelLosses==min(modelLosses))}, test);

%% Plots for best model
auc = zeros(2,1);
fig = figure('Color','w','Position',[900 1200 215 130]);
ax = subplot(1,1,1);
hold on
for k = 0
    [xVal,yVal,~,auc(k+1)] = perfcurve(test_lab(1:end),predict_score(1:end,k+1),k);
    stp = round(length(xVal)*.01);
    plot(ax,xVal(1:stp:end),yVal(1:stp:end),'Linewidth',2);
    text(0.25,.15*(k+1),strcat('AUC=',num2str(auc(k+1))),'FontSize',8);
end
plot(xVal(1:stp:end),xVal(1:stp:end),'--r','linewidth',2)
xlabel('False Positive Rate');
ylabel('True Positive Rate');
set(gca,'Linewidth',2,'FontSize',8,'FontName','Arial')
saveas(fig,'fullTime_kan_gcamp_roc.pdf')


cp = classperf(test_lab,predict_labels);
Mdl = Mdl2;

%Plot the loss as a function of training rounds
kflc = kfoldLoss(Mdl,'Mode','cumulative');
fig = figure('Color','w','Position',[900 1200 215 130]);
plot(kflc*100,'linewidth',2);
ylabel({'10-fold','Error rate'});
xlabel('Learning cycle');
set(gca,'Linewidth',2,'FontSize',8,'FontName','Arial')
tightfig
saveas(fig,'fullTime_kan_gcamp_learningCycle.pdf')

fig = figure('Color','w','Position',[900 1200 215 130]);
plot(modelLosses,'linewidth',2);
xlabel('Fold')
ylabel('Model Loss')
set(gca,'Linewidth',2,'FontSize',8,'FontName','Arial')
saveas(fig,'fullTime_kan_gcamp_kfoldLoss.pdf')

%Plot confusion chart
fig = figure('Color','w','Position',[900 1200 215 130]);
[predict_labels,predict_score] = predict(Mdl.Trained{find(modelLosses==min(modelLosses))}, test);
c = confusionchart(confusionmat(test_lab,predict_labels),{'Sens','Res'},'Normalization','row-normalized');
set(gca,'FontSize',8,'FontName','Arial')
saveas(fig,'fullTime_kan_gcamp_confusionMat.pdf')

% PLot the feature importance
imp = predictorImportance(Mdl.Trained{find(modelLosses==min(modelLosses))});
fig = figure('Color','w','Position',[900 1200 215 130]);
b = bar(sort(imp),'edgealpha',0);
xticks([])
yticks([])
ylabel({'Relative Predictor' ,'Importance'});
xlabel('PCA component')
set(gca,'Linewidth',2,'FontSize',8,'FontName','Arial')
saveas(fig,'fullTime_kan_gcamp_featureImportance.pdf')


[m,i1] = max(imp);
[s,i2] = sort(coeff(:,i1));
%Top loadings
for i=0:9
    disp(mastLab{i2(end-i)})
end
%Top loadings
for i=1:10
    disp(mastLab{i2(i)})
end
fig = figure('Color','w','Position',[900 1200 215 130]);
b = bar(sort(coeff(:,i1)),'edgealpha',0);
xticks([])
ylabel('Eigenvalue');
xlabel('Feature')
title({['PCA ' num2str(i1)] ,['Variance Explained: ' num2str(explained(i1),2) '%']})
set(gca,'Linewidth',2,'FontSize',8,'FontName','Arial')
saveas(fig,'fullTime_kan_gcamp_mostImportantComponent.pdf')




%% For different deltaT estimate the predictivity of the RandomForest learning algorithms
%Calculate feature matrices based on 1:deltaT frames
deltaT = 10:2:240;
kFoldCnt = 10;
aucComp = zeros(length(deltaT),kFoldCnt);
errorRate = zeros(length(deltaT),kFoldCnt);
correctRate = zeros(length(deltaT),kFoldCnt);

aucCompCon = zeros(length(deltaT),kFoldCnt);
errorRateCon = zeros(length(deltaT),kFoldCnt);
correctRateCon = zeros(length(deltaT),kFoldCnt);
numWorkers = 24;
p = gcp('nocreate'); % If no pool, do not create new one. ~14Gb/exp
if isempty(p)
    parpool(numWorkers);
end
indx = find(((kan_r==1)&(contains(drugNm,'kan'))) | (((kan_r==0)&(drug_conc==100)&(contains(drugNm,'kan'))) | ((kan_r==0)&(drug_conc==0))));

% << for an existing Confision Matrix 'ConfusionMat' >>
precision = @(confusionMat) diag(confusionMat)./sum(confusionMat,2);
% And another for recall
recall = @(confusionMat) diag(confusionMat)./sum(confusionMat,1)';

parfor dT = 1:length(deltaT)
    rng(5); %Set random number generator for reproducibility
    disp(['Model: ' num2str(deltaT(dT))])
    S = struct();
    S.meanIntG = intMatG(indx,1:deltaT(dT));
    S.maxIntG = maxIntMatG(indx,1:deltaT(dT));
    S.minIntG = minIntMatG(indx,1:deltaT(dT));
    S.stdIntG = minIntMatG(indx,1:deltaT(dT));
    nms = fieldnames(S);
    master_data  = []; mastLab=[];
    for j = 1:length(nms)
        [mat1,labs1] = calcTrajFeatures(S.(nms{j}));
        for l=1:length(labs1)
            labs1{l} = [labs1{l} '_' nms{j}];
        end
        master_data = [master_data,mat1];
        mastLab = [mastLab,labs1];
        for i=[5,10]
            [mat1,labs1] = calcTrajFeatures(movmean(S.(nms{j}),ceil(deltaT(dT)/i),2));
            for l=1:length(labs1)
                labs1{l} = [labs1{l} '_' nms{j} '_movmean_deltaT_window_' num2str(ceil(deltaT(dT)/i))];
            end
            master_data = [master_data,mat1];
            mastLab = [mastLab,labs1];
        end
        for i=[5,10]
            [mat1,labs1] = calcTrajFeatures(movstd(S.(nms{j}),ceil(deltaT(dT)/i),[],2));
            for l=1:length(labs1)
                labs1{l} = [labs1{l} '_' nms{j} '_movstd_deltaT_window_' num2str(ceil(deltaT(dT)/i))];
            end
            master_data = [master_data,mat1];
            mastLab = [mastLab,labs1];
        end

        %Now normalize matrix to T==0
        tmp = S.(nms{j});tmp = tmp./tmp(:,1);
        [mat1,labs1] = calcTrajFeatures(tmp);
        for l=1:length(labs1)
            labs1{l} = [labs1{l} '_' nms{j} '_norm'];
        end
        master_data = [master_data,mat1];
        mastLab = [mastLab,labs1];
        for i=[5,10]
            [mat1,labs1] = calcTrajFeatures(movmean(tmp,ceil(deltaT(dT)/i),2));
            for l=1:length(labs1)
                labs1{l} = [labs1{l} '_' nms{j} '_norm_movmean_deltaT_window_' num2str(ceil(deltaT(dT)/i))];
            end
            master_data = [master_data,mat1];
            mastLab = [mastLab,labs1];
        end
        for i=[5,10]
            [mat1,labs1] = calcTrajFeatures(movstd(tmp,ceil(deltaT(dT)/i),[],2));
            for l=1:length(labs1)
                labs1{l} = [labs1{l} '_' nms{j} '_norm_movstd_deltaT_window_' num2str(ceil(deltaT(dT)/i))];
            end
            master_data = [master_data,mat1];
            mastLab = [mastLab,labs1];
        end
    end

    %Use label cells Label cells as either Resistant or Sensitive.  Include untreated
    %sensitive as resistant cells!
    tmp_kan_r = kan_r(indx);tmp_drug_conc=drug_conc(indx);tmp_drug_nm = drugNm(indx);
    master_label = ones(length(tmp_kan_r),1);
    %Sensitive cells are zero
    master_label((tmp_kan_r==0)&(tmp_drug_conc==100)&(contains(tmp_drug_nm,'kan'))) = 0;
    %Normalize data
    master_data = normalize(master_data);
    
    master_data = master_data(:,sum(isnan(master_data))~=length(master_data));
    master_label = master_label(sum(isnan(master_data'))==0);
    master_data = master_data(sum(isnan(master_data'))==0,:);
    %Uncomment to examine how class labels separate
    [~,score,~,~,explained,~] = pca(master_data);

    %PCA downsampling...
    %Find the index that retains 95% of the variance
    expl = 0;pca_dim = 0;
    for i=1:length(explained)
        expl = expl+explained(i);
        if expl>95
            pca_dim = i;
            break
        end
    end
    master_data = score(:,1:pca_dim);

    for j = 1:kFoldCnt
        %Keep an equal amount of sensitive and resistant cells for training
        mn = min([sum(master_label==1),sum(master_label==0)]);
        idx_sens = find(master_label==0);idx_res = find(master_label==1);
        idx_sens = idx_sens(randperm(length(idx_sens)));
        idx_res = idx_res(randperm(length(idx_res)));
        idx_sens = idx_sens(1:mn);idx_res=idx_res(1:mn);
        idx = union(idx_res,idx_sens);
        tmp_master_data = master_data(idx,:);
        tmp_master_label = master_label(idx);

        %Use 80% of the data for training the model
        spt = rand(length(tmp_master_label),1)> 0.8;
        test = tmp_master_data(spt,:);
        train = tmp_master_data(spt==0,:);
        test_lab = tmp_master_label(spt,:);
        train_lab = tmp_master_label(spt==0,:);
        %Set up template Tree
        %See https://www.mathworks.com/help/stats/templatetree.html for list of
        %hyperparameters
        %%%SET PARAMETERS HERE!!!!!!
        t = templateTree('Reproducible',true,'MinLeafSize',7,'MaxNumSplits',34);
        Mdl1 = fitcensemble(train,train_lab,'Method','AdaBoostM1','NumLearningCycles',442,'LearnRate',0.94414,'NPrint',0);

        [predict_labels,predict_score] = predict(Mdl1, test);
        [~,~,~,auc] = perfcurve(test_lab,predict_score(:,1),0);
        %c = confusionmat(test_lab,predict_labels);
        cp1 = classperf(test_lab,predict_labels);
        aucComp(dT,j) = auc;
        errorRate(dT,j) = cp1.ErrorRate;
        correctRate(dT,j) = cp1.CorrectRate;


        %%Randomize and measure control
        tmp_train = train(randperm(length(train)),:);
        t = templateTree('Reproducible',true,'MinLeafSize',7,'MaxNumSplits',34);
        Mdl2 = fitcensemble(tmp_train,train_lab,'Method','AdaBoostM1','NumLearningCycles',442,'LearnRate',0.94414,'NPrint',0);

        [predict_labels,predict_score] = predict(Mdl2, test);
        [~,~,~,auc] = perfcurve(test_lab,predict_score(:,1),0);
        %c = confusionmat(test_lab,predict_labels);
        cp2 = classperf(test_lab,predict_labels);
        aucCompCon(dT,j) = auc;
        errorRateCon(dT,j) = cp2.ErrorRate;
        correctRateCon(dT,j) = cp2.CorrectRate;
    end
end

save('deltaT_vs_kanModel.mat','deltaT','aucComp','aucCompCon','errorRate','errorRateCon','correctRate','correctRateCon')

fig = figure('Color','w','Position',[900 1200 200 120]);
ax1 = subplot(1,1,1);
shadedErrorBar(deltaT,errorRate'*100,{@mean,@std},'lineProps',{'r','markerfacecolor','r','linewidth',2})
shadedErrorBar(deltaT,errorRateCon'*100,{@mean,@std},'lineProps',{'b','markerfacecolor','b','linewidth',2})
xlabel('\Deltat')
ylabel('Error Rate')
ylim([0,60])
p = xline(min(deltaT(mean(errorRate')<.05)),'c','linewidth',2);
set(get(get(p,'Annotation'),'LegendInformation'),'IconDisplayStyle','off')
legend({'Training Data','Scramble Control'},'Location','East')
tightfig
set(gca,'Linewidth',2,'FontSize',8,'FontName','Arial')
saveas(fig,'deltaT_kan_ml_gcamp_error.pdf')

fig = figure('Color','w','Position',[900 1200 200 120]);
ax1 = subplot(1,1,1);
shadedErrorBar(deltaT,aucComp',{@mean,@std},'lineProps',{'r','markerfacecolor','r','linewidth',2})
shadedErrorBar(deltaT,aucCompCon',{@mean,@std},'lineProps',{'b','markerfacecolor','b','linewidth',2})
xlabel('\Deltat')
ylabel('ROC AUC')
ylim([.4,1])
p = xline(min(deltaT(mean(errorRate')<.05)),'c','linewidth',2);
set(get(get(p,'Annotation'),'LegendInformation'),'IconDisplayStyle','off')
legend({'Training Data','Scramble Control'},'Location','East')
tightfig
set(gca,'Linewidth',2,'FontSize',8,'FontName','Arial')
saveas(fig,'deltaT_kan_ml_gcamp_auc.pdf')




