%Change into the correct directory
cd ~/Repos/GCAMP_paper/Data_Code/Figure4/

%% Step 1: Read in data generated from the "extract_GMmS_TIF_2color_parallel.m" file
%Clear everything and start a timer
clear; close all; tic
saveModel_name = '011521_baggedTree';      %Name of models
%Set active directory
activeDir      = '/mnt/kralj_celery/E coli GC antibioitic/*/2021-01-15_Results/matFiles/';
flist          = dir([activeDir '*' filesep '*data.mat']); %Current file list
nfiles         = length(flist); %Number of files

%Read in the feature matrices
cellCnt = 0;
for i = 1:length(flist)
    load([flist(i).folder filesep flist(i).name])
    cellCnt = cellCnt + S.ncells;
end
nframes        = size(S.cellMeanIntG,2);

drug_conc = zeros(cellCnt,1);kan_r=zeros(cellCnt,1);  %Information on drug treatment and resistance status
cellArea  = zeros(cellCnt,nframes);    %Change in cell area over time
%Feature matrices on the trajectories for both Red and Green ch
intMatG   = zeros(cellCnt,nframes); minIntMatG = zeros(cellCnt,nframes); maxIntMatG = zeros(cellCnt,nframes); stdIntMatG = zeros(cellCnt,nframes);
intMatR   = zeros(cellCnt,nframes); minIntMatR = zeros(cellCnt,nframes); maxIntMatR = zeros(cellCnt,nframes); stdIntMatR = zeros(cellCnt,nframes);
%Feature matrix on the segmentation used to filter cells
segArea   = zeros(cellCnt,1);segPerimeter=zeros(cellCnt,1);segMajAxisLen=zeros(cellCnt,1);segMinAxisLen=zeros(cellCnt,1);segCircularity=zeros(cellCnt,1);
%For each mat file, load and append the data to the appropriate matrix
cellCnt = [1 1];
for i = 1:length(flist)
    disp(['Loading file ' num2str(i)])
    load([flist(i).folder filesep flist(i).name])
    cellCnt(2) = cellCnt(1)+S.ncells-1;
    intMatG(cellCnt(1):cellCnt(2),:)      = S.cellMeanIntG;
    intMatR(cellCnt(1):cellCnt(2),:)      = S.cellMeanIntR;
    minIntMatG(cellCnt(1):cellCnt(2),:)   = S.cellMinIntG;
    minIntMatR(cellCnt(1):cellCnt(2),:)   = S.cellMinIntR;
    maxIntMatG(cellCnt(1):cellCnt(2),:)   = S.cellMaxIntG;
    maxIntMatR(cellCnt(1):cellCnt(2),:)   = S.cellMaxIntR;
    stdIntMatG(cellCnt(1):cellCnt(2),:)   = S.cellStdIntG;
    stdIntMatR(cellCnt(1):cellCnt(2),:)   = S.cellStdIntR;
    cellArea(cellCnt(1):cellCnt(2),:)       = S.cellArea;
    drug_conc(cellCnt(1):cellCnt(2))      = ones(S.ncells,1)*S.currConc;
    kan_r(cellCnt(1):cellCnt(2))          = ones(S.ncells,1)*contains(S.currCell,'KanR');
    segArea(cellCnt(1):cellCnt(2))        = S.segArea;
    segMajAxisLen(cellCnt(1):cellCnt(2))  = S.segMajAxisLen;
    segMinAxisLen(cellCnt(1):cellCnt(2))  = S.segMinAxisLen;
    segCircularity(cellCnt(1):cellCnt(2)) = S.segCircularity;
    segPerimeter(cellCnt(1):cellCnt(2))   = S.segPerimeter;
    cellCnt(1)                            = cellCnt(2)+1;
end
%Deal with the strange error on circularity being infinite
segCircularity(abs(segCircularity)==Inf)=0;


%% Step 2:  "Gate" the cells based on different requirements
%Remove cells which vanished from segmentation (matArea==0)
indx    = find(sum(cellArea'==0)>0);
%Fillmissing values if possible using movmedian over 15 frames
intMatG = fillmissing(intMatG','movmean',15)';
intMatR = fillmissing(intMatR','movmean',15)';
%If still undefined, remove 
indx    = union(indx,find(sum(isnan(intMatG'))>0));
indx    = union(indx,find(sum(isnan(intMatR'))>0));
%Remove cells which have segmentation parameter too different
dat = [segArea,segMajAxisLen,segMinAxisLen,segCircularity,segPerimeter];
dat = normalize(dat);
[~,score,~,~,explained,~] = pca(dat); %Run PCA to visualize what cells are being discarded
dist_mat = mean(squareform(pdist(score)));
data_keep = .95;  %What percentage to keep?  #############################
tmp = sort(dist_mat);
indx  = union(indx,find(dist_mat>tmp(floor(length(dist_mat)*data_keep))));
%Visualize cells
fig = figure();
hold on
scatter(score(:,1),score(:,2)); scatter(score(indx,1),score(indx,2))
legend('keep','remove')
xlabel('PCA1');ylabel('PCA2');
saveas(fig,'MultiDim_gating')

%Which indices to keep
to_keep    = setdiff(1:length(dist_mat),indx);
intMatG    = intMatG(to_keep,:);
intMatR    = intMatR(to_keep,:);
minIntMatG = minIntMatG(to_keep,:);
minIntMatR = minIntMatR(to_keep,:);
maxIntMatG = maxIntMatG(to_keep,:);
maxIntMatR = maxIntMatR(to_keep,:);
stdIntMatG = stdIntMatG(to_keep,:);
stdIntMatR = stdIntMatR(to_keep,:);
cellArea   = cellArea(to_keep,:);
drug_conc  = drug_conc(to_keep);
kan_r      = kan_r(to_keep);



%% Sliding window model training
kFoldCnt = 10;
slidingWindowDeltaT = 5;
tRangeMin = 120;
tRangeMax = 180;


%% For the 60minute deltaT find the best approach using the HyperOptimization in Matlab
mm_data  = [];tmp_kan_r=[];tmp_drug_conc=[];
for tt = tRangeMin:tRangeMax-slidingWindowDeltaT-1
    S = struct();
    S.meanIntG = intMatG(:,tt:tt+slidingWindowDeltaT);
    S.maxIntG = maxIntMatG(:,tt:tt+slidingWindowDeltaT);
    S.minIntG = minIntMatG(:,tt:tt+slidingWindowDeltaT);
    S.stdIntG = minIntMatG(:,tt:tt+slidingWindowDeltaT);
    nms = fieldnames(S);
    master_data  = []; mastLab=[];

    for j = 1:length(nms)
        [mat1,labs1] = calcTrajFeatures(S.(nms{j}));
        for l=1:length(labs1)
            labs1{l} = [labs1{l} '_' nms{j}];
        end
        master_data = [master_data,mat1];
        mastLab = [mastLab,labs1];
        for i=[2,5]
            [mat1,labs1] = calcTrajFeatures(movmean(S.(nms{j}),i,2));
            for l=1:length(labs1)
                labs1{l} = [labs1{l} '_' nms{j} '_movmean_deltaT_window_' num2str(i)];
            end
            master_data = [master_data,mat1];
            mastLab = [mastLab,labs1];
        end
        for i=[2,5]
            [mat1,labs1] = calcTrajFeatures(movstd(S.(nms{j}),i,[],2));
            for l=1:length(labs1)
                labs1{l} = [labs1{l} '_' nms{j} '_movstd_deltaT_window_' num2str(i)];
            end
            master_data = [master_data,mat1];
            mastLab = [mastLab,labs1];
        end

        %Now normalize matrix to T==0
        tmp = S.(nms{j});tmp = tmp./tmp(:,1);
        [mat1,labs1] = calcTrajFeatures(tmp);
        for l=1:length(labs1)
            labs1{l} = [labs1{l} '_' nms{j} '_norm'];
        end
        master_data = [master_data,mat1];
        mastLab = [mastLab,labs1];
        for i=[2,5]
            [mat1,labs1] = calcTrajFeatures(movmean(tmp,i,2));
            for l=1:length(labs1)
                labs1{l} = [labs1{l} '_' nms{j} '_norm_movmean_deltaT_window_' num2str(i)];
            end
            master_data = [master_data,mat1];
            mastLab = [mastLab,labs1];
        end
        for i=[2,5]
            [mat1,labs1] = calcTrajFeatures(movstd(tmp,i,[],2));
            for l=1:length(labs1)
                labs1{l} = [labs1{l} '_' nms{j} '_norm_movstd_deltaT_window_' num2str(i)];
            end
            master_data = [master_data,mat1];
            mastLab = [mastLab,labs1];
        end
    end
    mm_data = [mm_data;master_data];
    tmp_kan_r = [tmp_kan_r;kan_r];
    tmp_drug_conc = [tmp_drug_conc;drug_conc];
end

%T = array2table(master_data,'VariableNames',mastLab);
%Label cells as either Resistant or Sensitive.  Include untreated
%sensitive as resistant cells!
indx = find((tmp_kan_r==1) | (((tmp_kan_r==0)&(tmp_drug_conc==100)) | ((tmp_kan_r==0)&(tmp_drug_conc==0))));
master_data = mm_data(indx,:);
kr = tmp_kan_r(indx);dc=tmp_drug_conc(indx);
master_label = ones(length(kr),1);
%Sensitive cells are zero
master_label((kr==0)&(dc==100)) = 0;

%Normalize data
master_data = normalize(master_data);

%% Begin training model
%Split the data into training and testing
rng(5)
disp(['Starting Model Learning, first time range: '])
%Use only 5% of the data for training the model
spt = rand(length(master_data),1)> 0.8;
test = master_data(spt,:);
train = master_data(spt==0,:);
test_lab = master_label(spt,:);
train_lab = master_label(spt==0,:);

%Best according to hyperparameter optimization
t = templateTree('Reproducible',true,'MinLeafSize',16,'MaxNumSplits',47);
Mdl1 = fitcensemble(train,train_lab,'Method','AdaBoostM1','NumLearningCycles',80,'LearnRate',0.24094,'CrossVal','on','KFold',kFoldCnt,'NPrint',0,'Learners',t);
modelLosses = kfoldLoss(Mdl1,'mode','individual');
[predict_labels,predict_score] = predict(Mdl1.Trained{find(modelLosses==min(modelLosses))}, test);
auc = zeros(2,1);
figure;
hold on
ax = subplot(1,1,1);
for k = 0:1
    [xVal,yVal,~,auc(k+1)] = perfcurve(test_lab(1:end),predict_score(1:end,k+1),k);
    stp = round(length(xVal)*.01);
    plot(ax,xVal(1:stp:end),yVal(1:stp:end),'Linewidth',5);
    text(0.5,.15*(k+1),strcat('Class: ',num2str(k), ' AUC=',num2str(auc(k+1))));
end
legend({'Back','Sens','Res'});
xlabel('False Positive Rate');
ylabel('True Positive Rate');
title('Mdl1:  t=120-180, 5min sliding window')


save('test_labs_tmp.mat','test','test_lab')






%% Sliding window model training
tRangeMin = 180;
tRangeMax = 240;


%% For the 60minute deltaT find the best approach using the HyperOptimization in Matlab
mm_data  = [];tmp_kan_r=[];tmp_drug_conc=[];
for tt = tRangeMin:tRangeMax-slidingWindowDeltaT-1
    S = struct();
    S.meanIntG = intMatG(:,tt:tt+slidingWindowDeltaT);
    S.maxIntG = maxIntMatG(:,tt:tt+slidingWindowDeltaT);
    S.minIntG = minIntMatG(:,tt:tt+slidingWindowDeltaT);
    S.stdIntG = minIntMatG(:,tt:tt+slidingWindowDeltaT);
    nms = fieldnames(S);
    master_data  = []; mastLab=[];

    for j = 1:length(nms)
        [mat1,labs1] = calcTrajFeatures(S.(nms{j}));
        for l=1:length(labs1)
            labs1{l} = [labs1{l} '_' nms{j}];
        end
        master_data = [master_data,mat1];
        mastLab = [mastLab,labs1];
        for i=[2,5]
            [mat1,labs1] = calcTrajFeatures(movmean(S.(nms{j}),i,2));
            for l=1:length(labs1)
                labs1{l} = [labs1{l} '_' nms{j} '_movmean_deltaT_window_' num2str(i)];
            end
            master_data = [master_data,mat1];
            mastLab = [mastLab,labs1];
        end
        for i=[2,5]
            [mat1,labs1] = calcTrajFeatures(movstd(S.(nms{j}),i,[],2));
            for l=1:length(labs1)
                labs1{l} = [labs1{l} '_' nms{j} '_movstd_deltaT_window_' num2str(i)];
            end
            master_data = [master_data,mat1];
            mastLab = [mastLab,labs1];
        end

        %Now normalize matrix to T==0
        tmp = S.(nms{j});tmp = tmp./tmp(:,1);
        [mat1,labs1] = calcTrajFeatures(tmp);
        for l=1:length(labs1)
            labs1{l} = [labs1{l} '_' nms{j} '_norm'];
        end
        master_data = [master_data,mat1];
        mastLab = [mastLab,labs1];
        for i=[2,5]
            [mat1,labs1] = calcTrajFeatures(movmean(tmp,i,2));
            for l=1:length(labs1)
                labs1{l} = [labs1{l} '_' nms{j} '_norm_movmean_deltaT_window_' num2str(i)];
            end
            master_data = [master_data,mat1];
            mastLab = [mastLab,labs1];
        end
        for i=[2,5]
            [mat1,labs1] = calcTrajFeatures(movstd(tmp,i,[],2));
            for l=1:length(labs1)
                labs1{l} = [labs1{l} '_' nms{j} '_norm_movstd_deltaT_window_' num2str(i)];
            end
            master_data = [master_data,mat1];
            mastLab = [mastLab,labs1];
        end
    end
    mm_data = [mm_data;master_data];
    tmp_kan_r = [tmp_kan_r;kan_r];
    tmp_drug_conc = [tmp_drug_conc;drug_conc];
end

%T = array2table(master_data,'VariableNames',mastLab);
%Label cells as either Resistant or Sensitive.  Include untreated
%sensitive as resistant cells!
indx = find((tmp_kan_r==1) | (((tmp_kan_r==0)&(tmp_drug_conc==100)) | ((tmp_kan_r==0)&(tmp_drug_conc==0))));
master_data = mm_data(indx,:);
kr = tmp_kan_r(indx);dc=tmp_drug_conc(indx);
master_label = ones(length(kr),1);
%Sensitive cells are zero
master_label((kr==0)&(dc==100)) = 0;

%Normalize data
master_data = normalize(master_data);


%% Begin training model
%Split the data into training and testing
rng(5)
disp(['Starting Model Learning, second time range: '])
%Use only 5% of the data for training the model
spt = rand(length(master_data),1)> 0.8;
test = master_data(spt,:);
train = master_data(spt==0,:);
test_lab = master_label(spt,:);
train_lab = master_label(spt==0,:);

%Best according to hyperparameter optimization
t = templateTree('Reproducible',true,'MinLeafSize',16,'MaxNumSplits',47);
Mdl2 = fitcensemble(train,train_lab,'Method','AdaBoostM1','NumLearningCycles',80,'LearnRate',0.24094,'CrossVal','on','KFold',kFoldCnt,'NPrint',0,'Learners',t);
modelLosses = kfoldLoss(Mdl2,'mode','individual');
[predict_labels,predict_score] = predict(Mdl2.Trained{find(modelLosses==min(modelLosses))}, test);
auc = zeros(2,1);
figure;
hold on
ax = subplot(1,1,1);
for k = 0:1
    [xVal,yVal,~,auc(k+1)] = perfcurve(test_lab(1:end),predict_score(1:end,k+1),k);
    stp = round(length(xVal)*.01);
    plot(ax,xVal(1:stp:end),yVal(1:stp:end),'Linewidth',5);
    text(0.5,.15*(k+1),strcat('Class: ',num2str(k), ' AUC=',num2str(auc(k+1))));
end
legend({'Back','Sens','Res'});
xlabel('False Positive Rate');
ylabel('True Positive Rate');
title('Mdl2:  t=180-240, 5min sliding window')


figure()
load('test_labs_tmp.mat')
[predict_labels,predict_score] = predict(Mdl2.Trained{find(modelLosses==min(modelLosses))}, test);
auc = zeros(2,1);
figure;
hold on
ax = subplot(1,1,1);
for k = 0:1
    [xVal,yVal,~,auc(k+1)] = perfcurve(test_lab(1:end),predict_score(1:end,k+1),k);
    stp = round(length(xVal)*.01);
    plot(ax,xVal(1:stp:end),yVal(1:stp:end),'Linewidth',5);
    text(0.5,.15*(k+1),strcat('Class: ',num2str(k), ' AUC=',num2str(auc(k+1))));
end
legend({'Back','Sens','Res'});
xlabel('False Positive Rate');
ylabel('True Positive Rate');
title('Use Mdl2 to predict Mdl1 data')




