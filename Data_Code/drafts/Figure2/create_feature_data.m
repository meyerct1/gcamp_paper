%Change into the correct directory
cd ~/Repos/GCAMP_paper/Data_Code/Figure2/

%% Step 1: Read in data generated from the "extract_GMmS_TIF_2color_parallel.m" file
%Clear everything and start a timer
clear; close all; tic
saveModel_name = '011521_baggedTree';      %Name of models
%Set active directory
activeDir      = '/mnt/kralj_celery/E coli GC antibioitic/*/2021-01-15_Results/matFiles/';
flist          = dir([activeDir '*' filesep '*data.mat']); %Current file list
nfiles         = length(flist); %Number of files

%Read in the feature matrices
cellCnt = 0;
for i = 1:length(flist)
    load([flist(i).folder filesep flist(i).name])
    cellCnt = cellCnt + S.ncells;
end
nframes        = size(S.cellMeanIntG,2);

drug_conc = zeros(cellCnt,1);kan_r=zeros(cellCnt,1);  %Information on drug treatment and resistance status
cellArea  = zeros(cellCnt,nframes);    %Change in cell area over time
%Feature matrices on the trajectories for both Red and Green ch
intMatG   = zeros(cellCnt,nframes); minIntMatG = zeros(cellCnt,nframes); maxIntMatG = zeros(cellCnt,nframes); stdIntMatG = zeros(cellCnt,nframes);
intMatR   = zeros(cellCnt,nframes); minIntMatR = zeros(cellCnt,nframes); maxIntMatR = zeros(cellCnt,nframes); stdIntMatR = zeros(cellCnt,nframes);
%Feature matrix on the segmentation used to filter cells
segArea   = zeros(cellCnt,1);segPerimeter=zeros(cellCnt,1);segMajAxisLen=zeros(cellCnt,1);segMinAxisLen=zeros(cellCnt,1);segCircularity=zeros(cellCnt,1);
%For each mat file, load and append the data to the appropriate matrix
cellCnt = [1 1];
for i = 1:length(flist)
    disp(['Loading file ' num2str(i)])
    load([flist(i).folder filesep flist(i).name])
    cellCnt(2) = cellCnt(1)+S.ncells-1;
    intMatG(cellCnt(1):cellCnt(2),:)      = S.cellMeanIntG;
    intMatR(cellCnt(1):cellCnt(2),:)      = S.cellMeanIntR;
    minIntMatG(cellCnt(1):cellCnt(2),:)   = S.cellMinIntG;
    minIntMatR(cellCnt(1):cellCnt(2),:)   = S.cellMinIntR;
    maxIntMatG(cellCnt(1):cellCnt(2),:)   = S.cellMaxIntG;
    maxIntMatR(cellCnt(1):cellCnt(2),:)   = S.cellMaxIntR;
    stdIntMatG(cellCnt(1):cellCnt(2),:)   = S.cellStdIntG;
    stdIntMatR(cellCnt(1):cellCnt(2),:)   = S.cellStdIntR;
    cellArea(cellCnt(1):cellCnt(2),:)       = S.cellArea;
    drug_conc(cellCnt(1):cellCnt(2))      = ones(S.ncells,1)*S.currConc;
    kan_r(cellCnt(1):cellCnt(2))          = ones(S.ncells,1)*contains(S.currCell,'KanR');
    segArea(cellCnt(1):cellCnt(2))        = S.segArea;
    segMajAxisLen(cellCnt(1):cellCnt(2))  = S.segMajAxisLen;
    segMinAxisLen(cellCnt(1):cellCnt(2))  = S.segMinAxisLen;
    segCircularity(cellCnt(1):cellCnt(2)) = S.segCircularity;
    segPerimeter(cellCnt(1):cellCnt(2))   = S.segPerimeter;
    cellCnt(1)                            = cellCnt(2)+1;
end
%Deal with the strange error on circularity being infinite
segCircularity(abs(segCircularity)==Inf)=0;


%% Step 2:  "Gate" the cells based on different requirements
%Remove cells which vanished from segmentation (matArea==0)
indx    = find(sum(cellArea'==0)>0);
%Fillmissing values if possible using movmedian over 15 frames
intMatG = fillmissing(intMatG','movmean',15)';
intMatR = fillmissing(intMatR','movmean',15)';
%If still undefined, remove 
indx    = union(indx,find(sum(isnan(intMatG'))>0));
indx    = union(indx,find(sum(isnan(intMatR'))>0));
%Remove cells which have segmentation parameter too different
dat = [segArea,segMajAxisLen,segMinAxisLen,segCircularity,segPerimeter];
dat = normalize(dat);
[~,score,~,~,explained,~] = pca(dat); %Run PCA to visualize what cells are being discarded
dist_mat = mean(squareform(pdist(score)));
data_keep = .95;  %What percentage to keep?  #############################
tmp = sort(dist_mat);
indx  = union(indx,find(dist_mat>tmp(floor(length(dist_mat)*data_keep))));
%Visualize cells
fig = figure();
hold on
scatter(score(:,1),score(:,2)); scatter(score(indx,1),score(indx,2))
legend('keep','remove')
xlabel('PCA1');ylabel('PCA2');
saveas(fig,'MultiDim_gating')

%Which indices to keep
to_keep    = setdiff(1:length(dist_mat),indx);
intMatG    = intMatG(to_keep,:);
intMatR    = intMatR(to_keep,:);
minIntMatG = minIntMatG(to_keep,:);
minIntMatR = minIntMatR(to_keep,:);
maxIntMatG = maxIntMatG(to_keep,:);
maxIntMatR = maxIntMatR(to_keep,:);
stdIntMatG = stdIntMatG(to_keep,:);
stdIntMatR = stdIntMatR(to_keep,:);
cellArea   = cellArea(to_keep,:);
drug_conc  = drug_conc(to_keep);
kan_r      = kan_r(to_keep);


%% For the 60minute deltaT find the best approach using the HyperOptimization in Matlab
deltaT = [30];
dT = 1;
S = struct();
S.meanIntG = intMatG(:,1:deltaT(dT));
S.maxIntG = maxIntMatG(:,1:deltaT(dT));
S.minIntG = minIntMatG(:,1:deltaT(dT));
S.stdIntG = minIntMatG(:,1:deltaT(dT));
nms = fieldnames(S);
master_data  = []; mastLab=[];
for j = 1:length(nms)
    [mat1,labs1] = calcTrajFeatures(S.(nms{j}));
    for l=1:length(labs1)
        labs1{l} = [labs1{l} '_' nms{j}];
    end
    master_data = [master_data,mat1];
    mastLab = [mastLab,labs1];
    for i=[2,5,10]
        [mat1,labs1] = calcTrajFeatures(movmean(S.(nms{j}),ceil(deltaT(dT)/i),2));
        for l=1:length(labs1)
            labs1{l} = [labs1{l} '_' nms{j} '_movmean_deltaT_window_' num2str(ceil(deltaT(dT)/i))];
        end
        master_data = [master_data,mat1];
        mastLab = [mastLab,labs1];
    end
    for i=[2,5,10]
        [mat1,labs1] = calcTrajFeatures(movstd(S.(nms{j}),ceil(deltaT(dT)/i),[],2));
        for l=1:length(labs1)
            labs1{l} = [labs1{l} '_' nms{j} '_movstd_deltaT_window_' num2str(ceil(deltaT(dT)/i))];
        end
        master_data = [master_data,mat1];
        mastLab = [mastLab,labs1];
    end

    %Now normalize matrix to T==0
    tmp = S.(nms{j});tmp = tmp./tmp(:,1);
    [mat1,labs1] = calcTrajFeatures(tmp);
    for l=1:length(labs1)
        labs1{l} = [labs1{l} '_' nms{j} '_norm'];
    end
    master_data = [master_data,mat1];
    mastLab = [mastLab,labs1];
    for i=[2,5,10]
        [mat1,labs1] = calcTrajFeatures(movmean(tmp,ceil(deltaT(dT)/i),2));
        for l=1:length(labs1)
            labs1{l} = [labs1{l} '_' nms{j} '_norm_movmean_deltaT_window_' num2str(ceil(deltaT(dT)/i))];
        end
        master_data = [master_data,mat1];
        mastLab = [mastLab,labs1];
    end
    for i=[2,5,10]
        [mat1,labs1] = calcTrajFeatures(movstd(tmp,ceil(deltaT(dT)/i),[],2));
        for l=1:length(labs1)
            labs1{l} = [labs1{l} '_' nms{j} '_norm_movstd_deltaT_window_' num2str(ceil(deltaT(dT)/i))];
        end
        master_data = [master_data,mat1];
        mastLab = [mastLab,labs1];
    end
end


%T = array2table(master_data,'VariableNames',mastLab);
%Label cells as either Resistant or Sensitive.  Include untreated
%sensitive as resistant cells!
indx = find((kan_r==1) | (((kan_r==0)&(drug_conc==100)) | ((kan_r==0)&(drug_conc==0))));
master_data = master_data(indx,:);
tmp_kan_r = kan_r(indx);tmp_drug_conc=drug_conc(indx);
master_label = ones(length(tmp_kan_r),1);
%Sensitive cells are zero
master_label((tmp_kan_r==0)&(tmp_drug_conc==100)) = 0;

%Normalize data
master_data = normalize(master_data);
%Uncomment to examine how class labels separate
[~,score,~,~,explained,~] = pca(master_data);
figure()
hold on
scatter(score(master_label==0,1),score(master_label==0,2))
scatter(score(master_label==1,1),score(master_label==1,2))
    

%% Begin training model
%Split the data into training and testing
rng(5)
disp(['Starting Model Learning, deltaT: ' num2str(deltaT)])
%Use only 5% of the data for training the model
spt = rand(length(master_data),1)> 0.8;
test = master_data(spt,:);
train = master_data(spt==0,:);
test_lab = master_label(spt,:);
train_lab = master_label(spt==0,:);
%Set up template Tree
%See https://www.mathworks.com/help/stats/templatetree.html for list of
%hyperparameters
numWorkers = 24;
p = gcp('nocreate'); % If no pool, do not create new one. ~14Gb/exp
if isempty(p)
    parpool(numWorkers);
end
t = templateTree('Reproducible',true);
%Save model as a compact ensemble
Mdl = fitcensemble(train,train_lab,'OptimizeHyperparameters','all',...
                                    'Learners',t,...
                                    'HyperparameterOptimizationOptions',struct('AcquisitionFunctionName','expected-improvement-plus',...
                                                                                'MaxObjectiveEvaluations',200,...
                                                                                'UseParallel',true,...
                                                                                'Repartition',true));
%Based on which boosting method proves to be superior, run a subsequent
%hyperoptimization for number of learning cycles, learn rate, maxnumsplits
%Use AdaBoostM1
Mdl1 = fitcensemble(train,train_lab,'Method','AdaBoostM1','OptimizeHyperparameters',{'NumLearningCycles','LearnRate','MaxNumSplits','MinLeafSize'},'Learners',t,...
                                                'HyperparameterOptimizationOptions',struct( 'MaxObjectiveEvaluations',200,...
                                                        'UseParallel',true,...
                                                        'Repartition',true));



%Best according to hyperparameter optimization
t = templateTree('Reproducible',true,'MinLeafSize',16,'MaxNumSplits',47);
Mdl2 = fitcensemble(train,train_lab,'Method','AdaBoostM1','NumLearningCycles',80,'LearnRate',0.24094,'CrossVal','on','KFold',kFoldCnt,'NPrint',0)

modelLosses = kfoldLoss(Mdl2,'mode','individual');
[predict_labels,predict_score] = predict(Mdl2.Trained{find(modelLosses==min(modelLosses))}, test);

auc = zeros(2,1);
figure;
hold on
ax = subplot(1,1,1);
for k = 0:1
    [xVal,yVal,~,auc(k+1)] = perfcurve(test_lab(1:end),predict_score(1:end,k+1),k);
    stp = round(length(xVal)*.01);
    plot(ax,xVal(1:stp:end),yVal(1:stp:end),'Linewidth',5);
    text(0.5,.15*(k+1),strcat('Class: ',num2str(k), ' AUC=',num2str(auc(k+1))));
end
legend({'Back','Sens','Res'});
xlabel('False Positive Rate');
ylabel('True Positive Rate');

cp = classperf(test_lab,predict_labels);
Mdl = Mdl2;

%% Plots for best model
%Save the model.  This is a LARGE file!
save([saveModel_name '_best.mat'],'Mdl','-v7.3')
%Save the best model based on cross validation as a compact model
modelLosses = kfoldLoss(Mdl,'mode','individual');
saveLearnerForCoder(Mdl.Trained{find(modelLosses==min(modelLosses))},saveModel_name)

%% PLOTS
%Plot the depth of the trees
numBranches = @(x)sum(x.IsBranch);
mdlDefaultNumSplits = cellfun(numBranches, Mdl.Trained{find(modelLosses==min(modelLosses))}.Trained);
figure;
histogram(mdlDefaultNumSplits)
xlabel('Number of Splits in Best Model')
savefig([saveModel_name '_branch-depth'])

%Plot the loss as a function of training rounds
figure;
kflc = kfoldLoss(Mdl,'Mode','cumulative');
plot(kflc);
ylabel('5-fold Misclassification rate');
xlabel('Learning cycle');
savefig([saveModel_name '_accuracy_num_learners'])

figure;
plot(modelLosses);
xlabel('Fold')
ylabel('Loss')
savefig([saveModel_name '_loss_kfold'])

%Plot confusion chart
figure;
[predict_labels,predict_score] = predict(Mdl.Trained{find(modelLosses==min(modelLosses))}, test);
c = confusionchart(test_lab,predict_labels,'Normalization','row-normalized');
savefig([saveModel_name '_lconfusion_chart'])

%Plot the ROC curve for each class  ASSUMES 3 CLASSES
figure;
hold on
ax = subplot(1,1,1);
for k = 0:1
    [xVal,yVal,~,auc] = perfcurve(test_lab(1:end),predict_score(1:end,k+1),k);
    stp = round(length(xVal)*.01);
    plot(ax,xVal(1:stp:end),yVal(1:stp:end),'Linewidth',5);
    text(0.5,.15*(k+1),strcat('Class: ',num2str(k), ' AUC=',num2str(auc)));
end
legend({'Back','Sens','Res'});
xlabel('False Positive Rate');
ylabel('True Positive Rate');
savefig([saveModel_name '_roc_curve'])


% PLot the feature importance
imp = predictorImportance(Mdl.Trained{find(modelLosses==min(modelLosses))});
figure;
bar(imp);
xticks(1:length(imp));
xticklabels({'mean','median','std','auc','max','min','dt(mean)','dt(median)','dt(std)','dt(auc)','dt(max)','dt(min)','dtdt(mean)','dtdt(median)','dtdt(std)','dtdt(auc)','dtdt(max)','dtdt(min)'});
set(gca,'fontsize',18);
xtickangle(90);
ylabel('Predictor Importance');
savefig([saveModel_name '_featureImportance'])






%% For different deltaT estimate the predictivity of the RandomForest learning algorithms
%Calculate feature matrices based on 1:deltaT frames
deltaT = 10:2:240;
kFoldCnt = 10;
aucComp = zeros(length(deltaT),kFoldCnt);
errorRate = zeros(length(deltaT),kFoldCnt);
correctRate = zeros(length(deltaT),kFoldCnt);
prec = zeros(length(deltaT),kFoldCnt,2);
rec = zeros(length(deltaT),kFoldCnt,2);
numWorkers = 24;
p = gcp('nocreate'); % If no pool, do not create new one. ~14Gb/exp
if isempty(p)
    parpool(numWorkers);
end
indx = find((kan_r==1) | (((kan_r==0)&(drug_conc==100)) | ((kan_r==0)&(drug_conc==0))));

% << for an existing Confision Matrix 'ConfusionMat' >>
precision = @(confusionMat) diag(confusionMat)./sum(confusionMat,2);
% And another for recall
recall = @(confusionMat) diag(confusionMat)./sum(confusionMat,1)';



parfor dT = 1:length(deltaT)
    %Add to structure
    S = struct();
    S.meanIntG = intMatG(:,1:deltaT(dT));
    S.maxIntG = maxIntMatG(:,1:deltaT(dT));
    S.minIntG = minIntMatG(:,1:deltaT(dT));
    S.stdIntG = minIntMatG(:,1:deltaT(dT));
    nms = fieldnames(S);
    master_data  = []; mastLab=[];
    for j = 1:length(nms)
        [mat1,labs1] = calcTrajFeatures(S.(nms{j}));
        for l=1:length(labs1)
            labs1{l} = [labs1{l} '_' nms{j}];
        end
        master_data = [master_data,mat1];
        mastLab = [mastLab,labs1];
        for i=[2,5,10]
            [mat1,labs1] = calcTrajFeatures(movmean(S.(nms{j}),ceil(deltaT(dT)/i),2));
            for l=1:length(labs1)
                labs1{l} = [labs1{l} '_' nms{j} '_movmean_deltaT_window_' num2str(ceil(deltaT(dT)/i))];
            end
            master_data = [master_data,mat1];
            mastLab = [mastLab,labs1];
        end
        for i=[2,5,10]
            [mat1,labs1] = calcTrajFeatures(movstd(S.(nms{j}),ceil(deltaT(dT)/i),[],2));
            for l=1:length(labs1)
                labs1{l} = [labs1{l} '_' nms{j} '_movstd_deltaT_window_' num2str(ceil(deltaT(dT)/i))];
            end
            master_data = [master_data,mat1];
            mastLab = [mastLab,labs1];
        end
        
        %Now normalize matrix to T==0
        tmp = S.(nms{j});tmp = tmp./tmp(:,1);
        [mat1,labs1] = calcTrajFeatures(tmp);
        for l=1:length(labs1)
            labs1{l} = [labs1{l} '_' nms{j} '_norm'];
        end
        master_data = [master_data,mat1];
        mastLab = [mastLab,labs1];
        for i=[2,5,10]
            [mat1,labs1] = calcTrajFeatures(movmean(tmp,ceil(deltaT(dT)/i),2));
            for l=1:length(labs1)
                labs1{l} = [labs1{l} '_' nms{j} '_norm_movmean_deltaT_window_' num2str(ceil(deltaT(dT)/i))];
            end
            master_data = [master_data,mat1];
            mastLab = [mastLab,labs1];
        end
        for i=[2,5,10]
            [mat1,labs1] = calcTrajFeatures(movstd(tmp,ceil(deltaT(dT)/i),[],2));
            for l=1:length(labs1)
                labs1{l} = [labs1{l} '_' nms{j} '_norm_movstd_deltaT_window_' num2str(ceil(deltaT(dT)/i))];
            end
            master_data = [master_data,mat1];
            mastLab = [mastLab,labs1];
        end
        
    end

    %T = array2table(master_data,'VariableNames',mastLab);
    %Label cells as either Resistant or Sensitive.  Include untreated
    %sensitive as resistant cells!
    master_data = master_data(indx,:);
    tmp_kan_r = kan_r(indx);tmp_drug_conc=drug_conc(indx);
    master_label = ones(length(tmp_kan_r),1);
    %Sensitive cells are zero
    master_label((tmp_kan_r==0)&(tmp_drug_conc==100)) = 0;

    %Normalize data
    master_data = normalize(master_data);
    %Uncomment to examine how class labels separate
%     [~,score,~,~,explained,~] = pca(master_data);
%     figure()
%     hold on
%     scatter(score(master_label==0,1),score(master_label==0,2))
%     scatter(score(master_label==1,1),score(master_label==1,2))
%     
    %% Begin training model
    %Split the data into training and testing
    rng(5)
    disp(['Starting Model Learning, deltaT: ' num2str(deltaT(dT))])
    %Use only 5% of the data for training the model
    spt = rand(length(master_data),1)> 0.8;
    test = master_data(spt,:);
    train = master_data(spt==0,:);
    test_lab = master_label(spt,:);
    train_lab = master_label(spt==0,:);
    %Set up template Tree
    %See https://www.mathworks.com/help/stats/templatetree.html for list of
    %hyperparameters
    t = templateTree('Reproducible',true,'MinLeafSize',16,'MaxNumSplits',47);
    Mdl = fitcensemble(train,train_lab,'Method','AdaBoostM1','NumLearningCycles',80,'LearnRate',0.24094,'CrossVal','on','KFold',kFoldCnt,'NPrint',0)

    for j = 1:kFoldCnt
        [predict_labels,predict_score] = predict(Mdl.Trained{j}, test);
        [~,~,~,auc] = perfcurve(test_lab,predict_score(:,1),0);
        %c = confusionmat(test_lab,predict_labels);
        cp = classperf(test_lab,predict_labels);
        aucComp(dT,j) = auc;
        errorRate(dT,j) = cp.ErrorRate;
        correctRate(dT,j) = cp.CorrectRate;
    end
end

fig = figure('Color','w','Position',[900 1200 800 200]);
ax1 = subplot(1,3,1);
shadedErrorBar(deltaT,aucComp',{@mean,@std},'lineProps',{'r','markerfacecolor','r','linewidth',2})
title('ROC AUC')
xlabel('deltaT')
ylabel('AUC')
set(gca,'Linewidth',2,'FontSize',8,'FontName','Arial')

ax2 = subplot(1,3,2);
shadedErrorBar(deltaT,errorRate'*100.,{@mean,@std},'lineProps',{'r','markerfacecolor','r','linewidth',2})
title('Error Rate')
ylabel('Rate (%)')
xlabel('deltaT')
set(gca,'Linewidth',2,'FontSize',8,'FontName','Arial')

ax3 = subplot(1,3,3);
shadedErrorBar(deltaT,correctRate'*100.,{@mean,@std},'lineProps',{'r','markerfacecolor','r','linewidth',2})
title('Correct Rate')
ylabel('Rate (%)')
xlabel('deltaT')
set(gca,'Linewidth',2,'FontSize',8,'FontName','Arial')

tightfig

saveas(fig,'deltaT_ml_quality.pdf')











%% For different deltaT estimate the predictivity of the RandomForest learning algorithms
%Calculate feature matrices based on 1:deltaT frames
deltaT = 10:5:240;
kFoldCnt = 10;
aucCompG = zeros(length(deltaT),kFoldCnt);
errorRateG = zeros(length(deltaT),kFoldCnt);
correctRateG = zeros(length(deltaT),kFoldCnt);
numWorkers = 24;
p = gcp('nocreate'); % If no pool, do not create new one. ~14Gb/exp
if isempty(p)
    parpool(numWorkers);
end

parfor dT = 1:length(deltaT)
    %Add to structure
    S = struct();
    S.meanIntG = intMatG(:,1:deltaT(dT));
    S.maxIntG = maxIntMatG(:,1:deltaT(dT));
    S.minIntG = minIntMatG(:,1:deltaT(dT));
    S.stdIntG = minIntMatG(:,1:deltaT(dT));
    nms = fieldnames(S);
    master_data  = []; mastLab=[];
    for j = 1:length(nms)
        [mat1,labs1] = calcTrajFeatures(S.(nms{j}));
        for l=1:length(labs1)
            labs1{l} = [labs1{l} '_' nms{j}];
        end
        [mat2,labs2] = calcTrajFeatures(movmean(S.(nms{j}),ceil(deltaT(dT)),2));
        for l=1:length(labs2)
            labs2{l} = [labs2{l} '_' nms{j} '_movmean_deltaT'];
        end
        [mat3,labs3] = calcTrajFeatures(movmean(S.(nms{j}),ceil(deltaT(dT)/2),2));
        for l=1:length(labs3)
            labs3{l} = [labs3{l} '_' nms{j}  '_movmean_deltaTdiv2'];
        end
        [mat4,labs4] = calcTrajFeatures(movmean(S.(nms{j}),ceil(deltaT(dT)/10),2));
        for l=1:length(labs4)
            labs4{l} = [labs4{l} '_' nms{j}  '_movmean_deltaTdiv10'];
        end
        master_data = [master_data,mat1,mat2,mat3,mat4];
        mastLab = [mastLab,labs1,labs2,labs3,labs4];
    end

    %T = array2table(master_data,'VariableNames',mastLab);
    %Label cells as either Resistant or Sensitive.  Include untreated
    %sensitive as resistant cells!
    master_data = master_data(indx,:);
    tmp_kan_r = kan_r(indx);tmp_drug_conc=drug_conc(indx);
    master_label = ones(length(tmp_kan_r),1);
    %Sensitive cells are zero
    master_label((tmp_kan_r==0)&(tmp_drug_conc==100)) = 0;

    %Normalize data
    master_data = normalize(master_data);
    %Uncomment to examine how class labels separate
%     [~,score,~,~,explained,~] = pca(master_data);
%     figure()
%     hold on
%     scatter(score(master_label==0,1),score(master_label==0,2))
%     scatter(score(master_label==1,1),score(master_label==1,2))
%     
    %% Begin training model
    %Split the data into training and testing
    rng(5)
    disp(['Starting Model Learning, deltaT: ' num2str(deltaT(dT))])
    %Use only 5% of the data for training the model
    spt = rand(length(master_data),1)> 0.8;
    test = master_data(spt,:);
    train = master_data(spt==0,:);
    test_lab = master_label(spt,:);
    train_lab = master_label(spt==0,:);
    %Set up template Tree
    %See https://www.mathworks.com/help/stats/templatetree.html for list of
    %hyperparameters
    t = templateTree('Reproducible',true,'MinLeafSize',2,'MaxNumSplits',963);
    Mdl = fitcensemble(train,train_lab,'Method','AdaBoostM1','NumLearningCycles',25,'LearnRate',0.4605,'CrossVal','on','KFold',kFoldCnt,'NPrint',0)

    for j = 1:kFoldCnt
        [predict_labels,predict_score] = predict(Mdl.Trained{j}, test);
        [~,~,~,auc] = perfcurve(test_lab,predict_score(:,k+1),k);
        cp = classperf(test_lab,predict_labels);
        aucCompG(dT,j) = auc;
        errorRateG(dT,j) = cp.ErrorRate;
        correctRateG(dT,j) = cp.CorrectRate;
    end
end



%% For different deltaT estimate the predictivity of the RandomForest learning algorithms
%Calculate feature matrices based on 1:deltaT frames
deltaT = 10:5:240;
kFoldCnt = 10;
aucCompR = zeros(length(deltaT),kFoldCnt);
errorRateR = zeros(length(deltaT),kFoldCnt);
correctRateR = zeros(length(deltaT),kFoldCnt);
numWorkers = 12;
p = gcp('nocreate'); % If no pool, do not create new one. ~14Gb/exp
if isempty(p)
    parpool(numWorkers);
end

parfor dT = 1:length(deltaT)
    %Add to structure
    S = struct();
    S.meanIntR = intMatR(:,1:deltaT);
    S.maxIntR = maxIntMatR(:,1:deltaT);
    S.minIntR = minIntMatR(:,1:deltaT);
    S.stdIntR = minIntMatR(:,1:deltaT);
    nms = fieldnames(S);
    master_data  = []; mastLab=[];
    for j = 1:length(nms)
        [mat1,labs1] = calcTrajFeatures(S.(nms{j}));
        for l=1:length(labs1)
            labs1{l} = [labs1{l} '_' nms{j}];
        end
        [mat2,labs2] = calcTrajFeatures(movmean(S.(nms{j}),ceil(deltaT(dT)),2));
        for l=1:length(labs2)
            labs2{l} = [labs2{l} '_' nms{j} '_movmean_deltaT'];
        end
        [mat3,labs3] = calcTrajFeatures(movmean(S.(nms{j}),ceil(deltaT(dT)/2),2));
        for l=1:length(labs3)
            labs3{l} = [labs3{l} '_' nms{j}  '_movmean_deltaTdiv2'];
        end
        [mat4,labs4] = calcTrajFeatures(movmean(S.(nms{j}),ceil(deltaT(dT)/10),2));
        for l=1:length(labs4)
            labs4{l} = [labs4{l} '_' nms{j}  '_movmean_deltaTdiv10'];
        end
        master_data = [master_data,mat1,mat2,mat3,mat4];
        mastLab = [mastLab,labs1,labs2,labs3,labs4];
    end

    %T = array2table(master_data,'VariableNames',mastLab);
    %Label cells as either Resistant or Sensitive.  Include untreated
    %sensitive as resistant cells!
    master_data = master_data(indx,:);
    tmp_kan_r = kan_r(indx);tmp_drug_conc=drug_conc(indx);
    master_label = ones(length(tmp_kan_r),1);
    %Sensitive cells are zero
    master_label((tmp_kan_r==0)&(tmp_drug_conc==100)) = 0;

    %Normalize data
    master_data = normalize(master_data);
    %Uncomment to examine how class labels separate
%     [~,score,~,~,explained,~] = pca(master_data);
%     figure()
%     hold on
%     scatter(score(master_label==0,1),score(master_label==0,2))
%     scatter(score(master_label==1,1),score(master_label==1,2))
%     
    %% Begin training model
    %Split the data into training and testing
    rng(5)
    disp(['Starting Model Learning, deltaT: ' num2str(deltaT(dT))])
    %Use only 5% of the data for training the model
    spt = rand(length(master_data),1)> 0.8;
    test = master_data(spt,:);
    train = master_data(spt==0,:);
    test_lab = master_label(spt,:);
    train_lab = master_label(spt==0,:);
    %Set up template Tree
    %See https://www.mathworks.com/help/stats/templatetree.html for list of
    %hyperparameters
    t = templateTree('Reproducible',true,'MinLeafSize',2,'MaxNumSplits',963);
    Mdl = fitcensemble(train,train_lab,'Method','AdaBoostM1','NumLearningCycles',25,'LearnRate',0.4605,'CrossVal','on','KFold',kFoldCnt,'NPrint',0)

    for j = 1:kFoldCnt
        [predict_labels,predict_score] = predict(Mdl.Trained{j}, test);
        [~,~,~,auc] = perfcurve(test_lab,predict_score(:,k+1),k);
        cp = classperf(test_lab,predict_labels);
        aucCompR(dT,j) = auc;
        errorRateR(dT,j) = cp.ErrorRate;
        correctRateR(dT,j) = cp.CorrectRate;
    end
end

fig = figure('Color','w','Position',[900 1200 800 200]);
ax1 = subplot(1,3,1);
shadedErrorBar(deltaT,aucComp',{@mean,@std},'lineProps',{'m','markerfacecolor','m','linewidth',2})
shadedErrorBar(deltaT,aucCompR',{@mean,@std},'lineProps',{'r','markerfacecolor','r','linewidth',2})
shadedErrorBar(deltaT,aucCompG',{@mean,@std},'lineProps',{'g','markerfacecolor','g','linewidth',2})
legend('R+G','R','G','location','south')
title('ROC AUC')
xlabel('deltaT')
ylabel('AUC')
set(gca,'Linewidth',2,'FontSize',8,'FontName','Arial')

ax2 = subplot(1,3,2);
shadedErrorBar(deltaT,errorRate'*100.,{@mean,@std},'lineProps',{'m','markerfacecolor','m','linewidth',2})
shadedErrorBar(deltaT,errorRateR'*100.,{@mean,@std},'lineProps',{'r','markerfacecolor','r','linewidth',2})
shadedErrorBar(deltaT,errorRateG'*100.,{@mean,@std},'lineProps',{'g','markerfacecolor','g','linewidth',2})
title('Error Rate')
ylabel('Rate (%)')
xlabel('deltaT')
set(gca,'Linewidth',2,'FontSize',8,'FontName','Arial')

ax3 = subplot(1,3,3);
shadedErrorBar(deltaT,correctRate'*100.,{@mean,@std},'lineProps',{'m','markerfacecolor','m','linewidth',2})
shadedErrorBar(deltaT,correctRateR'*100.,{@mean,@std},'lineProps',{'r','markerfacecolor','r','linewidth',2})
shadedErrorBar(deltaT,correctRateG'*100.,{@mean,@std},'lineProps',{'g','markerfacecolor','g','linewidth',2})
title('Correct Rate')
ylabel('Rate (%)')
xlabel('deltaT')
set(gca,'Linewidth',2,'FontSize',8,'FontName','Arial')

tightfig













