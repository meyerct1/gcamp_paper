function [mat,labs] = calcTrajFeatures(A)
    men = mean(A,2);
    md  = median(A,2);
    sd  = std(A,[],2);
    mx  = max(A,[],2);
    mn  = min(A,[],2);
    men_dt = mean(diff(A,1,2),2);
    md_dt  = median(diff(A,1,2),2);
    sd_dt  = std(diff(A,1,2),[],2);
    mx_dt  = max(diff(A,1,2),[],2);
    mn_dt  = min(diff(A,1,2),[],2);
    men_dtdt = mean(diff(A,2,2),2);
    md_dtdt  = median(diff(A,2,2),2);
    sd_dtdt  = std(diff(A,2,2),[],2);
    mx_dtdt  = max(diff(A,2,2),[],2);
    mn_dtdt  = min(diff(A,2,2),[],2);
    labs = {'Mean','Median','Std','Max','Min',...
            'dtMean','dtMedian','dtStd','dtMax','dtMin',...
            'dtdtMean','dtdtMedian','dtdtStd','dtdtMax','dtdtMin'};
    mat = [men,md,sd,mx,mn,men_dt,md_dt,sd_dt,mx_dt,mn_dt,...
            men_dtdt,md_dtdt,sd_dtdt,mx_dtdt,mn_dtdt];
end