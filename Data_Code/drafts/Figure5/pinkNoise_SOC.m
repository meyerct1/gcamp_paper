%Change into the correct directory
cd ~/Repos/GCAMP_paper/Data_Code/Figure3/

%% Step 1: Read in data generated from the "extract_GMmS_TIF_2color_parallel.m" file
%Clear everything and start a timer
clear; close all; tic
%Set active directory
activeDir      = '/mnt/kralj_celery/E coli GC antibioitic/';
flist          = dir([activeDir '*/*Results/matFiles/*/*data.mat']); %Current file list
nfiles         = length(flist); %Number of files

%Read in all the files to get a number of cells to 
cellCnt = 0;
for i = 1:length(flist)
    load([flist(i).folder filesep flist(i).name])
    if ~isempty(S.cellMeanIntG)
        cellCnt = cellCnt + S.ncells;
    end
end
nframes        = size(S.cellMeanIntG,2);

drug_conc = zeros(cellCnt,1);kan_r=zeros(cellCnt,1);drugNm = cell(cellCnt,1);expNm = cell(cellCnt,1);wellNm = cell(cellCnt,1);  %Information on drug treatment and resistance status
cellArea  = zeros(cellCnt,nframes);    %Change in cell area over time
%Feature matrices on the trajectories for both Red and Green ch
intMatG   = zeros(cellCnt,nframes); minIntMatG = zeros(cellCnt,nframes); maxIntMatG = zeros(cellCnt,nframes); stdIntMatG = zeros(cellCnt,nframes);
intMatR   = zeros(cellCnt,nframes); minIntMatR = zeros(cellCnt,nframes); maxIntMatR = zeros(cellCnt,nframes); stdIntMatR = zeros(cellCnt,nframes);
matbckG = zeros(length(S.time),length(flist));matbckR = zeros(length(S.time),length(flist));

%Feature matrix on the segmentation used to filter cells
segArea   = zeros(cellCnt,1);segPerimeter=zeros(cellCnt,1);segMajAxisLen=zeros(cellCnt,1);segMinAxisLen=zeros(cellCnt,1);segCircularity=zeros(cellCnt,1);
%For each mat file, load and append the data to the appropriate matrix
cellCnt = [1 1];
for i = 1:length(flist)
    disp(['Loading file ' num2str(i)])
    load([flist(i).folder filesep flist(i).name])
    if ~isempty(S.cellMeanIntG)
        cellCnt(2) = cellCnt(1)+S.ncells-1;
        matbckG(:,i)                          = S.bckG;
        matbckR(:,i)                          = S.bckR;        
        intMatG(cellCnt(1):cellCnt(2),:)      = S.cellMeanIntG-S.bckG';
        intMatR(cellCnt(1):cellCnt(2),:)      = S.cellMeanIntR-S.bckR';
        minIntMatG(cellCnt(1):cellCnt(2),:)   = S.cellMinIntG;
        minIntMatR(cellCnt(1):cellCnt(2),:)   = S.cellMinIntR;
        maxIntMatG(cellCnt(1):cellCnt(2),:)   = S.cellMaxIntG;
        maxIntMatR(cellCnt(1):cellCnt(2),:)   = S.cellMaxIntR;
        stdIntMatG(cellCnt(1):cellCnt(2),:)   = S.cellStdIntG;
        stdIntMatR(cellCnt(1):cellCnt(2),:)   = S.cellStdIntR;
        cellArea(cellCnt(1):cellCnt(2),:)     = S.cellArea;
        drug_conc(cellCnt(1):cellCnt(2))      = ones(S.ncells,1)*S.currConc;
        kan_r(cellCnt(1):cellCnt(2))          = ones(S.ncells,1)*contains(S.currCell,'KanR');
        tmp = split(flist(i).folder,filesep);
        tmp1 = tmp{end};
        tmp2 = tmp{end-3};
        for j = cellCnt(1):cellCnt(2)
            drugNm{j}                         = S.currDrug;
            wellNm{j}                         = tmp1;
            expNm{j}                          = tmp2;
        end
        segArea(cellCnt(1):cellCnt(2))        = S.segArea;
        segMajAxisLen(cellCnt(1):cellCnt(2))  = S.segMajAxisLen;
        segMinAxisLen(cellCnt(1):cellCnt(2))  = S.segMinAxisLen;
        segCircularity(cellCnt(1):cellCnt(2)) = S.segCircularity;
        segPerimeter(cellCnt(1):cellCnt(2))   = S.segPerimeter;
        cellCnt(1)                            = cellCnt(2)+1;
    end
end
%Deal with the strange error on circularity being infinite
segCircularity(abs(segCircularity)==Inf)=0;


%% Step 2:  "Gate" the cells based on different requirements
%Remove cells which vanished from segmentation (matArea==0)
indx    = find(sum(cellArea'==0)>0);
%Fillmissing values if possible using movmedian over 15 frames
intMatG = fillmissing(intMatG','movmean',15)';
intMatR = fillmissing(intMatR','movmean',15)';
%If still undefined, remove 
indx    = union(indx,find(sum(isnan(intMatG'))>0));
indx    = union(indx,find(sum(isnan(intMatR'))>0));
%Remove cells which have segmentation parameter too different
dat = [segArea,segMajAxisLen,segMinAxisLen,segCircularity,segPerimeter];
dat = normalize(dat);
[~,score,~,~,~,~] = pca(dat); %Run PCA to visualize what cells are being discarded
dist_mat = mean(squareform(pdist(score)));
data_keep = .98;  %What percentage to keep?  #############################
tmp = sort(dist_mat);
indx  = union(indx,find(dist_mat>tmp(floor(length(dist_mat)*data_keep))));
%Visualize cells
fig = figure();
hold on
scatter(score(:,1),score(:,2)); scatter(score(indx,1),score(indx,2))
legend('keep','remove')
xlabel('PCA1');ylabel('PCA2');
saveas(fig,'MultiDim_gating')

%Which indices to keep
to_keep    = setdiff(1:length(dist_mat),indx);
intMatG    = intMatG(to_keep,:);
intMatR    = intMatR(to_keep,:);
minIntMatG = minIntMatG(to_keep,:);
minIntMatR = minIntMatR(to_keep,:);
maxIntMatG = maxIntMatG(to_keep,:);
maxIntMatR = maxIntMatR(to_keep,:);
stdIntMatG = stdIntMatG(to_keep,:);
stdIntMatR = stdIntMatR(to_keep,:);
cellArea   = cellArea(to_keep,:);
drug_conc  = drug_conc(to_keep);
kan_r      = kan_r(to_keep);
drugNm     = drugNm(to_keep);
wellNm     = wellNm(to_keep);
expNm      = expNm(to_keep);



indx2 = find((kan_r==0)&contains(drugNm,'kan')&(drug_conc==100));
%Figure 1 pink noise in Kan blinks
fig = figure('Color','w','Position',[100 100 350 350])
ax1 = subplot(1,1,1); hold on
mat = intMatG(indx2,120:end);
Y = fft(mat')'; L = size(mat,2);
P2 = abs(Y/L); P1 = P2(:,1:L/2+1);
%Assume imaging frequency of 60 seconds
f = (1/60)*(0:(L/2))/L;
x = log10(f(2:end));
y = mean(P1);y=log10(y(2:end));
plot(ax1,x,y,'linewidth',2)
ft = fitlm(x,y');
plot(ax1,x,predict(ft,x'),'linewidth',2)
legend({'Data','Fit'},'FontSize',10)
xlabel('Ca transients frequency (Hz)','FontSize',10)
ylabel('|P(f)|','FontSize',10)
title({'log-log Power spectrum','of Ca2+ transients (120min:240min)'},'FontSize',10)
m = ft.Coefficients.Estimate(2);
text(-3.75,.3,['P(f)~1/f^{ ' num2str(-m,2) '}'],'FontSize',10)
tightfig
saveas(fig,'fig1.png')



fig = figure('Color','w','Position',[100 100 750 300])
ax1 = subplot(1,2,1); hold on
ax2 = subplot(1,2,2); hold on
stTime = 30:30:180;
for i = stTime
    mat = intMatG(indx2,i:end);
    Y = fft(mat')';
    L = size(mat,2);
    P2 = abs(Y/L);
    P1 = P2(:,1:L/2+1);
    f = (1/60)*(0:(L/2))/L;
    x = f(2:end);
    y = mean(P1);y=y(2:end);
    plot(ax1,x,y,'linewidth',2)
    x = log10(x);
    y = log10(y);
    ft = fitlm(x,y);
    scatter(ax2,i,-ft.Coefficients.Estimate(2),50,'filled')
end
axes(ax1)
set(gca, 'YScale', 'log')
set(gca, 'XScale', 'log')
title({'Power spectrum of Ca transients','for different time periods'},'FontSize',10)
ylabel('|P1(f)|','FontSize',10)
xlabel('f (Hz)','FontSize',10)
leg = {};
for i = 1:length(stTime)
    leg{i} = [num2str(stTime(i)) 'min:240min'];
end
legend(leg,'FontSize',8)
axes(ax2)
title('\alpha for S(f)~1/f^{\alpha}','FontSize',10)
xlabel('Start Time (min)','FontSize',10)
ylabel('\alpha','FontSize',10)
tightfig
saveas(fig,'fig2.png')





fig = figure('Color','w','Position',[100 100 750 300])
ax1 = subplot(1,2,1); hold on
ax2 = subplot(1,2,2); hold on
stTime = [1 30:30:210];
for i = stTime
    mat = intMatG(indx2,i:i+30);
    Y = fft(mat')';
    L = size(mat,2);
    P2 = abs(Y/L);
    P1 = P2(:,1:L/2+1);
    f = (1/60)*(0:(L/2))/L;
    x = f(2:end);
    y = mean(P1);y=y(2:end);
    plot(ax1,x,y,'linewidth',2)
    x = log10(x);
    y = log10(y);
    ft = fitlm(x,y);
    scatter(ax2,i,-ft.Coefficients.Estimate(2),50,'filled')
end
axes(ax1)
set(gca, 'YScale', 'log')
set(gca, 'XScale', 'log')
title({'Power spectrum of Ca transients','for different time periods'},'FontSize',10)
ylabel('|P1(f)|','FontSize',10)
xlabel('f (Hz)','FontSize',10)
leg = {};
for i = 1:length(stTime)
    leg{i} = [num2str(stTime(i)) 'min:' num2str(stTime(i)+30) 'min'];
end
legend(leg,'FontSize',8)
axes(ax2)
title('\alpha for S(f)~1/f^{\alpha}','FontSize',10)
xlabel('Start Time (min)','FontSize',10)
ylabel('\alpha','FontSize',10)
tightfig
saveas(fig,'fig3.png')



fig = figure('Color','w','Position',[100 100 350 250])
hold on
mat = intMatG(indx2,120:end);
x = [];y=[];nn = 1;
for i = 1:size(mat,1)
    tmp = sign(diff(mat(i,:)));
    tmp(tmp==0)=1;pd = {};
    p1 = 1;cntr=1;indx_cnt = 1;
    for j = 2:size(tmp')
        if tmp(j)==tmp(j-1) && cntr <= 2
            continue
        elseif tmp(j)==-tmp(j-1) && cntr <= 2
            cntr = cntr + 1;
        elseif cntr>2
            pd{indx_cnt} = [p1 j-1];
            p1 = j-1;
            cntr = 1;
            indx_cnt = indx_cnt+1;
        else
            error('Whoops')
        end
    end
    for j=1:length(pd)
        x(nn) = diff(pd{j});
        y(nn) = max(mat(i,pd{j}(1):pd{j}(2)))-min(mat(i,pd{j}(1):pd{j}(2)));
        nn = nn + 1;
    end
end
[C,~,ic] = unique(x);x_counts = accumarray(ic,1);
lim = max(C(x_counts>3));
y = y(x<lim);
x = x(x<lim);
hold on;
cnt = 1;
tmp_x=zeros(length(C),1);
tmp_y=zeros(length(C),1);
for i = C
    tmp_x(cnt) = log10(1./i);
    tmp_y(cnt) = log10(mean(y(x==i)));
    cnt = cnt + 1;
end
scatter(tmp_x,tmp_y,'b','filled')
title({'Evidence of Self-Organized Criticality'},'FontSize',10)
ylabel('log(transient amplitude)','FontSize',10)
xlabel('log(transient frequency)','FontSize',10)
ft = fitlm(tmp_x,tmp_y);
plot(tmp_x,predict(ft,tmp_x),'linewidth',2)
TextLocation({['y=' num2str(ft.Coefficients.Estimate(2),2) 'x+' num2str(ft.Coefficients.Estimate(1),2)],['R^2=' num2str(ft.Rsquared.Adjusted,2)]},'Location','Best')
legend({'Data','Fit'},'location','northeast')
xlim([-1.1,-.3])
tightfig
saveas(fig,'fig4.png')





fig = figure('Color','w','Position',[100 100 333 125])
hold on
mat = intMatG(indx2,120:end);
x = [];y=[];nn = 1;
i = 1;

tmp = sign(diff(mat(i,:)));
tmp(tmp==0)=1;pd = {};
p1 = 1;cntr=1;indx_cnt = 1;
for j = 2:size(tmp')
    if tmp(j)==tmp(j-1) && cntr <= 2
        continue
    elseif tmp(j)==-tmp(j-1) && cntr <= 2
        cntr = cntr + 1;
    elseif cntr>2
        pd{indx_cnt} = [p1 j-1];
        p1 = j-1;
        cntr = 1;
        indx_cnt = indx_cnt+1;
    else
        error('Whoops')
    end
end
plot(mat(i,:),'g','linewidth',2)
for j=1:length(pd)
    xline(pd{j}(1),'color',[0.5 0 0.5],'linewidth',2)
    xline(pd{j}(2),'color',[0.5 0 0.5],'linewidth',2)
end
xlabel('time (min)','FontSize',10)
ylabel('GCAMP','FontSize',10)
xlim([10,30])
yticks([])
tightfig
saveas(fig,'fig5.png')







indx2 = find((kan_r==0)&(drug_conc==0));
%Figure 1 pink noise in Kan blinks
fig = figure('Color','w','Position',[100 100 350 350])
ax1 = subplot(1,1,1); hold on
mat = intMatG(indx2,1:end);
Y = fft(mat')'; L = size(mat,2);
P2 = abs(Y/L); P1 = P2(:,1:L/2+1);
%Assume imaging frequency of 60 seconds
f = (1/60)*(0:(L/2))/L;
x = log10(f(2:end));
y = mean(P1);y=log10(y(2:end));
plot(ax1,x,y,'linewidth',2)
ft = fitlm(x,y');
plot(ax1,x,predict(ft,x'),'linewidth',2)
legend({'Data','Fit'},'FontSize',10)
xlabel('Ca transients frequency (Hz)','FontSize',10)
ylabel('|P(f)|','FontSize',10)
title({'log-log Power spectrum of Ca2+','transients untreated (0min:240min)'},'FontSize',10)
m = ft.Coefficients.Estimate(2);
text(-4,0,['P(f)~1/f^{ ' num2str(-m,2) '}'],'FontSize',10)
tightfig
saveas(fig,'fig6.png')




fig = figure('Color','w','Position',[100 100 350 250])
hold on
mat = intMatG(indx2,1:end);
x = [];y=[];nn = 1;
for i = 1:size(mat,1)
    tmp = sign(diff(mat(i,:)));
    tmp(tmp==0)=1;pd = {};
    p1 = 1;cntr=1;indx_cnt = 1;
    for j = 2:size(tmp')
        if tmp(j)==tmp(j-1) && cntr <= 2
            continue
        elseif tmp(j)==-tmp(j-1) && cntr <= 2
            cntr = cntr + 1;
        elseif cntr>2
            pd{indx_cnt} = [p1 j-1];
            p1 = j-1;
            cntr = 1;
            indx_cnt = indx_cnt+1;
        else
            error('Whoops')
        end
    end
    for j=1:length(pd)
        x(nn) = diff(pd{j});
        y(nn) = max(mat(i,pd{j}(1):pd{j}(2)))-min(mat(i,pd{j}(1):pd{j}(2)));
        nn = nn + 1;
    end
end
[C,~,ic] = unique(x);x_counts = accumarray(ic,1);
lim = max(C(x_counts>3));
y = y(x<lim);
x = x(x<lim);
hold on;
cnt = 1;
tmp_x=zeros(length(C),1);
tmp_y=zeros(length(C),1);
for i = C
    tmp_x(cnt) = log10(1./i);
    tmp_y(cnt) = log10(mean(y(x==i)));
    cnt = cnt + 1;
end
scatter(tmp_x,tmp_y,'b','filled')
title({'Evidence of Self-Organized Criticality'},'FontSize',10)
ylabel('log(transient amplitude)','FontSize',10)
xlabel('log(transient frequency)','FontSize',10)
ft = fitlm(tmp_x,tmp_y);
plot(tmp_x,predict(ft,tmp_x),'linewidth',2)
TextLocation({['y=' num2str(ft.Coefficients.Estimate(2),2) 'x+' num2str(ft.Coefficients.Estimate(1),2)],['R^2=' num2str(ft.Rsquared.Adjusted,2)]},'Location','Best')
legend({'Data','Fit'},'location','northeast')
xlim([-1.3,-.25])
tightfig
saveas(fig,'fig7.png')




%%
indx2 = find((kan_r==0)&contains(drugNm,'kan')&(drug_conc==100));
stTime = [1 30:30:210];ft1=[];leg=[];
for st = 1:length(stTime)
    hold on
    mat = intMatG(indx2,stTime(st):stTime(st)+30);
    x = [];y=[];nn = 1;
    for i = 1:size(mat,1)
        tmp = sign(diff(mat(i,:)));
        tmp(tmp==0)=1;pd = {};
        p1 = 1;cntr=1;indx_cnt = 1;
        for j = 2:size(tmp')
            if tmp(j)==tmp(j-1) && cntr <= 2
                continue
            elseif tmp(j)==-tmp(j-1) && cntr <= 2
                cntr = cntr + 1;
            elseif cntr>2
                pd{indx_cnt} = [p1 j-1];
                p1 = j-1;
                cntr = 1;
                indx_cnt = indx_cnt+1;
            else
                error('Whoops')
            end
        end
        for j=1:length(pd)
            x(nn) = diff(pd{j});
            y(nn) = max(mat(i,pd{j}(1):pd{j}(2)))-min(mat(i,pd{j}(1):pd{j}(2)));
            nn = nn + 1;
        end
    end
    [C,~,ic] = unique(x);x_counts = accumarray(ic,1);
    lim = max(C(x_counts>3));
    y = y(x<lim);    x = x(x<lim);
    hold on;
    cnt = 1;
    tmp_x=zeros(length(C),1);    tmp_y=zeros(length(C),1);
    for i = C
        tmp_x(cnt) = log10(1./i);
        tmp_y(cnt) = log10(mean(y(x==i)));
        cnt = cnt + 1;
    end

    ft1{st} = fitlm(tmp_y,tmp_x);   
    leg{st} = [num2str(stTime(st)) 'min:' num2str(stTime(st)+30) 'min'];
end
indx2 = find((kan_r==0)&(drug_conc==0));
ft2 = [];
for st = 1:length(stTime)
    hold on
    mat = intMatG(indx2,stTime(st):stTime(st)+30);
    x = [];y=[];nn = 1;
    for i = 1:size(mat,1)
        tmp = sign(diff(mat(i,:)));
        tmp(tmp==0)=1;pd = {};
        p1 = 1;cntr=1;indx_cnt = 1;
        for j = 2:size(tmp')
            if tmp(j)==tmp(j-1) && cntr <= 2
                continue
            elseif tmp(j)==-tmp(j-1) && cntr <= 2
                cntr = cntr + 1;
            elseif cntr>2
                pd{indx_cnt} = [p1 j-1];
                p1 = j-1;
                cntr = 1;
                indx_cnt = indx_cnt+1;
            else
                error('Whoops')
            end
        end
        for j=1:length(pd)
            x(nn) = diff(pd{j});
            y(nn) = max(mat(i,pd{j}(1):pd{j}(2)))-min(mat(i,pd{j}(1):pd{j}(2)));
            nn = nn + 1;
        end
    end
    [C,~,ic] = unique(x);x_counts = accumarray(ic,1);
    lim = max(C(x_counts>3));
    y = y(x<lim);    x = x(x<lim);
    hold on;
    cnt = 1;
    tmp_x=zeros(length(C),1);    tmp_y=zeros(length(C),1);
    for i = C
        tmp_x(cnt) = log10(1./i);
        tmp_y(cnt) = log10(mean(y(x==i)));
        cnt = cnt + 1;
    end

    ft2{st} = fitlm(tmp_y,tmp_x);   
end
fig = figure('Color','w','Position',[100 100 300 300]);
x = [];
for i = 1:length(ft1)
    x(i) = i;
    y1(i) = ft1{i}.Coefficients.Estimate(2);
    y2(i) = ft2{i}.Coefficients.Estimate(2);
end
hold on
plot(x,y1,'linewidth',2,'color','r')
plot(x,y2,'linewidth',2,'color','b')
xticks(x)
xticklabels(leg)
xtickangle(45)
xlim([0.5 length(ft1)+.5])
ylabel('Slope','FontSize',10)
legend('Kan Untreated','Kan Treated','FontSize',10,'Location','SouthEast')
tightfig()
saveas(fig,'fig8.png')










ft2 = [];leg = [];
%Loop through the different drugs
drugs = {'cipro','chlor','trim','PmB','kan',{'kan'}};
maxConc = [10,100,10,3,100,0];
for st = 1:length(drugs)
    if iscell(drugs{st})
        indx2 = find((kan_r==0)&(drug_conc==maxConc(st)));
        drugs{st}='all'
    else
        indx2 = find((kan_r==0)&contains(drugNm,drugs{st})&(drug_conc==maxConc(st)));
    end
    mat = intMatG(indx2,120:240);
    x = [];y=[];nn = 1;
    for i = 1:size(mat,1)
        tmp = sign(diff(mat(i,:)));
        tmp(tmp==0)=1;pd = {};
        p1 = 1;cntr=1;indx_cnt = 1;
        for j = 2:size(tmp')
            if tmp(j)==tmp(j-1) && cntr <= 2
                continue
            elseif tmp(j)==-tmp(j-1) && cntr <= 2
                cntr = cntr + 1;
            elseif cntr>2
                pd{indx_cnt} = [p1 j-1];
                p1 = j-1;
                cntr = 1;
                indx_cnt = indx_cnt+1;
            else
                error('Whoops')
            end
        end
        for j=1:length(pd)
            x(nn) = diff(pd{j});
            y(nn) = max(mat(i,pd{j}(1):pd{j}(2)))-min(mat(i,pd{j}(1):pd{j}(2)));
            nn = nn + 1;
        end
    end
    fig = figure('Color','w','Position',[100 100 300 300]);
    [C,~,ic] = unique(x);x_counts = accumarray(ic,1);
    lim = max(C(x_counts>3));
    y = y(x<lim);
    x = x(x<lim);
    hold on;
    cnt = 1;
    tmp_x=zeros(length(C),1);
    tmp_y=zeros(length(C),1);
    for i = C
        tmp_x(cnt) = log10(1./i);
        tmp_y(cnt) = log10(mean(y(x==i)));
        cnt = cnt + 1;
    end
    scatter(tmp_x,tmp_y,'b','filled')
    title({['SOC ' drugs{st} ' [' num2str(maxConc(st)) ']']},'FontSize',10)
    ylabel('log(transient amplitude)','FontSize',10)
    xlabel('log(transient frequency)','FontSize',10)
    ft2{st} = fitlm(tmp_x,tmp_y);
    plot(tmp_x,predict(ft2{st},tmp_x),'linewidth',2)
    TextLocation({['y=' num2str(ft2{st}.Coefficients.Estimate(2),2) 'x+' num2str(ft2{st}.Coefficients.Estimate(1),2)],['R^2=' num2str(ft2{st}.Rsquared.Adjusted,2)]},'Location','best')
    legend({'Data','Fit'},'location','northeast')
    tightfig
    leg{st} = [drugs{st} ' [' num2str(maxConc(st)) '] (' num2str(size(mat,1)) ')'];
    saveas(fig,['fig9_' num2str(st) '.png'])
end
fig = figure('Color','w','Position',[100 100 300 300]);
x = [];y2 = [];
for i = 1:length(ft2)
    x(i) = i;
    y2(i) = ft2{i}.Coefficients.Estimate(2);
end
hold on
bar(x,y2)
xticks(x)
xticklabels(leg)
xtickangle(45)
xlim([0.5 length(ft2)+.5])
ylabel('Slope','FontSize',10)
tightfig()
saveas(fig,'fig10.png')






%%

%Figure 1 pink noise in Kan blinks
fig = figure('Color','w','Position',[100 100 850 350])
ax1 = subplot(1,2,1); hold on
mat = matbckG';
Y = fft(mat')'; L = size(mat,2);
P2 = abs(Y/L); P1 = P2(:,1:L/2+1);
%Assume imaging frequency of 60 seconds
f = (1/60)*(0:(L/2))/L;
x = log10(f(2:end));
y = mean(P1);y=log10(y(2:end));
plot(ax1,x,y,'linewidth',2)
ft = fitlm(x,y');
plot(ax1,x,predict(ft,x'),'linewidth',2)
legend({'Data','Fit'},'FontSize',10)
xlabel('Ca transients frequency (Hz)','FontSize',10)
ylabel('|P(f)|','FontSize',10)
title({'log-log Power spectrum background GCAMP'},'FontSize',10)
m = ft.Coefficients.Estimate(2);
TextLocation(['P(f)~1/f^{ ' num2str(-m,2) '}'],'Location','best')

ax2 = subplot(1,2,2); hold on
mat = matbckR';
Y = fft(mat')'; L = size(mat,2);
P2 = abs(Y/L); P1 = P2(:,1:L/2+1);
%Assume imaging frequency of 60 seconds
f = (1/60)*(0:(L/2))/L;
x = log10(f(2:end));
y = mean(P1);y=log10(y(2:end));
plot(ax2,x,y,'linewidth',2)
ft = fitlm(x,y');
plot(ax2,x,predict(ft,x'),'linewidth',2)
legend({'Data','Fit'},'FontSize',10)
xlabel('Ca transients frequency (Hz)','FontSize',10)
ylabel('|P(f)|','FontSize',10)
title({'log-log Power spectrum background mScarlet'},'FontSize',10)
m = ft.Coefficients.Estimate(2);
TextLocation(['P(f)~1/f^{ ' num2str(-m,2) '}'],'Location','best')
tightfig
saveas(fig,'fig11.png')





fig = figure('Color','w','Position',[100 100 750 250]);
hold on
subplot(1,2,1)
mat = matbckG';
x = [];y=[];nn = 1;
for i = 1:size(mat,1)
    tmp = sign(diff(mat(i,:)));
    tmp(tmp==0)=1;pd = {};
    p1 = 1;cntr=1;indx_cnt = 1;
    for j = 2:size(tmp')
        if tmp(j)==tmp(j-1) && cntr <= 2
            continue
        elseif tmp(j)==-tmp(j-1) && cntr <= 2
            cntr = cntr + 1;
        elseif cntr>2
            pd{indx_cnt} = [p1 j-1];
            p1 = j-1;
            cntr = 1;
            indx_cnt = indx_cnt+1;
        else
            error('Whoops')
        end
    end
    for j=1:length(pd)
        x(nn) = diff(pd{j});
        y(nn) = max(mat(i,pd{j}(1):pd{j}(2)))-min(mat(i,pd{j}(1):pd{j}(2)));
        nn = nn + 1;
    end
end
[C,~,ic] = unique(x);x_counts = accumarray(ic,1);
lim = max(C(x_counts>3));
y = y(x<lim);
x = x(x<lim);
hold on;
cnt = 1;
tmp_x=zeros(length(C),1);
tmp_y=zeros(length(C),1);
for i = C
    tmp_x(cnt) = log10(1./i);
    tmp_y(cnt) = log10(mean(y(x==i)));
    cnt = cnt + 1;
end
scatter(tmp_x,tmp_y,'b','filled')
title({'GCAMP'},'FontSize',10)
ylabel('log(transient amplitude)','FontSize',10)
xlabel('log(transient frequency)','FontSize',10)
ft = fitlm(tmp_x,tmp_y);
plot(tmp_x,predict(ft,tmp_x),'linewidth',2)
TextLocation({['y=' num2str(ft.Coefficients.Estimate(2),2) 'x+' num2str(ft.Coefficients.Estimate(1),2)],['R^2=' num2str(ft.Rsquared.Adjusted,2)]},'Location','Best')
legend({'Data','Fit'},'location','southwest')

subplot(1,2,2)
mat = matbckR';
x = [];y=[];nn = 1;
for i = 1:size(mat,1)
    tmp = sign(diff(mat(i,:)));
    tmp(tmp==0)=1;pd = {};
    p1 = 1;cntr=1;indx_cnt = 1;
    for j = 2:size(tmp')
        if tmp(j)==tmp(j-1) && cntr <= 2
            continue
        elseif tmp(j)==-tmp(j-1) && cntr <= 2
            cntr = cntr + 1;
        elseif cntr>2
            pd{indx_cnt} = [p1 j-1];
            p1 = j-1;
            cntr = 1;
            indx_cnt = indx_cnt+1;
        else
            error('Whoops')
        end
    end
    for j=1:length(pd)
        x(nn) = diff(pd{j});
        y(nn) = max(mat(i,pd{j}(1):pd{j}(2)))-min(mat(i,pd{j}(1):pd{j}(2)));
        nn = nn + 1;
    end
end
[C,~,ic] = unique(x);x_counts = accumarray(ic,1);
lim = max(C(x_counts>3));
y = y(x<lim);
x = x(x<lim);
hold on;
cnt = 1;
tmp_x=zeros(length(C),1);
tmp_y=zeros(length(C),1);
for i = C
    tmp_x(cnt) = log10(1./i);
    tmp_y(cnt) = log10(mean(y(x==i)));
    cnt = cnt + 1;
end
scatter(tmp_x,tmp_y,'b','filled')
title({'mScarlet'},'FontSize',10)
ylabel('log(transient amplitude)','FontSize',10)
xlabel('log(transient frequency)','FontSize',10)
ft = fitlm(tmp_x,tmp_y);
plot(tmp_x,predict(ft,tmp_x),'linewidth',2)
TextLocation({['y=' num2str(ft.Coefficients.Estimate(2),2) 'x+' num2str(ft.Coefficients.Estimate(1),2)],['R^2=' num2str(ft.Rsquared.Adjusted,2)]},'Location','Best')
legend({'Data','Fit'},'location','southwest')
tightfig
saveas(fig,'fig12.png')












%%

cd('scarlet')
indx2 = find((kan_r==0)&contains(drugNm,'kan')&(drug_conc==100));
%Figure 1 pink noise in Kan blinks
fig = figure('Color','w','Position',[100 100 350 350])
ax1 = subplot(1,1,1); hold on
mat = intMatR(indx2,120:end);
Y = fft(mat')'; L = size(mat,2);
P2 = abs(Y/L); P1 = P2(:,1:L/2+1);
%Assume imaging frequency of 60 seconds
f = (1/60)*(0:(L/2))/L;
x = log10(f(2:end));
y = mean(P1);y=log10(y(2:end));
plot(ax1,x,y,'linewidth',2)
ft = fitlm(x,y');
plot(ax1,x,predict(ft,x'),'linewidth',2)
legend({'Data','Fit'},'FontSize',10)
xlabel('Ca transients frequency (Hz)','FontSize',10)
ylabel('|P(f)|','FontSize',10)
title({'log-log Power spectrum','of Ca2+ transients (120min:240min)'},'FontSize',10)
m = ft.Coefficients.Estimate(2);
text(-3.75,.3,['P(f)~1/f^{ ' num2str(-m,2) '}'],'FontSize',10)
tightfig
saveas(fig,'fig1.png')



fig = figure('Color','w','Position',[100 100 750 300])
ax1 = subplot(1,2,1); hold on
ax2 = subplot(1,2,2); hold on
stTime = 30:30:180;
for i = stTime
    mat = intMatR(indx2,i:end);
    Y = fft(mat')';
    L = size(mat,2);
    P2 = abs(Y/L);
    P1 = P2(:,1:L/2+1);
    f = (1/60)*(0:(L/2))/L;
    x = f(2:end);
    y = mean(P1);y=y(2:end);
    plot(ax1,x,y,'linewidth',2)
    x = log10(x);
    y = log10(y);
    ft = fitlm(x,y);
    scatter(ax2,i,-ft.Coefficients.Estimate(2),50,'filled')
end
axes(ax1)
set(gca, 'YScale', 'log')
set(gca, 'XScale', 'log')
title({'Power spectrum of Ca transients','for different time periods'},'FontSize',10)
ylabel('|P1(f)|','FontSize',10)
xlabel('f (Hz)','FontSize',10)
leg = {};
for i = 1:length(stTime)
    leg{i} = [num2str(stTime(i)) 'min:240min'];
end
legend(leg,'FontSize',8)
axes(ax2)
title('\alpha for S(f)~1/f^{\alpha}','FontSize',10)
xlabel('Start Time (min)','FontSize',10)
ylabel('\alpha','FontSize',10)
tightfig
saveas(fig,'fig2.png')





fig = figure('Color','w','Position',[100 100 750 300])
ax1 = subplot(1,2,1); hold on
ax2 = subplot(1,2,2); hold on
stTime = [1 30:30:210];
for i = stTime
    mat = intMatR(indx2,i:i+30);
    Y = fft(mat')';
    L = size(mat,2);
    P2 = abs(Y/L);
    P1 = P2(:,1:L/2+1);
    f = (1/60)*(0:(L/2))/L;
    x = f(2:end);
    y = mean(P1);y=y(2:end);
    plot(ax1,x,y,'linewidth',2)
    x = log10(x);
    y = log10(y);
    ft = fitlm(x,y);
    scatter(ax2,i,-ft.Coefficients.Estimate(2),50,'filled')
end
axes(ax1)
set(gca, 'YScale', 'log')
set(gca, 'XScale', 'log')
title({'Power spectrum of Ca transients','for different time periods'},'FontSize',10)
ylabel('|P1(f)|','FontSize',10)
xlabel('f (Hz)','FontSize',10)
leg = {};
for i = 1:length(stTime)
    leg{i} = [num2str(stTime(i)) 'min:' num2str(stTime(i)+30) 'min'];
end
legend(leg,'FontSize',8)
axes(ax2)
title('\alpha for S(f)~1/f^{\alpha}','FontSize',10)
xlabel('Start Time (min)','FontSize',10)
ylabel('\alpha','FontSize',10)
tightfig
saveas(fig,'fig3.png')



fig = figure('Color','w','Position',[100 100 350 250])
hold on
mat = intMatR(indx2,120:end);
x = [];y=[];nn = 1;
for i = 1:size(mat,1)
    tmp = sign(diff(mat(i,:)));
    tmp(tmp==0)=1;pd = {};
    p1 = 1;cntr=1;indx_cnt = 1;
    for j = 2:size(tmp')
        if tmp(j)==tmp(j-1) && cntr <= 2
            continue
        elseif tmp(j)==-tmp(j-1) && cntr <= 2
            cntr = cntr + 1;
        elseif cntr>2
            pd{indx_cnt} = [p1 j-1];
            p1 = j-1;
            cntr = 1;
            indx_cnt = indx_cnt+1;
        else
            error('Whoops')
        end
    end
    for j=1:length(pd)
        x(nn) = diff(pd{j});
        y(nn) = max(mat(i,pd{j}(1):pd{j}(2)))-min(mat(i,pd{j}(1):pd{j}(2)));
        nn = nn + 1;
    end
end
[C,~,ic] = unique(x);x_counts = accumarray(ic,1);
lim = max(C(x_counts>3));
y = y(x<lim);
x = x(x<lim);
hold on;
cnt = 1;
tmp_x=zeros(length(C),1);
tmp_y=zeros(length(C),1);
for i = C
    tmp_x(cnt) = log10(1./i);
    tmp_y(cnt) = log10(mean(y(x==i)));
    cnt = cnt + 1;
end
scatter(tmp_x,tmp_y,'b','filled')
title({'Evidence of Self-Organized Criticality'},'FontSize',10)
ylabel('log(transient amplitude)','FontSize',10)
xlabel('log(transient frequency)','FontSize',10)
ft = fitlm(tmp_x,tmp_y);
plot(tmp_x,predict(ft,tmp_x),'linewidth',2)
TextLocation({['y=' num2str(ft.Coefficients.Estimate(2),2) 'x+' num2str(ft.Coefficients.Estimate(1),2)],['R^2=' num2str(ft.Rsquared.Adjusted,2)]},'Location','Best')
legend({'Data','Fit'},'location','northeast')
xlim([-1.1,-.3])
tightfig
saveas(fig,'fig4.png')





fig = figure('Color','w','Position',[100 100 333 125])
hold on
mat = intMatR(indx2,120:end);
x = [];y=[];nn = 1;
i = 1;

tmp = sign(diff(mat(i,:)));
tmp(tmp==0)=1;pd = {};
p1 = 1;cntr=1;indx_cnt = 1;
for j = 2:size(tmp')
    if tmp(j)==tmp(j-1) && cntr <= 2
        continue
    elseif tmp(j)==-tmp(j-1) && cntr <= 2
        cntr = cntr + 1;
    elseif cntr>2
        pd{indx_cnt} = [p1 j-1];
        p1 = j-1;
        cntr = 1;
        indx_cnt = indx_cnt+1;
    else
        error('Whoops')
    end
end
plot(mat(i,:),'g','linewidth',2)
for j=1:length(pd)
    xline(pd{j}(1),'color',[0.5 0 0.5],'linewidth',2)
    xline(pd{j}(2),'color',[0.5 0 0.5],'linewidth',2)
end
xlabel('time (min)','FontSize',10)
ylabel('GCAMP','FontSize',10)
xlim([10,30])
yticks([])
tightfig
saveas(fig,'fig5.png')







indx2 = find((kan_r==0)&(drug_conc==0));
%Figure 1 pink noise in Kan blinks
fig = figure('Color','w','Position',[100 100 350 350])
ax1 = subplot(1,1,1); hold on
mat = intMatR(indx2,1:end);
Y = fft(mat')'; L = size(mat,2);
P2 = abs(Y/L); P1 = P2(:,1:L/2+1);
%Assume imaging frequency of 60 seconds
f = (1/60)*(0:(L/2))/L;
x = log10(f(2:end));
y = mean(P1);y=log10(y(2:end));
plot(ax1,x,y,'linewidth',2)
ft = fitlm(x,y');
plot(ax1,x,predict(ft,x'),'linewidth',2)
legend({'Data','Fit'},'FontSize',10)
xlabel('Ca transients frequency (Hz)','FontSize',10)
ylabel('|P(f)|','FontSize',10)
title({'log-log Power spectrum of Ca2+','transients untreated (0min:240min)'},'FontSize',10)
m = ft.Coefficients.Estimate(2);
text(-4,0,['P(f)~1/f^{ ' num2str(-m,2) '}'],'FontSize',10)
tightfig
saveas(fig,'fig6.png')




fig = figure('Color','w','Position',[100 100 350 250])
hold on
mat = intMatR(indx2,1:end);
x = [];y=[];nn = 1;
for i = 1:size(mat,1)
    tmp = sign(diff(mat(i,:)));
    tmp(tmp==0)=1;pd = {};
    p1 = 1;cntr=1;indx_cnt = 1;
    for j = 2:size(tmp')
        if tmp(j)==tmp(j-1) && cntr <= 2
            continue
        elseif tmp(j)==-tmp(j-1) && cntr <= 2
            cntr = cntr + 1;
        elseif cntr>2
            pd{indx_cnt} = [p1 j-1];
            p1 = j-1;
            cntr = 1;
            indx_cnt = indx_cnt+1;
        else
            error('Whoops')
        end
    end
    for j=1:length(pd)
        x(nn) = diff(pd{j});
        y(nn) = max(mat(i,pd{j}(1):pd{j}(2)))-min(mat(i,pd{j}(1):pd{j}(2)));
        nn = nn + 1;
    end
end
[C,~,ic] = unique(x);x_counts = accumarray(ic,1);
lim = max(C(x_counts>3));
y = y(x<lim);
x = x(x<lim);
hold on;
cnt = 1;
tmp_x=zeros(length(C),1);
tmp_y=zeros(length(C),1);
for i = C
    tmp_x(cnt) = log10(1./i);
    tmp_y(cnt) = log10(mean(y(x==i)));
    cnt = cnt + 1;
end
scatter(tmp_x,tmp_y,'b','filled')
title({'Evidence of Self-Organized Criticality'},'FontSize',10)
ylabel('log(transient amplitude)','FontSize',10)
xlabel('log(transient frequency)','FontSize',10)
ft = fitlm(tmp_x,tmp_y);
plot(tmp_x,predict(ft,tmp_x),'linewidth',2)
TextLocation({['y=' num2str(ft.Coefficients.Estimate(2),2) 'x+' num2str(ft.Coefficients.Estimate(1),2)],['R^2=' num2str(ft.Rsquared.Adjusted,2)]},'Location','Best')
legend({'Data','Fit'},'location','northeast')
xlim([-1.3,-.25])
tightfig
saveas(fig,'fig7.png')




%%
indx2 = find((kan_r==0)&contains(drugNm,'kan')&(drug_conc==100));
stTime = [1 30:30:210];ft1=[];leg=[];
for st = 1:length(stTime)
    hold on
    mat = intMatR(indx2,stTime(st):stTime(st)+30);
    x = [];y=[];nn = 1;
    for i = 1:size(mat,1)
        tmp = sign(diff(mat(i,:)));
        tmp(tmp==0)=1;pd = {};
        p1 = 1;cntr=1;indx_cnt = 1;
        for j = 2:size(tmp')
            if tmp(j)==tmp(j-1) && cntr <= 2
                continue
            elseif tmp(j)==-tmp(j-1) && cntr <= 2
                cntr = cntr + 1;
            elseif cntr>2
                pd{indx_cnt} = [p1 j-1];
                p1 = j-1;
                cntr = 1;
                indx_cnt = indx_cnt+1;
            else
                error('Whoops')
            end
        end
        for j=1:length(pd)
            x(nn) = diff(pd{j});
            y(nn) = max(mat(i,pd{j}(1):pd{j}(2)))-min(mat(i,pd{j}(1):pd{j}(2)));
            nn = nn + 1;
        end
    end
    [C,~,ic] = unique(x);x_counts = accumarray(ic,1);
    lim = max(C(x_counts>3));
    y = y(x<lim);    x = x(x<lim);
    hold on;
    cnt = 1;
    tmp_x=zeros(length(C),1);    tmp_y=zeros(length(C),1);
    for i = C
        tmp_x(cnt) = log10(1./i);
        tmp_y(cnt) = log10(mean(y(x==i)));
        cnt = cnt + 1;
    end

    ft1{st} = fitlm(tmp_y,tmp_x);   
    leg{st} = [num2str(stTime(st)) 'min:' num2str(stTime(st)+30) 'min'];
end
indx2 = find((kan_r==0)&(drug_conc==0));
ft2 = [];
for st = 1:length(stTime)
    hold on
    mat = intMatR(indx2,stTime(st):stTime(st)+30);
    x = [];y=[];nn = 1;
    for i = 1:size(mat,1)
        tmp = sign(diff(mat(i,:)));
        tmp(tmp==0)=1;pd = {};
        p1 = 1;cntr=1;indx_cnt = 1;
        for j = 2:size(tmp')
            if tmp(j)==tmp(j-1) && cntr <= 2
                continue
            elseif tmp(j)==-tmp(j-1) && cntr <= 2
                cntr = cntr + 1;
            elseif cntr>2
                pd{indx_cnt} = [p1 j-1];
                p1 = j-1;
                cntr = 1;
                indx_cnt = indx_cnt+1;
            else
                error('Whoops')
            end
        end
        for j=1:length(pd)
            x(nn) = diff(pd{j});
            y(nn) = max(mat(i,pd{j}(1):pd{j}(2)))-min(mat(i,pd{j}(1):pd{j}(2)));
            nn = nn + 1;
        end
    end
    [C,~,ic] = unique(x);x_counts = accumarray(ic,1);
    lim = max(C(x_counts>3));
    y = y(x<lim);    x = x(x<lim);
    hold on;
    cnt = 1;
    tmp_x=zeros(length(C),1);    tmp_y=zeros(length(C),1);
    for i = C
        tmp_x(cnt) = log10(1./i);
        tmp_y(cnt) = log10(mean(y(x==i)));
        cnt = cnt + 1;
    end

    ft2{st} = fitlm(tmp_y,tmp_x);   
end
fig = figure('Color','w','Position',[100 100 300 300]);
x = [];
for i = 1:length(ft1)
    x(i) = i;
    y1(i) = ft1{i}.Coefficients.Estimate(2);
    y2(i) = ft2{i}.Coefficients.Estimate(2);
end
hold on
plot(x,y1,'linewidth',2,'color','r')
plot(x,y2,'linewidth',2,'color','b')
xticks(x)
xticklabels(leg)
xtickangle(45)
xlim([0.5 length(ft1)+.5])
ylabel('Slope','FontSize',10)
legend('Kan Untreated','Kan Treated','FontSize',10,'Location','SouthEast')
tightfig()
saveas(fig,'fig8.png')










ft2 = [];leg = [];
%Loop through the different drugs
drugs = {'cipro','chlor','trim','PmB','kan',{'kan'}};
maxConc = [10,100,10,3,100,0];
for st = 1:length(drugs)
    if iscell(drugs{st})
        indx2 = find((kan_r==0)&(drug_conc==maxConc(st)));
        drugs{st}='all'
    else
        indx2 = find((kan_r==0)&contains(drugNm,drugs{st})&(drug_conc==maxConc(st)));
    end
    mat = intMatR(indx2,120:240);
    x = [];y=[];nn = 1;
    for i = 1:size(mat,1)
        tmp = sign(diff(mat(i,:)));
        tmp(tmp==0)=1;pd = {};
        p1 = 1;cntr=1;indx_cnt = 1;
        for j = 2:size(tmp')
            if tmp(j)==tmp(j-1) && cntr <= 2
                continue
            elseif tmp(j)==-tmp(j-1) && cntr <= 2
                cntr = cntr + 1;
            elseif cntr>2
                pd{indx_cnt} = [p1 j-1];
                p1 = j-1;
                cntr = 1;
                indx_cnt = indx_cnt+1;
            else
                error('Whoops')
            end
        end
        for j=1:length(pd)
            x(nn) = diff(pd{j});
            y(nn) = max(mat(i,pd{j}(1):pd{j}(2)))-min(mat(i,pd{j}(1):pd{j}(2)));
            nn = nn + 1;
        end
    end
    fig = figure('Color','w','Position',[100 100 300 300]);
    [C,~,ic] = unique(x);x_counts = accumarray(ic,1);
    lim = max(C(x_counts>3));
    y = y(x<lim);
    x = x(x<lim);
    hold on;
    cnt = 1;
    tmp_x=zeros(length(C),1);
    tmp_y=zeros(length(C),1);
    for i = C
        tmp_x(cnt) = log10(1./i);
        tmp_y(cnt) = log10(mean(y(x==i)));
        cnt = cnt + 1;
    end
    scatter(tmp_x,tmp_y,'b','filled')
    title({['SOC ' drugs{st} ' [' num2str(maxConc(st)) ']']},'FontSize',10)
    ylabel('log(transient amplitude)','FontSize',10)
    xlabel('log(transient frequency)','FontSize',10)
    ft2{st} = fitlm(tmp_x,tmp_y);
    plot(tmp_x,predict(ft2{st},tmp_x),'linewidth',2)
    TextLocation({['y=' num2str(ft2{st}.Coefficients.Estimate(2),2) 'x+' num2str(ft2{st}.Coefficients.Estimate(1),2)],['R^2=' num2str(ft2{st}.Rsquared.Adjusted,2)]},'Location','best')
    legend({'Data','Fit'},'location','northeast')
    tightfig
    leg{st} = [drugs{st} ' [' num2str(maxConc(st)) '] (' num2str(size(mat,1)) ')'];
    saveas(fig,['fig9_' num2str(st) '.png'])
end
fig = figure('Color','w','Position',[100 100 300 300]);
x = [];y2 = [];
for i = 1:length(ft2)
    x(i) = i;
    y2(i) = ft2{i}.Coefficients.Estimate(2);
end
hold on
bar(x,y2)
xticks(x)
xticklabels(leg)
xtickangle(45)
xlim([0.5 length(ft2)+.5])
ylabel('Slope','FontSize',10)
tightfig()
saveas(fig,'fig10.png')
