This repository contains the code for generating the figures and building the random forest models
for predicting bacterial drug resistance and sensitivity from real time measurements of calcium
dynamics.  Developed on Matlab 2020a running on ubuntu linux (20.04).  See license.txt.

Functions/
This folder contains the functions required for the generating the figures. 
Add to matlab path.
Also includes functions downloaded from Mathworks including:
Colormaps
shadedErrorBar
tightfig
TextLocation

Redistribution of these functions is controlled by copyright rules annotated in license.txt in the
associated folders.


Datasets/
This folder contains the processed data used for training the models.  The processed GCaMP6 signal
extracted from segmented cells in image stacks are saved as .mat files
Code in the AnalysisCode folder loads the appropriate dataset for each analysis

AnalysisCode/
This folder contains a series of folders with code for generating the figures.  NOTE that at the 
top of many of the scripts, one needs to set the correct directory.





 
