%Function for calculating feature matrix of traces
function [master_data,mastLab] = calcFeatureMatrix(S,deltaT)
    nms = fieldnames(S); %For each of the field names in the structure S (Mean/Min/Max/Std of GcAMP over time)
    
    master_data  = []; mastLab=[];
    for j = 1:length(nms)
        %Calculate the features
        [mat1,labs1] = calcTrajFeatures(S.(nms{j}));
        for l=1:length(labs1)
            labs1{l} = [labs1{l} '_' nms{j}];
        end
        master_data = [master_data,mat1];
        mastLab = [mastLab,labs1];
        %Calculate the features convolving with mean for 20% and 10%
        %trajectory length
        for i=[5,10]
            [mat1,labs1] = calcTrajFeatures(movmean(S.(nms{j}),ceil(deltaT/i),2));
            for l=1:length(labs1)
                labs1{l} = [labs1{l} '_' nms{j} '_movmean_deltaT_window_' num2str(ceil(deltaT/i))];
            end
            master_data = [master_data,mat1];
            mastLab = [mastLab,labs1];
        end
        %Calculate the features convolving with std for 20% and 10%
        %trajectory length
        for i=[5,10]
            [mat1,labs1] = calcTrajFeatures(movstd(S.(nms{j}),ceil(deltaT/i),[],2));
            for l=1:length(labs1)
                labs1{l} = [labs1{l} '_' nms{j} '_movstd_deltaT_window_' num2str(ceil(deltaT/i))];
            end
            master_data = [master_data,mat1];
            mastLab = [mastLab,labs1];
        end

        %Now normalize matrix to T==0
        %And compute same features
        tmp = S.(nms{j});tmp = tmp./tmp(:,1);
        [mat1,labs1] = calcTrajFeatures(tmp);
        for l=1:length(labs1)
            labs1{l} = [labs1{l} '_' nms{j} '_norm'];
        end
        master_data = [master_data,mat1];
        mastLab = [mastLab,labs1];
        for i=[5,10]
            [mat1,labs1] = calcTrajFeatures(movmean(tmp,ceil(deltaT/i),2));
            for l=1:length(labs1)
                labs1{l} = [labs1{l} '_' nms{j} '_norm_movmean_deltaT_window_' num2str(ceil(deltaT/i))];
            end
            master_data = [master_data,mat1];
            mastLab = [mastLab,labs1];
        end
        for i=[5,10]
            [mat1,labs1] = calcTrajFeatures(movstd(tmp,ceil(deltaT/i),[],2));
            for l=1:length(labs1)
                labs1{l} = [labs1{l} '_' nms{j} '_norm_movstd_deltaT_window_' num2str(ceil(deltaT/i))];
            end
            master_data = [master_data,mat1];
            mastLab = [mastLab,labs1];
        end
    end

end